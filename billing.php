<?php
include "./include/config.php";
include "./plugins/pesapal/PesaPal.php";


//write your php code here to pull data from session and from the database.

?>

<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="Projects sell/buy platform">
	    <meta name="author" content="Entel Limited">
		<title>Project Pal | Billing</title>
		
		<link href="./css/bootstrap.min.css" rel="stylesheet">
		<link href="./css/custom.css" rel="stylesheet">
		<link rel="icon" href="./css/logo_2.png" type="image/x-icon">.
	</head>
	<body class="bgd-theme">
		<?php
		
		if(isset($_GET['spec_id'])) {
			$spec_id = $_GET['spec_id'];
		} else {
			header('location:./projects.php');
			exit;
		}
		
		if (!isset($_SESSION['user_email'])){
			header('location:./getDs_/d_login.php?spec_id='.$spec_id);
			exit;
		}
		
		$access_token = uniqid();
		setcookie('download_cookie', $access_token, 0);
		
		//echo $_COOKIE['download_cookie'];
		//exit;
		
		$Obj = new Projects();
		
		$getProjectInfo = $Obj->get_project_downloadInfo($spec_id);
		$project_name = $Obj->project_titleInfo;
		$paid = $Obj->project_payInfo;
		$fee = $Obj->project_feeInfo;
		$desc = $Obj->project_descInfo;
		$approved = $Obj->project_approveInfo;
		
		if($Obj->count_files < 1) {
			header('location:./getDs_/no_files.php');
		}
		
		$firstName = $_SESSION['firstname'];
		$secondName = $_SESSION['lastname'];
		$description = $project_name ." - Project Pal Payment"; 		//->description of the payment e.g make payment for Project?
	    $ref = $spec_id; 												//-> the unique id of the project
	    $eml = $_SESSION['user_email']; 								//->email of the user
	    $phonenumber = $_SESSION['user_phone']; 						//-> the phonenumber of the user
	    $typ = "Merchant"; 												//should be MERCHANT, (THIS IS THE DEFAULT)
	    $amount = $fee;													//The amount to be paid -> should be from the database
		//$callback_url = "http://www.projectpal.co.ke/getDs_/get_project_.php?access_token=".$access_token."&spec_token=".$spec_id; //-> the url to go to once a successfull payment has been made.
		$callback_url = "./getDs_/get_project_.php?access_token=".$access_token."&spec_token=".$spec_id; //-> the url to go to once a successfull payment has been made.
		
		
		if ($paid == 1 && $approved == 1) {
			//echo 'gd';
			header('location:'.$callback_url);
		}
		?>
		
		
		<div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="login-panel panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please make payments here</h3>
                    </div>
                    <div class="panel-body">
					<div class="alert alert-danger">Make sure you have a working Internet Connection<a href="#" data-dismiss="alert" class="close">�</a></div>
                        <?php
                        	if ($paid == 2 && $approved == 1) {
								//echo json_encode($getProjectInfo);
								$bill = new Pesapal($firstName, $secondName, $description, $ref, $eml, $phonenumber, $typ, $amount, $callback_url);
							}
						?>
                    </div>
                </div>
            </div>
        </div>
    </div>
		
	</body>
</html>
