<?php /* Smarty version Smarty-3.1.14, created on 2015-04-28 06:21:19
         compiled from ".\d_login.tpl" */ ?>
<?php /*%%SmartyHeaderCode:584854eafbbb412171-83726895%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'c54ca98b683eb758cfae69cea1f7b61d0b7af8d3' => 
    array (
      0 => '.\\d_login.tpl',
      1 => 1430191090,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '584854eafbbb412171-83726895',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_54eafbbb42d6a1_85846141',
  'variables' => 
  array (
    'menu_login' => 0,
    'id' => 0,
    'spec' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_54eafbbb42d6a1_85846141')) {function content_54eafbbb42d6a1_85846141($_smarty_tpl) {?>

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Projects sell/buy platform">
    <meta name="author" content="Entel Limited">

    <title>Dash | User Login </title>
	<link href="../user_area/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="../user_area/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
	<link href="../user_area/dist/css/custom.css" rel="stylesheet">
	<link href="../user_area/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>
	
	<?php echo $_smarty_tpl->tpl_vars['menu_login']->value;?>

	
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Sign In</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" id="d-login-form" method="POST" action="./d_login.php?d_id=<?php echo $_smarty_tpl->tpl_vars['id']->value;?>
&spec_id=<?php echo $_smarty_tpl->tpl_vars['spec']->value;?>
">
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="d_email" id="d_email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="d_pass" id="d_password"  type="password" value="">
                                </div>
                                
                                <button type="submit" class="btn btn-sm btn-primary btn-block" id="d_login_btn" name="submit">Login</button>
                                <br>
                                <i class="btn-block centered">You don't have an account?</i><br>
								<a href="../user_area/pages/user_reg.php" class="btn btn-sm btn-success btn-block" id="register" name="register" >Create an Account</a>
								
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../user_area/bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../user_area/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    //<script src="../user_area/bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../user_area/dist/js/sb-admin-2.js"></script>
	<!-- <script src="../js/custom.js"></script>-->

</body>

</html>
<?php }} ?>