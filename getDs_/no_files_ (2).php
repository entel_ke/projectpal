<?php
include "../include/config.php";


?>

<!DOCTYPE html>
<html lang="en">

	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="Projects sell/buy platform">
	    <meta name="author" content="Entel Limited">
		<title>Project Pal | Download</title>
		
		<link href="../css/bootstrap.min.css" rel="stylesheet">
		<link href="../css/custom.css" rel="stylesheet">
		<link rel="icon" href="../css/logo_2.png" type="image/x-icon">.
	</head>
	<body class="bgd-theme">
		
		<div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Download project</h3>
                    </div>
                    <div class="panel-body">
					<div class="alert alert-danger">This project has no downloadable files<a href="#" data-dismiss="alert" class="close">&times;</a></div>
                        
                    </div>
                </div>
            </div>
        </div>
    </div>
		
	</body>
</html>

