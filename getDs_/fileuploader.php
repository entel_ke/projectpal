<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>User - Dashboard |Upload Project</title>
	<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
	<link href="../dist/css/timeline.css" rel="stylesheet">
	<link href="../dist/css/custom.css" rel="stylesheet">
	<link href="../bower_components/morrisjs/morris.css" rel="stylesheet">
	<link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../../include/dropzone/css/dropzone.css">
	<link rel="stylesheet" href="../dist/css/style.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../dist/css/jquery.fileupload.css">
	

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

       {$menus}

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard | Home</h1>
                </div>
                <!-- /.col-lg-12 --> <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Upload Project
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
								<div class="col-lg-6">
									<form role="form" id="add-project-form">
										<div class="form-group">
												<select class="form-control" name="type" id="pay-opt" required="">
													<option value="0">--Select payment option --</option>
													<option value="1">Free project</option>
													<option value="2">Paid project</option>
												</select>
										</div>
										<div class="clearfix"></div>
										<div class="form-group price-box">
											<input class="form-control" placeholder="Enter project price in KES" name="project_price" id="project_price" value="0" required="">
										</div>
										<div class="clearfix"></div>
										<div class="form-group">
												<select class="form-control" name="courses" id="courses">
													<option value="0">--Select Courses --</option>
												{section start=0 step=1 name=row loop=$courses}
													<option value="{$courses[row].id}">{$courses[row].course_name}</option>
												{/section}		
												</select>
										</div>
										<div class="clearfix"></div>
										<div class="form-group">
											<input class="form-control" placeholder="Enter project title" name="project_title" id="project_title" required="">
										</div>
										<div class="form-group">
											<textarea class="form-control" rows="5" placeholder="Enter description here" name="description" id="description"  required=""></textarea>
										</div>
										<div class="form-group">
											Programming Language(s) - Optional
											<div class="lang-area">
												<input class="lang-input" placeholder="" name="languages" id="lang" >
											</div>
											
										</div>
										<div class="clearfix"></div>
										<div class="form-group">
											<button type="submit" class="btn btn-primary" id="add_project" name="submit">Upload</button>
										</div>
									</form>
																
								</div>
								<div class="col-lg-6">
									<div class="panel panel-default">
										<div class="panel-heading">
											<h3 class="panel-title centered">Upload project files here</h3>
										</div>
										<div class="panel-body">
											{*<form class="dropzone" method="post" action="../../admin/ajax/uploadfiles.php">
											</form>*}
											<!-- The fileinput-button span is used to style the file input field as button -->
										    <span class="btn btn-success fileinput-button">
										        <i class="glyphicon glyphicon-plus"></i>
										        <span>Select files...</span>
										        <!-- The file input field used as target for the file upload widget -->
										        <input id="fileupload" type="file" name="files[]" multiple>
										    </span>
										    <br>
										    <br>
										    <!-- The global progress bar -->
										    <div id="progress" class="progress">
										        <div class="progress-bar progress-bar-success"></div>
										    </div>
										    <!-- The container for the uploaded files -->
										    <div id="files" class="files"></div>
										</div>
									</div>
								</div>
                            </div>
                            <!-- /.table-responsive -->
                            <!--
							<div class="well">
                                <h4>Assets Information</h4>
                                <p>The table above displays all the Items in the systems database. The rows in green represents assets that have been <a target="_blank" href="found.php">recovered</a> by their respective owners. Rows in pink shows items yet not recovered</p>
                                <a class="btn btn-default btn-lg btn-block" target="_blank" href="pending.php">View Pending Assets</a>
                            </div> -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
	<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
	<script src="../bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../dist/js/sb-admin-2.js"></script>
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
	<script src="../dist/js/vendor/jquery.ui.widget.js"></script>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="../dist/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="../dist/js/jquery.fileupload.js"></script>
    {literal}
	<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
        
        $(function () {
        	alert();
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = '../server/php/';
		    $('#fileupload').fileupload({
		        url: url,
		        dataType: 'json',
		        done: function (e, data) {
		            $.each(data.result.files, function (index, file) {
		                $('<p/>').text(file.name).appendTo('#files');
		            });
		        },
		        progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
		            $('#progress .progress-bar').css(
		                'width',
		                progress + '%'
		            );
		        }
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
		
    });
    </script>
    {/literal}
	<script src="../../js/custom.js"></script>
	<script src="../../include/dropzone/dropzone.js"></script>
</body>

</html>
