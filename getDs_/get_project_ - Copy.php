<?php
include "../include/config.php";

if (!isset($_GET['access_token']) || !isset($_GET['download_token']) || !isset($_GET['spec_token'])){
	header;('location:../');exit; 
}

if (!isset($_COOKIE['download_cookie']) || $_COOKIE['download_cookie']!= $_GET['access_token']){
	header('location:../');exit;
	exit;
} 

//setcookie('download_cookie',$_COOKIE['download_cookie'], time()-60);

$Obj = new Projects();
		
$getProjectInfo = $Obj->get_project_downloadInfo($_GET['download_token'], $_GET['spec_token']);
$project_name = $Obj->project_titleInfo;
$folder_name = $Obj->project_folderInfo;
$project_folder = $_GET['spec_token'];
$file_name = $Obj->project_fileInfo;

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
	    <meta http-equiv="X-UA-Compatible" content="IE=edge">
	    <meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="ProjectPal:Projects sell/buy platform">
	    <meta name="author" content="Entel Limited">
	    <title>Project Pal | Download</title>
	    
	    <link href="../user_area/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
		<link href="../user_area/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
		<link href="../user_area/dist/css/custom.css" rel="stylesheet">
		<link href="../user_area/bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	
	    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	    <!--[if lt IE 9]>
	        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
	        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
	    <![endif]-->
	</head>
	<body class="bgd-theme">
			
	
	<!--?php include "./menu_login.php";?>-->
	
	
	
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <div class="login-panel panel panel-primary  shd-theme">
                    <div class="panel-heading">
                        <h3 class="panel-title">Download</h3>
                    </div>
                    <div class="panel-body" id="names_holder">
						<input type="hidden" name="file_name" id="file_name" value="<?php echo $file_name;?>">
						<input type="hidden" name="folder_name" id="folder_name" value="<?php echo $folder_name;?>">
						<input type="hidden" name="project_name" id="project_name" value="<?php echo $project_name;?>">
						<strong> The Project is downloading.......<img src="../css/301_black_24.GIF" /></strong>
                    </div>
					<div class="panel panel-default">
                    	<div class="panel-heading">
                        	<a href='../' class="btn btn-success btn-sm" style="position:relative !important"><i class="fa fa-home fa-fw"></i> Go back home</a>
                    	</div>
                    </div>
                    
                </div>
            </div>
        </div>
		
		<?php 
			//echo "HELLLLLLLLLOOOOOOOOOOOOOOOOOOO";
			include './download.php';
			
			?>
		
    </div>
	
	<script src="../js/jquery.min.js"></script>
	<script src="../js/jquery.js"></script>
	
	<script>
    $(document).ready(function() {
        //alert();
		//download ();
		
		function download() {
			
			var file_name = $("#names_holder").find('#file_name').val().trim();
			var project_name = $("#names_holder").find('#project_name').val().trim();
			var folder_name = $("#names_holder").find('#folder_name').val().trim();
			
			data = {
				'file_name':file_name,
				'folder_name':folder_name,
				'fc':project_name,
				'f':file_name
			};
				
			$.ajax({
				type    : "GET",
				cache   : false,
				data : data,
				url     : "../admin/ajax/download.php",
				success: function(response) {
					alert(response);
				}
			});
			
			return false;
		};
		
    });
    </script>
	
	
	</body>
</html>

