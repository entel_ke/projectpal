<!DOCTYPE HTML>

<html>
	<head>
		<title>{$title}</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<script src="js/skel.min.js"></script>
		<script src="js/init.js"></script>
		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link rel="icon" href="dist/img/logo_2.png" type="image/x-icon">
		
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-wide.css" />
			<link rel="stylesheet" href="css/style-noscript.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body class="loading">
		<div id="wrapper">
			<div id="bg"></div>
			<div id="overlay"></div>
			<div id="main">

				<!-- Header -->
					<header id="header">
						<h1><img src="dist/img/main-logo.png" alt=""/></h1>
						<h2>ProjectPal Past papers</h2>
						<p>Friendly &nbsp;&bull;&nbsp; Educational &nbsp;&bull;&nbsp; Partner</p>
						<nav>
							<ul>
								<li></li>
								<li>Select Past Papers category below</li>
								<li></li>
							</ul>
							<ul class="nav-btns">
								<li></li>
								<li><a href="past_papers/sec_sch/"  class="btn btn-primary btn-lg">Secondary School Past Papers</a></li>
								<li><a href="past_papers/ter_sch/"  class="btn btn-primary btn-lg">Higher Education Past Papers</a></li>
								<li></li>
							</ul>
							<ul class="nav-btns">
								<li></li>
								<li><p><i class="fa fa-home"></i> <a href="./">Go Back Home</a></p></li>
								<li></li>
							</ul>
							
						</nav>
					</header>

				<!-- Footer -->
					<footer id="footer">
						<span class="copyright"> Copyright &copy; <a href="http://projectpal.co.ke">ProjectPal</a>.</span>
					</footer>
				
			</div>
		</div>
	</body>
</html>