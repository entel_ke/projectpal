<div class="box-header">
  <i class="fa fa-file"></i>
  <h3 class="box-title">Projects</h3>
  <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
    <ul class="pagination pagination-sm inline">
      <li><a href="#">&laquo;</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">&raquo;</a></li>
    </ul>
  </div>
</div>
<div class="box-body" id="projects">
  <ul class="todo-list">
  	{section start=0 step=1 name=row loop=$projects}
	    <li>
	      <span class="handle">
	        <i class="fa fa-ellipsis-v"></i>
	        <i class="fa fa-ellipsis-v"></i>
	      </span>
	      <span class="text"><a href="view_project.php?spec_id={$projects[row].spec_id}">{$projects[row].project_title}</a></span><br>
	      <p>
	      	{$projects[row].category}<br>
			<span class="green"></span>
	      	<span class="pull-right">
	            <a href="billing.php?spec_id={$projects[row].spec_id}" class="btn btn-success"><i class="fa fa-download"> </i> {if ($projects[row].paid == 1)}Download {else} Purchase {/if}</a>
	        </span>
	      </p>
	      <small class="label {if ($projects[row].paid == 1)}label-info {else} label-danger {/if}"><i class="fa fa-money"></i> {if ($projects[row].paid == 1)}Free {else} KSH. {$projects[row].fee} {/if}</small>
	      
	    </li>
    {/section}
    
  </ul>
</div><!-- /.projects -->
<div class="box-footer clearfix no-border">
  <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Upload Project</button>
</div>