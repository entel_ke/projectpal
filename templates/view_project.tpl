<div class="box-header">
  <i class="fa fa-file"></i>
  <h3 class="box-title"> 
  	{section start=0 step=1 name=row loop=$project}
  		<a href="view_project.php?id={$project[row].id}">{$project[row].project_title}</a>
  	{/section}
  </h3>
  <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
    
  </div>
</div>
<div class="box-body" id="projects">
	<div class="row">
        <div class="col-md-9 col-sm-6 col-xs-12">
        {section start=0 step=1 name=row loop=$project}
          	<p>
				<label>Category</label>: {$project[row].category}<br>
				<label>Course</label>: {$project[row].course_name}<br>
				<label>Uploaded On</label>: {$project[row].date_uploaded}<br>
				<small class="label {if ($project[row].paid == 1)}label-info {else} label-danger {/if}"><i class="fa fa-money"></i> {if ($project[row].paid == 1)}Free {else} KSH. {$project[row].fee} {/if}</small>
				<br>
				<h4 class="media-heading"><b>Description</b></h4>
				
				<p>
					{$project[row].description}
				<p>
			</p>
		{/section}	
        </div><!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
          	<div class="callout callout-default no-margin center-align">
		 		<h3>{$views}</h3>
				<h5>Views</h5>
		 		<h3>{$count_files}</h3>
				<h5>Files</h5>
				<h3>{$downloads}</h3>
				<h5>Downloads</h5>
				
				<br><br>	
	        </div>
        </div><!-- /.col -->
        
    </div><!-- /.row -->              
  <ul class="todo-list">
	  
    
  </ul>
 
</div><!-- /.projects -->
<div class="box-footer clearfix">
  <button class="btn btn-success pull-right"><i class="fa fa-download"></i> Download</button>
</div>