<?php
	require "../include/config.php";
	
	if (!isset($_SESSION['admin'])) {
		header('location:../admin_login.php');
		exit;
	}
	
	$Obj = new classMain();
	$Obj_projects = new Projects();
	$Obj_categories = new Categories();
	$Obj_courses = new Courses();
	
	//Get Projects variables
	$users = $Obj->getUsers();
	$count_users = $Obj->count_users;
	//Get Categories variables
	$categories = $Obj_categories->getCategories();
	$count_categories =$Obj_categories->count_categories;
	//Get Courses variables
	$courses = $Obj_courses->getCourses('');
	$count_courses =$Obj_courses->count_courses;
	//Get Projects variables
	$projs = $Obj_projects->getProjects('');
	$projects = $Obj_projects->all_projects;
	$count_all_projects = sizeof($projects);
	
	$new_projects = $Obj_projects->latest_projects;
	$count = $Obj_projects->count_projects;
	$count_pending = $Obj_projects->count_pending;
	$approved_projects = $Obj_projects->approved_projects;
	$count_downloads = $Obj_projects->get_all_downloads();
	//$count_transactions = $Obj_projects->count_transactions;
	//$count_visits = $Obj_projects->count_visits;
	//$count_unread = $Obj_projects->count_unread;
	//Get Messages variables
	$messages = $Obj->getMessages();
	$unread_messages = $Obj->unread_messages;
	$count_messages =$Obj->count_all_messages;
	$count_unread =$Obj->unread_count;
	
	
	$smarty = new Smarty;
	
	
	$smarty->assign('title', 'Dashboard | Home');
	$smarty->assign('main_head', 'Users');
	$smarty->assign('top_logo', 'ProjectPal');
	$smarty->assign('categories', $categories);
	$smarty->assign('courses', $courses);
	$smarty->assign('new_projects', $new_projects);
	$smarty->assign('count_projects', $count_all_projects);
	$smarty->assign('count_pending',$count_pending);
	$smarty->assign('approved_projects', $approved_projects);
	$smarty->assign('count_downloads', $count_downloads);
	$smarty->assign('users', $users);
	$smarty->assign('count_users', $count_users);
	$smarty->assign('count_new_users', '1');
	$smarty->assign('count_transactions', '111');
	$smarty->assign('count_visits', '6875');
	$smarty->assign('unread_messages', $unread_messages);
	$smarty->assign('count_unread', $count_unread);
	$smarty->assign('count_courses', $count_courses);
	$smarty->assign('count_categories', $count_categories);
	$smarty->assign('projects', $projects);
	$content = $smarty->fetch('./templates/users.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main.tpl');

?>