/** DELTA ADMIN **/
$(document).ready(function(){
	
	
	
	// === Prepare peity charts === //
	unicorn.peity();
	
		
    // === Calendar === //    
    var date = new Date();
	var d = date.getDate();
	var m = date.getMonth();
	var y = date.getFullYear();
	
	$('.calendar').fullCalendar({
		header: {
			left: 'prev,next',
			center: 'title',
			right: 'month,basicWeek,basicDay'
		},
		editable: true,
		events: [
			{
				title: 'All day event',
				start: new Date(y, m, 1)
			},
			{
				title: 'Long event',
				start: new Date(y, m, 5),
				end: new Date(y, m, 8)
			},
			{
				id: 999,
				title: 'Repeating event',
				start: new Date(y, m, 2, 16, 0),
				end: new Date(y, m, 3, 18, 0),
				allDay: false
			},
			{
				id: 999,
				title: 'Repeating event',
				start: new Date(y, m, 9, 16, 0),
				end: new Date(y, m, 10, 18, 0),
				allDay: false
			},
			{
				title: 'Lunch',
				start: new Date(y, m, 14, 12, 0),
				end: new Date(y, m, 15, 14, 0),
				allDay: false
			},
			{
				title: 'Birthday PARTY',
				start: new Date(y, m, 18),
				end: new Date(y, m, 20),
				allDay: false
			},
			{
				title: 'Click for Google',
				start: new Date(y, m, 27),
				end: new Date(y, m, 29),
				url: 'http://www.google.com'
			}
		]
	});

    // === Popovers === //    
    var placement = 'bottom';
    var trigger = 'hover';
	var ttl_projects = $('.ttl_projects').html();
	var pending_projects = $('.td_projects').val();
	var approved_projects = $('.approved_projects').html();
	var ttl_users = $('.ttl_users').html();
	var ttl_categories = $('.ttl_categories').html();
	var ttl_courses = $('.ttl_courses').html();
	/*
	
	
	var ttl_Transactions = $('.ttl_transactions').html();
	var td_Users = $('.td_Users').html();
	var td_Categories = $('.td_categories').html();
	var td_Transactions = $('.td_transactions').html();*/
	
	var html = true;

    $('.popover-categories').popover({
       placement: placement,
       content: '<span class="content-big">'+ttl_categories+'</span> <span class="content-small">Total categories</span>',
       trigger: trigger,
       html: html   
    });
    $('.popover-users').popover({
       placement: placement,
       content: '<span class="content-big">'+ttl_users+'</span> <span class="content-small">Total Users</span><br /><span class="content-big">0</span> <span class="content-small">Registered Today</span><br /><span class="content-big">2</span> <span class="content-small">Registered This month</span>',
       trigger: trigger,
       html: html   
    });
    $('.popover-courses').popover({
       placement: placement,
       content: '<span class="content-big">'+ttl_courses+'</span> <span class="content-small">Total courses</span>',
       trigger: trigger,
       html: html   
    });
    $('.popover-projects').popover({
       placement: placement,
       content: '<span class="content-big">'+ttl_projects+'</span> <span class="content-small">Projects</span><br /><span class="content-big">'+pending_projects+'</span> <span class="content-small">Pending reviews</span><br /><span class="content-big">'+approved_projects+'</span> <span class="content-small">Approved</span>',
       trigger: trigger,
       html: html   
    });
});


unicorn = {
		// === Peity charts === //
		peity: function(){		
			$.fn.peity.defaults.line = {
				strokeWidth: 1,
				delimeter: ",",
				height: 24,
				max: null,
				min: 0,
				width: 50
			};
			$.fn.peity.defaults.bar = {
				delimeter: ",",
				height: 24,
				max: null,
				min: 0,
				width: 50
			};
			$(".peity_line_good span").peity("line", {
				colour: "#B1FFA9",
				strokeColour: "#459D1C"
			});
			$(".peity_line_bad span").peity("line", {
				colour: "#FFC4C7",
				strokeColour: "#BA1E20"
			});	
			$(".peity_line_neutral span").peity("line", {
				colour: "#CCCCCC",
				strokeColour: "#757575"
			});
			$(".peity_bar_good span").peity("bar", {
				colour: "#459D1C"
			});
			$(".peity_bar_bad span").peity("bar", {
				colour: "#BA1E20"
			});	
			$(".peity_bar_neutral span").peity("bar", {
				colour: "#757575"
			});
		},

		// === Tooltip for flot charts === //
		flot_tooltip: function(x, y, contents) {
			
			$('<div id="tooltip">' + contents + '</div>').css( {
				top: y + 5,
				left: x + 5
			}).appendTo("body").fadeIn(200);
		}
}
