<?php
	require "../include/config.php";
	
	if (isset($_SESSION['admin'])) {
		header('location:admin_home.php');
		exit;
	}
	
	$Obj = new classMain();
		
	$smarty = new Smarty;
	
	$smarty->assign('title', 'Projects | Admin');
	$smarty->assign('top_logo', 'Dashboard');
	
	$menus = $smarty->fetch('./templates/menu_login.tpl');
	$smarty->assign('menu_login', $menus);
	
	$smarty->display('./templates/admin_login.tpl');

?>