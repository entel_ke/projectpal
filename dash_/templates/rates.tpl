<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Site Statistics</h5><div class="buttons"><a href="#" class="btn btn-mini"><i class="icon-refresh"></i> Update stats</a></div>
</div>
<div class="widget-content">
	<div class="row-fluid">
		<div class="span8 categories-area">
			<div class="alert alert-success alert-rate centered">
				{section start=0 step=1 name=row loop=$rates}
					<span class="label label-info tip-left size-adjusted">
						<label>{$rates[row].rate}%</label>
						<br>
						Standing rate
					</span>
					<br/>
					<br/>
					<p>The Project Owner gets {100 - ($rates[row].rate)}% of the total amount per Purchase as from <br><br ><span class="label label-success">{$rates[row].last_update}</span></p>
					
				{/section}	
				</div>
		</div>
		<div class="span4 lifted">
			<div class="widget-title"><span class="icon"><i class="icon-plus"></i></span><h5 class="side-tbtitle">Change Rate</h5></div>
				<div class="widget-content nopadding">
					<form id="rate-form" method="post">
						<div class="form-group col-lg-6">
							<br>
							<input class="form-control input-area" placeholder="New rate format: Whole number e.g 0 or 15" name="rate" id="rate" value="" required="">
							<input type="hidden" class="form-control input-area" placeholder="rate" name="rate_holder" id="rate_holder" value="{section start=0 step=1 name=row loop=$rates}{$rates[row].rate}{/section}">
							<br>
						</div>
						<br>
						<div class="clearfix"></div>
						<div class="form-group col-lg-6">
							<button type="submit" class="btn btn-primary" id="change-rate" >Change Rate</button>
							
						</div>
						<br>
					</form>
				</div>
			
		</div>	
	</div>							
</div>