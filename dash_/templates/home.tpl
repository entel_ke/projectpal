<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Site Statistics</h5><div class="buttons btn-refresh"><a href="#" class="btn btn-mini" class="btn-refresh"><i class="icon-refresh"></i> Update stats</a></div></div>
						<div class="widget-content stats-content">
							<div class="row-fluid stats-holder">
							<div class="span4">
								<ul class="site-stats">
									<li><i class="icon-spin icon-spinner"></i><strong>{$count_pending}</strong> <small>Pending reviews</small></li>
									<li><i class="icon-download"></i> <strong>{$count_downloads}</strong> <small>Total Downloads</small></li>
									<li class="divider"></li>
									<li><i class="icon-th-list"></i> <strong>{$count_categories}</strong> <small>Manage Categories</small></li>
									<li><i class="icon-certificate"></i> <strong>{$count_courses}</strong> <small>Manage Courses</small></li>
									<li class="divider"></li>
									<li><i class="icon-user"></i> <strong>{$count_users}</strong> <small>Total Users</small></li>
									<li><i class="icon-arrow-right"></i> <strong>{$count_new_users}</strong> <small>New Users (Today's user)</small></li>
									
								</ul>
							</div>
							<div class="span8">
								<div class="widget-title"><span class="icon"><i class="icon-envelope"></i></span><h5>Messages</h5><span title="unread" class="label label-info tip-left">{$count_unread}</span></div>
									<div class="widget-content nopadding">
										<ul class="recent-posts>
										{if ($count_messages > 0)}
										{section start=0 step=1 name=row loop=$unread_messages}
											<li>
												<div class="user-thumb">
													<img width="40" height="40" alt="User" src="./images/user.png">
												</div>
												<div class="article-post">
													<span class="user-info"> By: {$unread_messages[row].name} on {$unread_messages[row].date_saved}</span>
													<p>
														<a href="#">{$unread_messages[row].message}...</a>
													</p>
													
													<a class="btn btn-primary btn-mini" href="#" data-placement="right" data-original-title="Edit"><i class="icon-pencil"></i></a>
													<a class="btn btn-success btn-mini" href="#" data-placement="top" data-original-title="Approved"><i class="icon-ok"></i></a>
													<a class="btn btn-danger btn-mini" href="#" data-placement="left" data-original-title="Delete"><i class="icon-remove"></i></a>	
												</div>
											</li>
											{/section}
											{else}
												<li class="align-center"><br><div class="alert alert-info">No messages</div></li>
											{/if}
										</ul>
									</div>
								</div>	
							</div>							
						</div>