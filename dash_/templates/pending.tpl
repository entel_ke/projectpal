<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Site Statistics</h5><div class="buttons"></div>
</div>
<div class="widget-content">
	<div class="row-fluid">
		<div class="span12">
			 {if ($count_pending > 0)}
			<table class="table table-hover table-striped" id="tb-categories">
				<thead>
					<tr>
						<th width="5%"><input type="checkbox" class="select-all"></th>
						<th width="60%">Project name</th>
						<th width="35%" class="">Action</th>
					</tr>
				</thead>
				<tbody>
				   
						{section start=0 step=1 name=row loop=$pending_projects}
						<tr>
							<td><input type="checkbox" class="check check_proj" value="{$projects[row].spec_id}"></td>
							<td class="edit-cat" id-holder="{$pending_projects[row].id}">
								<a href="#" >{$pending_projects[row].project_title}</a>
							</td>
							<td>
								<ul class="action inline pull-right">
										<li><a href="get_review.php?spec_id={$projects[row].spec_id}" class="btn s-width btn-xs btn-success download-review">Download</a></li>
									{if ($projects[row].approved == 0)}
										<li class="btn s-width btn-xs btn-primary approve-project"> Approve </li>
									{else}
										<li class="btn s-width btn-xs btn-default">Active</li>
									{/if}	
									<li class="btn btn-xs btn-danger purge-project"><i class="icon icon-trash"></i></li>
								</ul>
								<input type="hidden" value="{$projects[row].id}" class="id_holder">
								
							</td>
						</tr>
						{/section}
					 
				</tbody>
			</table>
			{else}
				<div class="alert alert-info">
					No Pending projects
				</div>
			{/if}
		</div>
		
	</div>							
</div>