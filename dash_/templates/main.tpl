<!DOCTYPE html>
<html lang="en">
	<head>
		<title>{$title}</title>
		<meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet/less" type="text/css" href="themes/less/bootstrap.less">
		<script src="themes/js/less/less.js" type="text/javascript"></script>
		<link rel="stylesheet" href="themes/style/fullcalendar.css" />	
		<link rel="stylesheet" href="themes/style/delta.main.css" />
		<link rel="stylesheet" href="themes/style/delta.grey.css"/>
		<link rel="icon" href="../dist/img/logo_2.png" type="image/x-icon">
	</head>
	<body>
	<br>
	<div id="sidebar"> 
		<h3 id=""><span class="logo-holder"> <img src="../dist/img/main-logo.png" width="40" height="40"</span>  ProjectPal</h3>  
		<a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
		<ul>
			<li class="active"><a href="./"><i class="icon icon-home"></i> <span>Home</span></a></li>
			<li><a href="categories.php"><i class="icon icon-th-list"></i> <span>Categories</span> <span class="label"></span></a></li>
			<li><a href="courses.php"><i class="icon icon-certificate"></i> <span>Courses</span> <span class="label"></span></a></li>
			<li class="submenu">
				<a href="#"><i class="icon icon-book"></i> <span>Projects</span> {if ($count_pending > 0)} <span class="label">{$count_pending}</span>{/if}</a>
				<ul>
					<li><a href="projects.php">Manage</a></li>
					<li><a href="pending.php">Pending</a></li>
					{*<li><a href="downloads.php">Downloads</a></li>*}
				</ul>
			</li>
			<li><a href="users.php"><i class="icon icon-user"></i> <span>Users</span> <span class="label"></span></a></li>
			<li><a href="inbox.php"><i class="icon icon-comment"></i> <span>Inbox</span>{if ($count_unread > 0)} <span class="label">{$count_unread}</span>{/if}</a></li>
			{*<li><a href="transactions.php"><i class="icon icon-tag"></i> <span>Transactions</span></a></li>*}
			<li><a href="rates.php"><i class="icon icon-tag"></i> <span>Rates</span></a></li>
			
		</ul>
	</div>
	  <div id="mainBody">
			<h3>Admin
				<div class="pull-right">
				<a class="btn btn-large tip-bottom" title="Manage Projects" style="position:relative"><i class="icon-book"></i>
				{if ($count_pending > 0)}
				<span style="position:absolute; border-radius:12px; top:-23%; height:16px; width:16px" class="label label-important btn-success">{$count_pending}</span>
				{/if}
				</a>
				<a class="btn btn-large tip-bottom" title="Manage Users" style="position:relative"><i class="icon-user"></i></a>
				<a class="btn btn-large tip-bottom" title="Manage Messages" style="position:relative"><i class="icon-comment"></i>
				{if ($count_unread > 0)}
				<span style="position:absolute; border-radius:12px; top:-23%; height:16px; width:16px" class="label label-important">{$count_unread}</span>
				{/if}
				</a>
				<a class="btn btn-large tip-bottom" title="Manage Transactions"><i class="icon-tag"></i></a>
				<a class="btn btn-large" title="" href="#"><i class="icon icon-user"></i> <span>Profile</span></a>
				<!--<a class="btn btn-large" title="" href="#"><i class="icon icon-cog"></i> Settings</a>-->
				<a class="btn btn-large btn-danger" title="" href="../"><i class="icon-off"></i></a>
				</div>
			</h3>
		<div id="breadcrumb">
			<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Dashboard</a>
			<a href="#" class="current">{$main_head}</a>
		</div>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12">
					
					<div class="widget-box">
						{$content}
					</div>					
				</div>
			</div>
			<div class="row-fluid">
				
			</div>
			<div class="row-fluid">
				<div id="footer" class="span12">
					
				</div>
			</div>
		</div>
		</div>

            <script src="themes/js/excanvas.min.js"></script>
            <script src="themes/js/jquery.min.js"></script>
            <script src="themes/js/jquery.ui.custom.js"></script>
            <script src="themes/js/bootstrap.min.js"></script>
            <script src="themes/js/jquery.flot.min.js"></script>
            <script src="themes/js/jquery.flot.resize.min.js"></script>
            <script src="themes/js/jquery.peity.min.js"></script>
            <script src="themes/js/fullcalendar.min.js"></script>
            <script src="themes/js/delta.js"></script>
            <script src="themes/js/delta.dashboard.js"></script>
			<script src="../js/custom.js"></script>
	</body>
</html>