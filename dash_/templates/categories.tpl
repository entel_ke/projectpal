<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Site Statistics</h5><div class="buttons"><a href="#" class="btn btn-mini"><i class="icon-refresh"></i> Update stats</a></div>
</div>
<div class="widget-content">
	<div class="row-fluid">
		<div class="span8 categories-area">
			<table class="table table-hover table-striped" id="tb-categories">
				<thead>
					<tr>
						<th width="5%"><input type="checkbox" class="check_all_categories"></th>
						<th width="70%">Category</th>
						<th width="25%" class="centered">Action</th>
					</tr>
				</thead>
				<tbody>
					{section start=0 step=1 name=row loop=$categories}
					<tr>
					<td><input type="checkbox" class="check_course" value="{$categories[row].id}"></td>
						<td class="edit-cat" id-holder="{$categories[row].id}">
							<a href="#" >{$categories[row].category}</a>
						</td>
						<td>
							<ul class="inline">
								<li class="btn btn-xs btn-primary">
									<i class="icon icon-pencil edit-cat" id-holder="{$categories[row].id}"><a class="hide"href="#" >{$categories[row].category}</a></i>
								</li>
								<li class="btn btn-xs btn-danger purge-category"><i class="icon icon-trash"></i></li>
							</ul>
							<input type="hidden" value="{$categories[row].id}" class="id_holder">
							
						</td>
					</tr>
					
					{/section}
				</tbody>
			</table>
		</div>
		<div class="span4 lifted">
			<div class="widget-title"><span class="icon"><i class="icon-plus"></i></span><h5 class="side-tbtitle">Add Category</h5></div>
				<div class="widget-content nopadding">
					<form id="category-form" method="post">
						<div class="form-group col-lg-6">
							<br>
							<input class="form-control input-area" type="hidden" name="protocol" id="protocol" value="0">
							<input class="form-control input-area" type="hidden" name="category_id" id="category_id" value="">
							<input class="form-control input-area" placeholder="category" name="category_name" id="category_name" value="">
							<br>
						</div>
						<br>
						<div class="clearfix"></div>
						<div class="form-group col-lg-6">
							<button type="submit" class="btn btn-primary" id="save-category" >Add</button>
							<button type="submit" class="btn btn-success hide" id="update-category" >Update</button>
						</div>
						<br>
					</form>
				</div>
			
		</div>	
	</div>							
</div>