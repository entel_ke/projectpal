<!DOCTYPE html>
<html lang="en">
	<head>
		<title>{$title}</title>
		<meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet/less" type="text/css" href="themes/less/bootstrap.less">
		<script src="themes/js/less/less.js" type="text/javascript"></script>
		<link rel="stylesheet" href="themes/style/fullcalendar.css" />	

		<link rel="stylesheet" href="themes/style/delta.main.css" />
		<link rel="stylesheet" href="themes/style/delta.grey.css"/>
		<link rel="icon" href="../css/logo_2.png" type="image/x-icon">
	</head>
	<body>
	<br>
	<div id="sidebar"> 
		<h3 id=""><span class="logo-holder"> <img src="../css/main-logo.png" width="40" height="40"</span>  ProjectPal</h3>  
		<a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
		<ul>
			<li class="active"><a href="./"><i class="icon icon-home"></i> <span>Home</span></a></li>
			<li><a href="categories.php"><i class="icon icon-th-list"></i> <span>Categories</span> <span class="label"></span></a></li>
			<li><a href="courses.php"><i class="icon icon-certificate"></i> <span>Courses</span> <span class="label"></span></a></li>
			<li class="submenu">
				<a href="#"><i class="icon icon-book"></i> <span>Projects</span> {if ($count_pending > 0)} <span class="label">{$count_pending}</span>{/if}</a>
				<ul>
					<li><a href="projects.php">Manage</a></li>
					<li><a href="pending.php">Pending</a></li>
					{*<li><a href="downloads.php">Downloads</a></li>*}
				</ul>
			</li>
			<li><a href="users.php"><i class="icon icon-user"></i> <span>Users</span> <span class="label"></span></a></li>
			<li><a href="inbox.php"><i class="icon icon-comment"></i> <span>Inbox</span>{if ($count_unread > 0)} <span class="label">{$count_unread}</span>{/if}</a></li>
			<li><a href="transactions.php"><i class="icon icon-tag"></i> <span>Transactions</span></a></li>
			
		</ul>
	</div>
	  <div id="mainBody">
			<h3>Admin
				<div class="pull-right">
				<a class="btn btn-large tip-bottom" title="Manage Projects" style="position:relative"><i class="icon-book"></i>
				{if ($count_pending > 0)}
				<span style="position:absolute; border-radius:12px; top:-23%; height:16px; width:16px" class="label label-important btn-success">{$count_pending}</span>
				{/if}
				</a>
				<a class="btn btn-large tip-bottom" title="Manage Users" style="position:relative"><i class="icon-user"></i></a>
				<a class="btn btn-large tip-bottom" title="Manage Messages" style="position:relative"><i class="icon-comment"></i>
				{if ($count_unread > 0)}
				<span style="position:absolute; border-radius:12px; top:-23%; height:16px; width:16px" class="label label-important">{$count_unread}</span>
				{/if}
				</a>
				<a class="btn btn-large tip-bottom" title="Manage Transactions"><i class="icon-tag"></i></a>
				<a class="btn btn-large" title="" href="#"><i class="icon icon-user"></i> <span>Profile</span></a>
				<!--<a class="btn btn-large" title="" href="#"><i class="icon icon-cog"></i> Settings</a>-->
				<a class="btn btn-large btn-danger" title="" href="../"><i class="icon-off"></i></a>
				</div>
			</h3>
		<div id="breadcrumb">
			<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Dashboard</a>
			<a href="#" class="current">{$main_head}</a>
		</div>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12 center" style="text-align: center;">					
					<ul class="stat-boxes">
						<li class="popover-categories">
							<div class="left peity_line_bad"><span>1,12,7</span>+10%</div>
							<div class="right">
								<strong class="ttl_categories">{$count_categories}</strong>
								Categories
							</div>
						</li>
						<li class="popover-courses">
							<div class="left peity_line_neutral"><span>1,12,7</span>+10%</div>
							<div class="right">
								<strong class="ttl_courses">{$count_courses}</strong>
								Courses
							</div>
						</li>
						<li class="popover-projects">
							<div class="left peity_line_good"><span>14,10,17</span>+10%</div>
								<strong class="hide approved_projects">{$approved_projects}</strong>
								<input type="hidden" class="td_projects" value="{$count_pending}"/>
							<div class="right">
								<strong class="ttl_projects">{$count_projects}</strong>
								Projects
							</div>
						</li>
						{*<li class="popover-visits">
							<div class="left peity_bar_good"><span>10,10,12</span>+40%</div>
							<div class="right">
								<strong>{$count_visits}</strong>
								Visits
							</div>
						</li>*}
						<li class="popover-users">
							<div class="left peity_bar_neutral"><span>20,15,18</span>0%</div>
							<div class="right">
								<strong class="ttl_users">{$count_users}</strong>
								Users
							</div>
						</li>
						{*
						<li class="popover-transactions">
							<div class="left peity_bar_bad"><span>12,20,10</span>-50%</div>
							<div class="right">
								<strong>{$count_transactions}</strong>
								Transactions
							</div>
						</li>
						*}
					</ul>
				</div>	
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="alert alert-info">
						Welcome <strong>{$smarty.session.user_name}</strong>
						<a href="#" data-dismiss="alert" class="close">×</a>
					</div>
					<div class="widget-box">
						<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Site Statistics</h5><div class="buttons btn-refresh"><a href="#" class="btn btn-mini" class="btn-refresh"><i class="icon-refresh"></i> Update stats</a></div></div>
						<div class="widget-content stats-content">
							<div class="row-fluid stats-holder">
							<div class="span4">
								<ul class="site-stats">
									<li><i class="icon-spin icon-spinner"></i><strong>{$count_pending}</strong> <small>Pending reviews</small></li>
									<li><i class="icon-download"></i> <strong>{$count_downloads}</strong> <small>Total Downloads</small></li>
									<li class="divider"></li>
									<li><i class="icon-th-list"></i> <strong>{$count_categories}</strong> <small>Manage Categories</small></li>
									<li><i class="icon-certificate"></i> <strong>{$count_courses}</strong> <small>Manage Courses</small></li>
									<li class="divider"></li>
									<li><i class="icon-user"></i> <strong>{$count_users}</strong> <small>Total Users</small></li>
									<li><i class="icon-arrow-right"></i> <strong>{$count_new_users}</strong> <small>New Users (Today's user)</small></li>
									
								</ul>
							</div>
							<div class="span8">
								<div class="widget-title"><span class="icon"><i class="icon-file"></i></span><h5>Unread Messages</h5><span title="unread" class="label label-info tip-left">{$count_unread}</span></div>
									<div class="widget-content nopadding">
										<ul class="recent-posts>
										{if ($count_unread > 0)}
										{section start=0 step=1 name=row loop=$unread_messages}
											<li>
												<div class="user-thumb">
													<img width="40" height="40" alt="User" src="./images/user.png">
												</div>
												<div class="article-post">
													<span class="user-info"> By: {$unread_messages[row].name} on {$unread_messages[row].date_saved}</span>
													<p>
														<a href="#">{$unread_messages[row].message}...</a>
													</p>
													
													<a class="btn btn-primary btn-mini" href="#" data-placement="right" data-original-title="Edit"><i class="icon-pencil"></i></a>
													<a class="btn btn-success btn-mini" href="#" data-placement="top" data-original-title="Approved"><i class="icon-ok"></i></a>
													<a class="btn btn-danger btn-mini" href="#" data-placement="left" data-original-title="Delete"><i class="icon-remove"></i></a>	
												</div>
											</li>
											{/section}
											{else}
											<li class="align-center"><br><div class="alert alert-info">No Unread messages</div></li>
											{/if}
										</ul>
									</div>
								</div>	
							</div>							
						</div>
					</div>					
				</div>
			</div>
			<div class="row-fluid">
				
			</div>
			<div class="row-fluid">
				<div id="footer" class="span12">
					
				</div>
			</div>
		</div>
		</div>

            <script src="themes/js/excanvas.min.js"></script>
            <script src="themes/js/jquery.min.js"></script>
            <script src="themes/js/jquery.ui.custom.js"></script>
            <script src="themes/js/bootstrap.min.js"></script>
            <script src="themes/js/jquery.flot.min.js"></script>
            <script src="themes/js/jquery.flot.resize.min.js"></script>
            <script src="themes/js/jquery.peity.min.js"></script>
            <script src="themes/js/fullcalendar.min.js"></script>
            <script src="themes/js/delta.js"></script>
            <script src="themes/js/delta.dashboard.js"></script>
			<script src="../js/custom.js"></script>
	</body>
</html>