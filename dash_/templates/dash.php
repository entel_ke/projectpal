<?php
	

?>

<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Admin Dashboard</title>
		<meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet/less" type="text/css" href="themes/less/bootstrap.less">
		<script src="themes/js/less/less.js" type="text/javascript"></script>
		<link rel="stylesheet" href="themes/style/fullcalendar.css" />	

		<link rel="stylesheet" href="themes/style/delta.main.css" />
		<link rel="stylesheet" href="themes/style/delta.grey.css"/>
	</head>
	<body>
	<br>
	<div id="sidebar"> 
		<h2 id=""><a href="index.php">ProjectPal</a></h2>  
		<a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
		<ul>
			<li class="active"><a href="index.php"><i class="icon icon-home"></i> <span>Dashboard</span></a></li>
			<li class="submenu">
				<a href="#"><i class="icon icon-th-list"></i> <span>Projects</span> <span class="label">3</span></a>
				<ul>
					<li><a href="#">Manage</a></li>
					<li><a href="#">Pending</a></li>
					<li><a href="#">Downloads</a></li>
				</ul>
			</li>
			<li class="submenu">
				<a href="#"><i class="icon icon-user"></i> <span>Users</span> <span class="label"></span></a>
				<ul>
					<li><a href="#">Manage</a></li>
					<li><a href="#">Add new</a></li>
				</ul>
			</li>
			<li><a href="#"><i class="icon icon-comment"></i> <span>Inbox</span> <span class="label">5</span></a></li>
			<li><a href="#"><i class="icon icon-tag"></i> <span>Transactions</span></a></li>
			
		</ul>
	</div>
	  <div id="mainBody">
			<h1>Dashboard
				<div class="pull-right">
				<a class="btn btn-large tip-bottom" title="Manage Projects" style="position:relative"><i class="icon-th-list"></i>
				<span style="position:absolute; border-radius:12px; top:-23%; height:16px; width:16px" class="label label-important btn-success">21</span></a>
				<a class="btn btn-large tip-bottom" title="Manage Users" style="position:relative"><i class="icon-user"></i>
				<span style="position:absolute; border-radius:12px; top:-23%; height:16px; width:16px" class="label label-important btn-primary">10</span></a>
				<a class="btn btn-large tip-bottom" title="Manage Messages" style="position:relative"><i class="icon-comment"></i>
				<span style="position:absolute; border-radius:12px; top:-23%; height:16px; width:16px" class="label label-important">112</span></a>
				<a class="btn btn-large tip-bottom" title="Manage Transactions"><i class="icon-tag"></i></a>
				<a class="btn btn-large" title="" href="#"><i class="icon icon-user"></i> <span>Profile</span></a>
				<!--<a class="btn btn-large" title="" href="#"><i class="icon icon-cog"></i> Settings</a>-->
				<a class="btn btn-large btn-danger" title="" href="../"><i class="icon-off"></i></a>
				</div>
			</h1>
		<div id="breadcrumb">
			<a href="#" title="Go to Home" class="tip-bottom"><i class="icon-home"></i> Home</a>
			<a href="#" class="current">Dashboard</a>
		</div>
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span12 center" style="text-align: center;">					
					<ul class="stat-boxes">
						<li class="popover-tickets">
							<div class="left peity_line_good"><span>14,10,17</span>+10%</div>
							<div class="right">
								<strong>68</strong>
								Projects
							</div>
						</li>
						<li class="popover-visits">
							<div class="left peity_bar_good"><span>10,10,12</span>+40%</div>
							<div class="right">
								<strong>24</strong>
								Visits
							</div>
						</li>
						<li class="popover-users">
							<div class="left peity_bar_neutral"><span>20,15,18</span>0%</div>
							<div class="right">
								<strong>1433</strong>
								Users
							</div>
						</li>
						<li class="popover-orders">
							<div class="left peity_bar_bad"><span>12,20,10</span>-50%</div>
							<div class="right">
								<strong>4808</strong>
								Transactions
							</div>
						</li>
						
					</ul>
				</div>	
			</div>
			<div class="row-fluid">
				<div class="span12">
					<div class="alert alert-info">
						Welcome <strong>Admin</strong>.!
						<a href="#" data-dismiss="alert" class="close">×</a>
					</div>
					<div class="widget-box">
						<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Site Statistics</h5><div class="buttons"><a href="#" class="btn btn-mini"><i class="icon-refresh"></i> Update stats</a></div></div>
						<div class="widget-content">
							<div class="row-fluid">
							<div class="span4">
								<ul class="site-stats">
									<li><i class="icon-spinner icon-spin"></i><strong>2</strong> <small>Pending reviews</small></li>
									<li><i class="icon-th-list"></i> <strong>259</strong> <small>Total Projects</small></li>
									<li><i class="icon-download"></i> <strong>4808</strong> <small>Total Downloads</small></li>
									<li class="divider"></li>
									<li><i class="icon-user"></i> <strong>123</strong> <small>Total Users</small></li>
									<li><i class="icon-arrow-right"></i> <strong>5</strong> <small>New Users (Today's user)</small></li>
									
								</ul>
							</div>
							<div class="span8">
								<div class="widget-title"><span class="icon"><i class="icon-file"></i></span><h5>Recent Messages</h5><span title="54 total posts" class="label label-info tip-left">54</span></div>
									<div class="widget-content nopadding">
										<ul class="recent-posts">
											<li>
												<div class="user-thumb">
													<img width="40" height="40" alt="User" src="themes/images/avatar2.png">
												</div>
												<div class="article-post">
													<span class="user-info"> By: neytiri on 2 Aug 2012, 09:27 AM, IP: 186.56.45.7 </span>
													<p>
														<a href="#">Vivamus sed auctor nibh congue, ligula vitae tempus pharetra...</a>
													</p>
												<a class="btn btn-primary btn-mini" href="#" data-placement="right" data-original-title="Edit"><i class="icon-pencil"></i></a>
													<a class="btn btn-success btn-mini" href="#" data-placement="top" data-original-title="Approved"><i class="icon-ok"></i></a>
													<a class="btn btn-danger btn-mini" href="#" data-placement="left" data-original-title="Delete"><i class="icon-remove"></i></a>	
												</div>
											</li>
											<li>
												<div class="user-thumb">
													<img width="40" height="40" alt="User" src="themes/images/avatar3.png">
												</div>
												<div class="article-post">
													<span class="user-info"> By: john on on 24 Jun 2012, 04:12 PM, IP: 192.168.24.3 </span>
													<p>
														<a href="#">Vivamus sed auctor nibh congue, ligula vitae tempus pharetra...</a>
													</p>
													<a class="btn btn-primary btn-mini" href="#" data-placement="right" data-original-title="Edit"><i class="icon-pencil"></i></a>
													<a class="btn btn-success btn-mini" href="#" data-placement="top" data-original-title="Approved"><i class="icon-ok"></i></a>
													<a class="btn btn-danger btn-mini" href="#" data-placement="left" data-original-title="Delete"><i class="icon-remove"></i></a>	
												</div>
											</li>
											<li>
												<div class="user-thumb">
													<img width="40" height="40" alt="User" src="themes/images/avatar1.png">
												</div>
												<div class="article-post">
													<span class="user-info"> By: michelle on 22 Jun 2012, 02:44 PM, IP: 172.10.56.3 </span>
													<p>
														<a href="#">Vivamus sed auctor nibh congue, ligula vitae tempus pharetra...</a>
													</p>
													<a class="btn btn-primary btn-mini" href="#" data-placement="right" data-original-title="Edit"><i class="icon-pencil"></i></a>
													<a class="btn btn-success btn-mini" href="#" data-placement="top" data-original-title="Approved"><i class="icon-ok"></i></a>
													<a class="btn btn-danger btn-mini" href="#" data-placement="left" data-original-title="Delete"><i class="icon-remove"></i></a>
													</div>
											</li>
											<li class="viewall">
												<a title="View all posts" class="tip-top" href="#"> + View all + </a>
											</li>
										</ul>
									</div>
								</div>	
							</div>							
						</div>
					</div>					
				</div>
			</div>
			<div class="row-fluid">
				
			</div>
			<div class="row-fluid">
				<div id="footer" class="span12">
					
				</div>
			</div>
		</div>
		</div>

            <script src="themes/js/excanvas.min.js"></script>
            <script src="themes/js/jquery.min.js"></script>
            <script src="themes/js/jquery.ui.custom.js"></script>
            <script src="themes/js/bootstrap.min.js"></script>
            <script src="themes/js/jquery.flot.min.js"></script>
            <script src="themes/js/jquery.flot.resize.min.js"></script>
            <script src="themes/js/jquery.peity.min.js"></script>
            <script src="themes/js/fullcalendar.min.js"></script>
            <script src="themes/js/delta.js"></script>
            <script src="themes/js/delta.dashboard.js"></script>
	</body>
</html>