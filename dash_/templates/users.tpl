<div class="widget-title"><span class="icon"><i class="icon-signal"></i></span><h5>Site users</h5><div class="buttons"></div>
</div>
<div class="widget-content">
	<div class="row-fluid">
		<div class="span12">
			<table class="table" id="tb-users">
				<thead>
					<tr>
						<th width="5%" class="centered"><input type="checkbox" class="check_all_categories"></th>
						<th width="35%" class=""> Select all</th>
						<th width="15%" class="centered"></th>
						<th width="15%" class="centered"></th>
						<th width="15%" class="centered"></th>
						<th width="15%" class="centered"></th>
					</tr>
				</thead>
				<tbody>
					{section start=0 step=1 name=row loop=$users}
						
						<tr class="row-user">
							<td class="td-lgrey centered">
								<input type="checkbox" class="check_course" value="{$users[row].id}">
							</td>
							<td class="td-lgrey">
								<a href="edit_user.php" >{$users[row].fname} {$users[row].lname}</a>
								{if ($users[row].user_level ==2)}<span class="pull-right"><i class="btn btn-sm btn-success">Admin</i></span>{/if}
								{if ($users[row].active ==1)}
									<span class="pull-right"><i class="btn btn-sm btn-default">Active</i></span>
								{else}
									<span class="pull-right"><i class="btn btn-sm btn-primary">Activate</i></span>
								{/if}
							</td>
							<td  class="td-dblue centered">
								2
								<h4>Projects</h4>
							</td>
							<td class="td-lblue centered">
								3
								<h4>Approved</h4>
							</td>
							<td class="td-dblue centered">
								2
								<h4>Downloads</h4>
							</td>
							<td class="td-lblue centered">
								KES 3000
								<h4>Revenue</h4>
							</td>
						</tr>
					
					{/section}
				</tbody>
			</table>
		</div>
</div>