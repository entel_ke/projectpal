<?php /* Smarty version Smarty-3.1.14, created on 2015-03-26 10:37:59
         compiled from ".\templates\cat_projects.tpl" */ ?>
<?php /*%%SmartyHeaderCode:216785513b7576c6449-12934620%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'bfeef9fe11e7e8c9595864b2962f7a4f75cf734e' => 
    array (
      0 => '.\\templates\\cat_projects.tpl',
      1 => 1427293713,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '216785513b7576c6449-12934620',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'category' => 0,
    'count_category_projects' => 0,
    'category_projects' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5513b7577a59f6_06649856',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5513b7577a59f6_06649856')) {function content_5513b7577a59f6_06649856($_smarty_tpl) {?><div class="box-header">
  <i class="fa fa-file"></i>
  <h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['category']->value;?>
 projects</h3>
  <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
    <?php if (($_smarty_tpl->tpl_vars['count_category_projects']->value>0)){?> 
	    <ul class="pagination pagination-sm inline">
	      <li><a href="#">&laquo;</a></li>
	      <li><a href="#">1</a></li>
	      <li><a href="#">2</a></li>
	      <li><a href="#">3</a></li>
	      <li><a href="#">&raquo;</a></li>
	    </ul>
    <?php }?>
  </div>
</div>
<div class="box-body" id="projects">
  <ul class="todo-list">
	  <?php if (($_smarty_tpl->tpl_vars['count_category_projects']->value>0)){?> 
	  	<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['row'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['row']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['name'] = 'row';
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['category_projects']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total']);
?>
		    <li>
		      <span class="handle">
		        <i class="fa fa-ellipsis-v"></i>
		        <i class="fa fa-ellipsis-v"></i>
		      </span>
		      <span class="text"><a href="view_project.php?spec_id=<?php echo $_smarty_tpl->tpl_vars['category_projects']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['spec_id'];?>
"><?php echo $_smarty_tpl->tpl_vars['category_projects']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['project_title'];?>
</a></span><br>
		      <p>
		      	<?php echo $_smarty_tpl->tpl_vars['category_projects']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['category'];?>
<br>
				<span class="green"></span>
		      	<span class="tools pull-right">
		            <button class="btn btn-success"><i class="fa fa-download"> </i> <?php if (($_smarty_tpl->tpl_vars['category_projects']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['paid']==1)){?>Download <?php }else{ ?> Purchase <?php }?></button>
		          </span>
		      </p>
		      <small class="label <?php if (($_smarty_tpl->tpl_vars['category_projects']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['paid']==1)){?>label-info <?php }else{ ?> label-danger <?php }?>"><i class="fa fa-money"></i> <?php if (($_smarty_tpl->tpl_vars['category_projects']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['paid']==1)){?>Free <?php }else{ ?> KSH. <?php echo $_smarty_tpl->tpl_vars['category_projects']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['fee'];?>
 <?php }?></small>
		      
		    </li>
	    <?php endfor; endif; ?>
	 <?php }else{ ?>
	 	<div class="callout callout-info no-margin">
	 		Sorry, projects under this category will be made available soon
        </div>
	 <?php }?>   
    
  </ul>
</div><!-- /.projects -->
<div class="box-footer clearfix no-border">
  <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Upload Project</button>
</div><?php }} ?>