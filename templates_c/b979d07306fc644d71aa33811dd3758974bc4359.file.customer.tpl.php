<?php /* Smarty version Smarty-3.1.14, created on 2015-05-02 09:07:39
         compiled from ".\templates\customer.tpl" */ ?>
<?php /*%%SmartyHeaderCode:9885543cd320c4583-79556219%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'b979d07306fc644d71aa33811dd3758974bc4359' => 
    array (
      0 => '.\\templates\\customer.tpl',
      1 => 1430557516,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '9885543cd320c4583-79556219',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5543cd3253ce96_19543155',
  'variables' => 
  array (
    'title' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5543cd3253ce96_19543155')) {function content_5543cd3253ce96_19543155($_smarty_tpl) {?><!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <meta property="fb:app_id" content="676854189038131" />

    <!-- Bootstrap 3.3.2 -->
    <link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />  
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
     <!-- font-awesome --> 
    <link href="font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<!-- Theme styles -->
    <link href="dist/css/custom.min.css" rel="stylesheet" type="text/css" />
    <link href="dist/css/style.css" rel="stylesheet" type="text/css" />
    
    <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
    <!-- Morris chart -->
    <link href="plugins/morris/morris.css" rel="stylesheet" type="text/css" />
    <!-- jvectormap -->
    <link href="plugins/jvectormap/jquery-jvectormap-1.2.2.css" rel="stylesheet" type="text/css" />
    <!-- Date Picker -->
    <link href="plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
    <!-- Daterange picker -->
    <link href="plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet" type="text/css" />
    <!-- bootstrap wysihtml5 - text editor -->
    <link href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
    <script type='text/javascript' src='http://projectpal.co.ke/adserver/www/delivery/spcjs.php?id=1'></script>
	<link rel="icon" href="dist/img/logo_2.png" type="image/x-icon">
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
    <script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-60273234-1', 'auto');
		  ga('send', 'pageview');
		  
		</script>
		
	
  </head>
    <body class="skin-blue">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
          var js, fjs = d.getElementsByTagName(s)[0];
          if (d.getElementById(id)) return;
          js = d.createElement(s); js.id = id;
          js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=676854189038131";
          fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

        <div class="wrapper" style="background-color:#fff !important;">
             <header class="main-header">
        <!-- Logo -->
        <a href="./" class="logo">
            <img src="dist/img/logo_2.png" class="img-circle circle-small" alt="User Image"/>
            <b>Project</b>Pal
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="./">
                  Home
                </a>
              </li>
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="projects.php">
                  Projects
                </a>
              </li>
              
              <li class="dropdown messages-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Past Papers
                </a>
                <ul class="dropdown-menu">
                  <li>
                    <!-- inner menu: contains the actual data -->
                    <ul class="menu">
                      
                      <li>
                        <a href="past_papers/sec_sch/">
                          <i class="fa fa-users text-yellow"> </i> Secondary school
                         </a> 
                      </li>
                      <li>
                        <a href="past_papers/ter_sch/">
                          <i class="fa fa-users text-red"> </i> Higher Education
                        </a>
                      </li>
                    </ul>
                  </li>
                </ul>
              </li>
              
              <?php if (isset($_SESSION['user_id'])){?>
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="user_area/img/users/128/<?php echo $_SESSION['thumbnail'];?>
" class="user-image" alt="User Image"/>
                  <span class="hidden-xs">Uweys Makaweys</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="user_area/img/users/128/<?php echo $_SESSION['thumbnail'];?>
" class="img-circle" alt="User Image" />
                    <p>
                      Uweys Makaweys - Web Developer
                      <small><?php echo $_SESSION['user_email'];?>
</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--
                  <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>-->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="./user_area/pages/" class="btn btn-default btn-flat">Account</a>
                    </div>
                    <div class="pull-right">
                      <a href="./logout.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <?php }else{ ?>
                <li class="">
                    <a href="./user_area/pages/login.php">
                      Sign In / Register
                    </a>
                  </li>
              <?php }?>
            </ul>
          </div>
        </nav>
      </header>
            <div style="margin:0 auto; width:50%; margin-top:4%;">
                <div class="panel panel-default">
                    <div style="padding-left:2%">
                        <h3>Your Feedback Matters to us.</h3>
                        <hr>
                    </div>
                    <div class="panel-body">
                        <form id="">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" name="name" id="name" class="form-control"/>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="email">Email</label>
                                <input type="text" class="form-control" name="email" id="email" required=''/>
                                <span class="glyphicon glyphicon-user form-control-feedback"> </span>
                            </div>
                            <div class="form-group has-feedback">
                                <label for="feedback">Feedback</label>
                                <textarea class="form-control" id="feedback" name="feedback"></textarea>
                                <span class="glyphicon glyphicon-user form-control-feedback"> </span>
                            </div>
                            <input type="button" class="btn btn-success btn-lg" value="Send Feedback" id="send-fdb-btn"/>
                        </form>

                        <div class="social-media">
                            <p>
                                <hr/>
                                <h3> Or Catch us On Facebook</h3>
                                
                            </p>
                           <div class="fb-comments" data-href="http://www.projectpal.co.ke/customer.php" data-numposts="10" data-colorscheme="light"></div>

                        </div>

                    </div>
                </div>

            </div>
        </div>
         <script src="plugins/jQuery/jQuery-2.1.3.min.js"></script>

         <!-- CSS -->
         <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.4.0/css/alertify.min.css"/>
        <!-- Bootstrap theme -->
        <link rel="stylesheet" href="//cdn.jsdelivr.net/alertifyjs/1.4.0/css/themes/bootstrap.min.css"/>
         <!-- Alertify JavaScript -->
        <script src="//cdn.jsdelivr.net/alertifyjs/1.4.0/alertify.min.js"></script> 
        
        <script type="text/javascript">
            $("#send-fdb-btn").click(function(){
                var name=$("#name").val();
                var email=$("#email");
                var feedback=$("#feedback").val();

                if(name=="" || email=="" || feedback==""){
                    alertify.error("Fill in all fields");
                    //$().attr("disabled","true");


                }else{
                    $.ajax({
                        url: './admin/ajax/feedback.php',
                        type: 'POST',
                        // dataType: 'json',
                        data: {
                            name:name ,
                            email:email,
                            feedback:feedback
                        },
                    })
                    .done(function(data) {
                        console.log(data);
                    })
                    .fail(function() {
                        console.log("error");
                    })
                }


            });

        </script>
        
    </body> 
    </html>

<?php }} ?>