<?php /* Smarty version Smarty-3.1.14, created on 2015-05-01 05:37:24
         compiled from ".\templates\splash.tpl" */ ?>
<?php /*%%SmartyHeaderCode:867553d32b0dd9d87-93848602%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2bc7d3aa0b42a54391c203f98d93b7bdfe106714' => 
    array (
      0 => '.\\templates\\splash.tpl',
      1 => 1430458561,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '867553d32b0dd9d87-93848602',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_553d32b0f20405_83812585',
  'variables' => 
  array (
    'title' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_553d32b0f20405_83812585')) {function content_553d32b0f20405_83812585($_smarty_tpl) {?><!DOCTYPE HTML>

<html>
	<head>
		<title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
		<meta http-equiv="content-type" content="text/html; charset=utf-8" />
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
		<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
		<script src="js/skel.min.js"></script>
		<script src="js/init.js"></script>
		<link href="css/font-awesome.min.css" rel="stylesheet" type="text/css">
		<link rel="icon" href="dist/img/logo_2.png" type="image/x-icon">
		
		<noscript>
			<link rel="stylesheet" href="css/skel.css" />
			<link rel="stylesheet" href="css/style.css" />
			<link rel="stylesheet" href="css/style-wide.css" />
			<link rel="stylesheet" href="css/style-noscript.css" />
		</noscript>
		<!--[if lte IE 9]><link rel="stylesheet" href="css/ie/v9.css" /><![endif]-->
		<!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
	</head>
	<body class="loading">
		<div id="wrapper">
			<div id="bg"></div>
			<div id="overlay"></div>
			<div id="main">

				<!-- Header -->
					<header id="header">
						<h1>ProjectPal</h1>
						<p>Friendly &nbsp;&bull;&nbsp; Educational &nbsp;&bull;&nbsp; Partner</p>
						<nav>
							<ul>
								<li><a href="./user_area/pages/upload_project.php" class="btn bg-navy">Upload Project</a></li>
								<li><a href="./user_area/pages/login.php"  class="btn btn-primary">Sign In</a></li>
								<li><a href="./projects.php"  class="btn bg-navy">Get Projects</a></li>
							</ul>
							<ul>
								<li></li>
								<li><a href="./user_area/pages/user_reg.php"  class="btn btn-success">Create Account</a></li>
								<li></li>
								
							</ul>
						</nav>
					</header>

				<!-- Footer -->
					<footer id="footer">
						<span class="copyright"> Copyright &copy; <a href="http://projectpal.co.ke">ProjectPal</a>.</span>
					</footer>
				
			</div>
		</div>
	</body>
</html><?php }} ?>