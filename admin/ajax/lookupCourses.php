<?php

	require "../../include/config.php";
	
	$Obj = new Courses ();
    
	$search = isset($_POST['category']) ? $_POST['category'] : '';
	
	$res = array ("success"=>0, "data"=>0);
	$res ['data'] = $Obj->getCourses($search);
	
	if (sizeof($res ['data'])>0) {
		$res ['success'] = 1;
	}
     
	echo json_encode($res);
                
