<?php

	require "../../include/config.php";
	
	$dir = "../../user_area/img/users/";
	$formElement='file';
	
	$img = new Images();
    $img->openImage($dir, $formElement);
    $imgName = $img->NewImageName;
    $imgMime = ltrim($img->imageMimeType, ".");
	
	$_SESSION['thumb'] = $imgName;