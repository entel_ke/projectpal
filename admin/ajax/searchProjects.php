<?php

	require "../../include/config.php";
	
	$Obj = new Projects ();
    $search = isset($_REQUEST['query']) ? $_REQUEST['query'] : '';
	
	$res = array ("success"=>0, "data"=>0);
	$res ['data'] = $Obj->lookupProjects($search);
	
	if (sizeof($res ['data'])>0) {
		$res ['success'] = 1;
	}
     
	echo json_encode($res);
                
