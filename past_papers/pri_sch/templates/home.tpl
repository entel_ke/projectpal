<div class="box-header">
  <i class="fa fa-file"></i>
  <h3 class="box-title">Past Papers</h3>
  <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
    <ul class="pagination pagination-sm inline">
      <li><a href="#">&laquo;</a></li>
      <li><a href="#">1</a></li>
      <li><a href="#">2</a></li>
      <li><a href="#">3</a></li>
      <li><a href="#">&raquo;</a></li>
    </ul>
  </div>
</div>
<div class="box-body" id="projects">
  <ul class="todo-list">
  	{section start=0 step=1 name=row loop=$papers}
	    <li>
	      <span class="handle">
	        <i class="fa fa-ellipsis-v"></i>
	        <i class="fa fa-ellipsis-v"></i>
	      </span>
	      <span class="text"><a href="view_paper.php?spec_id={$papers[row].id}">{$papers[row].title}</a></span><br>
	      <p>
	      	{$papers[row].subject}<br>
			<span class="green">{$papers[row].level_name}</span>
	      	<span class="tools pull-right">
	            <button class="btn btn-success"><i class="fa fa-download"> </i> {if ($papers[row].paid == 1)}Download {else} Purchase {/if}</button>
	          </span>
	      </p>
	      
	    </li>
    {/section}
    
  </ul>
</div><!-- /.projects -->