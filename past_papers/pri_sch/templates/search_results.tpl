<div class="box-header">
  <i class="fa fa-file"></i>
  <h3 class="box-title"><i>{$count_results}</i> project(s) found matching <i> "{$query}"</i></h3>
  <div class="box-tools pull-right" data-toggle="tooltip" title="Status">
    {if ($count_results > 0)} 
	    <ul class="pagination pagination-sm inline">
	      <li><a href="#">&laquo;</a></li>
	      <li><a href="#">1</a></li>
	      <li><a href="#">2</a></li>
	      <li><a href="#">3</a></li>
	      <li><a href="#">&raquo;</a></li>
	    </ul>
    {/if}
  </div>
</div>
<div class="box-body" id="projects">
  <ul class="todo-list">
  {if ($count_results > 0)} 
  	{section start=0 step=1 name=row loop=$result_projects}
	    <li>
	      <span class="handle">
	        <i class="fa fa-ellipsis-v"></i>
	        <i class="fa fa-ellipsis-v"></i>
	      </span>
	      <span class="text"><a href="view_project.php?spec_id={$result_projects[row].spec_id}">{$result_projects[row].project_title}</a></span><br>
	      <p>
	      	{$result_projects[row].category}<br>
			<span class="green"></span>
	      	<span class="tools pull-right">
	            <button class="btn btn-success"><i class="fa fa-download"> </i> {if ($result_projects[row].paid == 1)}Download {else} Purchase {/if}</button>
	          </span>
	      </p>
	      <small class="label {if ($result_projects[row].paid == 1)}label-info {else} label-danger {/if}"><i class="fa fa-money"></i> {if ($result_projects[row].paid == 1)}Free {else} KSH. {$result_projects[row].fee} {/if}</small>
	      
	    </li>
    {/section}
  	{else}
	 	<div class="callout callout-info no-margin">
	 		Sorry, No projects Found
        </div>
	 {/if}  
    
  </ul>
</div><!-- /.projects -->
<div class="box-footer clearfix no-border">
  <button class="btn btn-default pull-right"><i class="fa fa-plus"></i> Upload Project</button>
</div>