<div class="box-header">
  <h3 class="box-title">{$course_name} units (<i>Select unit to view past papers</i>)</h3>
  <div class="box-tools">
    
  </div>
</div><!-- /.box-header -->
<div class="box-body table-responsive no-padding">
  {if ($count_units > 0)}
  <table class="table table-hover">
  	<thead>
        <tr>
          <th width="1005%" class=""></th>
          {*<th width="15%"></th>
          <th width="30%"></th>*}
        </tr>
    </thead> 
  	 <tbody> 
    	{section start=0 step=1 name=row loop=$course_units}	   
            <tr>
              <td>
              	<a href="unit_pastpapers.php?id={$course_units[row].id}">
              		<label>{$course_units[row].acronym} : </label>{$course_units[row].subject_name}
              	</a>
              </td>
              {*<td>{$course_units[row].level_name}</td>
              <td>
              	<a href="preview_papers.php?file={$course_units[row].file_name}&stage=2" class="btn btn-default btn-xs">Preview</a>
              	<a href="download_papers.php?id={$course_units[row].id}" class="btn btn-success btn-xs">Download Pdf</a>
              </td>*}
            </tr>
        {/section}
    </tbody>
  </table>
  {else}
  	<div class="callout callout-info no-margin">
 		Sorry, No units available for this course
    </div>
  {/if}
</div><!-- /.box-body -->