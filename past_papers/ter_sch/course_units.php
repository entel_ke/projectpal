<?php
	require "../../include/config.php";
	
	if (isset($_GET['id'])) {
		$course_id = $_GET['id'];
	} else {
		header('location:./');
		exit;
	}	
		
	$Obj = new classMain();
	$Obj_papers = new pastPapers();
	
	$stage_id = 3;
	
	//Get courses variables
	$courses = $Obj_papers->get_stage_courses($stage_id);
	$Obj_papers->get_course($course_id);
	$course_name = $Obj_papers->course_name;
	
	//Get units/ by course
	$course_units = $Obj_papers->get_units_by_course($course_id, $stage_id);
	$count_units = sizeof($course_units);
	
	//Get Papers variables
	//$papers = $Obj_papers->get_pastpapers('',$stage_id);
	//$count_papers = sizeof($papers);
	
	//print_r('<pre>');
	//print_r($course_units);
//	exit;
	
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | Higher Education - PastPapers');
	$smarty->assign('courses', $courses);
	$smarty->assign('course_name', $course_name);
	$smarty->assign('course_units', $course_units);
	//$smarty->assign('papers', $papers);
	$smarty->assign('count_units', $count_units);
	$content = $smarty->fetch('./templates/course_units.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main_ter.tpl');

?>