<?php
	require "../../include/config.php";
		
	$Obj = new classMain();
	$Obj_papers = new pastPapers();
	
	$stage_id = 3;
	
	//Get courses variables
	$courses = $Obj_papers->get_stage_courses($stage_id);
	
	
	//Get units/ by course
	//$course_units = $Obj_papers->get_units_by_course($course_id, $stage_id);
	//Get Papers variables
	$papers = $Obj_papers->get_pastpapers('',$stage_id);
	$count_papers = sizeof($papers);
	
	//print_r('<pre>');
	//print_r($projects);
	//exit;
	
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | Higher Education - PastPapers');
	$smarty->assign('courses', $courses);
	$smarty->assign('papers', $papers);
	$smarty->assign('count_papers', $count_papers);
	$content = $smarty->fetch('./templates/home.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main_ter.tpl');

?>