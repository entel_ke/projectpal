<?php
	require "../../include/config.php";
	
	if (isset($_GET['file']) && isset($_GET['stage'])) {
		$file = $_GET['file'];
		$stage = $_GET['stage'];
	} else {
		header('location:./');
		exit;
	}
	
	if ($stage == 2) {
		$stage_name = 'stage_2';
	} else if ($stage == 3) {
		$stage_name = 'stage_3';
	}
	
	$Obj = new classMain();
	$Obj_papers = new pastPapers();
	
	$stage_id = 3;
	
	//Get courses variables
	$courses = $Obj_papers->get_stage_courses($stage_id);
	
	
	//Get units/ by course
	//$course_units = $Obj_papers->get_units_by_course($course_id, $stage_id);
	//Get Papers variables
	$papers = $Obj_papers->get_pastpapers('',$stage_id);
	$count_papers = sizeof($papers);
	
	//print_r('<pre>');
	//print_r($projects);
	//exit;
	
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | Higher Education - PastPapers');
	$smarty->assign('courses', $courses);
	$smarty->assign('papers', $papers);
	$smarty->assign('count_papers', $count_papers);
	$smarty->assign('stage_name', $stage_name);
	$smarty->assign('file', $file);
	//$content = $smarty->fetch('./templates/prev.tpl');
	//$smarty->assign('content', $content);
	
	$smarty->display('./templates/main_prev.tpl');
	
?>