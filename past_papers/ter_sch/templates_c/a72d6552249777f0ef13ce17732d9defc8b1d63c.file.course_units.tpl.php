<?php /* Smarty version Smarty-3.1.14, created on 2015-03-26 15:16:56
         compiled from ".\templates\course_units.tpl" */ ?>
<?php /*%%SmartyHeaderCode:269905513f58bf34663-94702274%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    'a72d6552249777f0ef13ce17732d9defc8b1d63c' => 
    array (
      0 => '.\\templates\\course_units.tpl',
      1 => 1427372148,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '269905513f58bf34663-94702274',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5513f58c0bd530_97199748',
  'variables' => 
  array (
    'course_name' => 0,
    'count_units' => 0,
    'course_units' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5513f58c0bd530_97199748')) {function content_5513f58c0bd530_97199748($_smarty_tpl) {?><div class="box-header">
  <h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['course_name']->value;?>
 units (<i>Select unit to view past papers</i>)</h3>
  <div class="box-tools">
    
  </div>
</div><!-- /.box-header -->
<div class="box-body table-responsive no-padding">
  <?php if (($_smarty_tpl->tpl_vars['count_units']->value>0)){?>
  <table class="table table-hover">
  	<thead>
        <tr>
          <th width="1005%" class=""></th>
          
        </tr>
    </thead> 
  	 <tbody> 
    	<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['row'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['row']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['name'] = 'row';
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['course_units']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total']);
?>	   
            <tr>
              <td>
              	<a href="unit_pastpapers.php?id=<?php echo $_smarty_tpl->tpl_vars['course_units']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['id'];?>
">
              		<label><?php echo $_smarty_tpl->tpl_vars['course_units']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['acronym'];?>
 : </label><?php echo $_smarty_tpl->tpl_vars['course_units']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['subject_name'];?>

              	</a>
              </td>
              
            </tr>
        <?php endfor; endif; ?>
    </tbody>
  </table>
  <?php }else{ ?>
  	<div class="callout callout-info no-margin">
 		Sorry, No units available for this course
    </div>
  <?php }?>
</div><!-- /.box-body --><?php }} ?>