<?php
	require "../../include/config.php";
	
	if (isset($_GET['id'])) {
		$unit_id = $_GET['id'];
	} else {
		header('location:./');
		exit;
	}	
	$Obj = new classMain();
	$Obj_papers = new pastPapers();
	
	$stage_id = 3;
	
	//Get courses variables
	$courses = $Obj_papers->get_stage_courses($stage_id);
	
		
	//Get unit
	$unit = $Obj_papers->get_unit_subject_by_id($unit_id);
	$unit_title = $Obj_papers->subject_title;
	
	//Get Papers variables
	$all = $Obj_papers->get_pastpapers($unit_id, $stage_id);
	$papers = $Obj_papers->subject_papers;
	$count_papers = sizeof($papers);
	
	//print_r('<pre>');
	//print_r($projects);
	//exit;
	
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | Higher Education - PastPapers');
	$smarty->assign('top_logo', 'Projects');
	$smarty->assign('courses', $courses);
	$smarty->assign('unit_title', $unit_title);
	$smarty->assign('papers', $papers);
	$smarty->assign('count_papers', $count_papers);
	$content = $smarty->fetch('./templates/unit_papers.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main_ter.tpl');

?>