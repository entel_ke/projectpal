<?php /* Smarty version Smarty-3.1.14, created on 2015-03-26 14:29:01
         compiled from ".\templates\subject_papers.tpl" */ ?>
<?php /*%%SmartyHeaderCode:82655513ed7dad58f6-16289232%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '2188ad7a4ff98b93b421edff92bb3187d760d275' => 
    array (
      0 => '.\\templates\\subject_papers.tpl',
      1 => 1427368274,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '82655513ed7dad58f6-16289232',
  'function' => 
  array (
  ),
  'variables' => 
  array (
    'subject_title' => 0,
    'count_papers' => 0,
    'papers' => 0,
  ),
  'has_nocache_code' => false,
  'version' => 'Smarty-3.1.14',
  'unifunc' => 'content_5513ed7db97194_27713351',
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_5513ed7db97194_27713351')) {function content_5513ed7db97194_27713351($_smarty_tpl) {?><div class="box-header">
  <h3 class="box-title"><?php echo $_smarty_tpl->tpl_vars['subject_title']->value;?>
 Past Papers <i>(<?php echo $_smarty_tpl->tpl_vars['count_papers']->value;?>
)</i></h3>
  <div class="box-tools">
    
  </div>
</div><!-- /.box-header -->
<div class="box-body table-responsive no-padding">
  <?php if (($_smarty_tpl->tpl_vars['count_papers']->value>0)){?>
  <table class="table table-hover">
  	
  	<thead>
        <tr>
          <th width="55%">Title</th>
          <th width="15%">Class</th>
          <th width="30%">Action</th>
        </tr>
    </thead>    
    <tbody> 
    	<?php if (isset($_smarty_tpl->tpl_vars['smarty']->value['section']['row'])) unset($_smarty_tpl->tpl_vars['smarty']->value['section']['row']);
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] = (int)0;
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] = ((int)1) == 0 ? 1 : (int)1;
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['name'] = 'row';
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] = is_array($_loop=$_smarty_tpl->tpl_vars['papers']->value) ? count($_loop) : max(0, (int)$_loop); unset($_loop);
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show'] = true;
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['max'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'];
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] < 0)
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] = max($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] > 0 ? 0 : -1, $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start']);
else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] = min($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop']-1);
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show']) {
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'] = min(ceil(($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'] > 0 ? $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['loop'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'] : $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start']+1)/abs($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'])), $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['max']);
    if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'] == 0)
        $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show'] = false;
} else
    $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'] = 0;
if ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['show']):

            for ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['start'], $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] = 1;
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] <= $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total'];
                 $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] += $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'], $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration']++):
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['rownum'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index_prev'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] - $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index_next'] = $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['index'] + $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['step'];
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['first']      = ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] == 1);
$_smarty_tpl->tpl_vars['smarty']->value['section']['row']['last']       = ($_smarty_tpl->tpl_vars['smarty']->value['section']['row']['iteration'] == $_smarty_tpl->tpl_vars['smarty']->value['section']['row']['total']);
?>	   
            <tr>
              <td><a href="preview_papers.php?file=<?php echo $_smarty_tpl->tpl_vars['papers']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['file_name'];?>
&stage=2"><?php echo $_smarty_tpl->tpl_vars['papers']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['title'];?>
</a><br>
              <label>Subject:</label><?php echo $_smarty_tpl->tpl_vars['papers']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['subject_name'];?>

              </td>
              <td><?php echo $_smarty_tpl->tpl_vars['papers']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['level_name'];?>
</td>
              <td>
              	<a href="preview_papers.php?file=<?php echo $_smarty_tpl->tpl_vars['papers']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['file_name'];?>
&stage=2" class="btn btn-default btn-xs">Preview</a>
              	<a href="download_papers.php?id=<?php echo $_smarty_tpl->tpl_vars['papers']->value[$_smarty_tpl->getVariable('smarty')->value['section']['row']['index']]['id'];?>
" class="btn btn-success btn-xs">Download Pdf</a>
              </td>
            </tr>
        <?php endfor; endif; ?>
    </tbody>
  </table>
  <?php }else{ ?>
  	<div class="callout callout-info no-margin">
 		Sorry, Past papers under this subject will be made available soon
    </div>
  <?php }?>
</div><!-- /.box-body --><?php }} ?>