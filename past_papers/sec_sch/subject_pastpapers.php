<?php
	require "../../include/config.php";
	
	if (isset($_GET['id'])) {
		$subject_id = $_GET['id'];
	} else {
		header('location:./');
		exit;
	}	
	$Obj = new classMain();
	$Obj_papers = new pastPapers();
	
	$stage_id = 2;
	
	//Get subjects variables
	$subjects = $Obj_papers->get_subjects($stage_id);
	
	//Get subject
	$subject = $Obj_papers->get_unit_subject_by_id($subject_id, $stage_id);
	$subject_title = $Obj_papers->subject_title;
	//Get Papers variables
	$all = $Obj_papers->get_pastpapers($subject_id);
	$papers = $Obj_papers->subject_papers;
	
	$count_papers = sizeof($papers);
	
	//print_r('<pre>');
	//print_r($projects);
	//exit;
	
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | High schools - PastPapers');
	$smarty->assign('top_logo', 'Projects');
	$smarty->assign('subjects', $subjects);
	$smarty->assign('subject_title', $subject_title);
	$smarty->assign('papers', $papers);
	$smarty->assign('count_papers', $count_papers);
	$content = $smarty->fetch('./templates/subject_papers.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main_sec.tpl');

?>