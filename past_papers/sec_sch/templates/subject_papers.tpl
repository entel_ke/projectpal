<div class="box-header">
  <h3 class="box-title">{$subject_title} Past Papers <i>({$count_papers})</i></h3>
  <div class="box-tools">
    
  </div>
</div><!-- /.box-header -->
<div class="box-body table-responsive no-padding">
  {if ($count_papers > 0)}
  <table class="table table-hover">
  	
  	<thead>
        <tr>
          <th width="55%">Title</th>
          <th width="15%">Class</th>
          <th width="30%">Action</th>
        </tr>
    </thead>    
    <tbody> 
    	{section start=0 step=1 name=row loop=$papers}	   
            <tr>
              <td><a href="preview_papers.php?file={$papers[row].file_name}&stage=2">{$papers[row].title}</a><br>
              <label>Subject:</label>{$papers[row].subject_name}
              </td>
              <td>{$papers[row].level_name}</td>
              <td>
              	<a href="preview_papers.php?file={$papers[row].file_name}&stage=2" class="btn btn-default btn-xs">Preview</a>
              	<a href="download_papers.php?id={$papers[row].id}" class="btn btn-success btn-xs">Download Pdf</a>
              </td>
            </tr>
        {/section}
    </tbody>
  </table>
  {else}
  	<div class="callout callout-info no-margin">
 		Sorry, Past papers under this subject will be made available soon
    </div>
  {/if}
</div><!-- /.box-body -->