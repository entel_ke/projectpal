<?php
	require "../../include/config.php";
		
	$Obj = new classMain();
	$Obj_papers = new pastPapers();
	
	$stage_id = 2;
	
	//Get subjects/units variables
	$subjects = $Obj_papers->get_subjects($stage_id);
	
	//Get Papers variables
	$papers = $Obj_papers->get_pastpapers('', $stage_id);
	$count_papers = sizeof($papers);
	
	//print_r('<pre>');
	//print_r($projects);
	//exit;
	
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | High schools - PastPapers');
	$smarty->assign('top_logo', 'Projects');
	$smarty->assign('subjects', $subjects);
	$smarty->assign('papers', $papers);
	$smarty->assign('count_papers', $count_papers);
	$content = $smarty->fetch('./templates/home.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main_sec.tpl');

?>