<?php
	require "include/config.php";
	
	if (isset($_GET['spec_id'])) {
		$id = $_GET['spec_id'];
	} else {
		header('location:./index.php');
		exit;
	}
	
	$Obj = new classMain();
	$Obj_projects = new Projects();
	$Obj_categories = new Categories();
	$Obj_courses = new Courses();
	
	$saved_views= $Obj_projects->save_views($id);
	
	
	$downloads = $Obj_projects->get_downloads($id);
	$views = $Obj_projects->get_views($id);
	
	
	//Get Categories variables
	$categories = $Obj_categories->getCategories();
	
	//Get Courses variables
	$courses = $Obj_courses->getCourses('');
	//Get Projects variables
	$projects = $Obj_projects->getProjects('');
	$new_projects = $Obj_projects->latest_projects;
	$count = $Obj_projects->count_projects;
	
	$project = $Obj_projects->getProject($id);
	$project_files = $Obj_projects->getProjectFiles($id);
	$count_files = $Obj_projects->count_files;
	
	//print_r($project) ;
	//exit;
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | Project details');
	$smarty->assign('top_logo', 'ProjectPal');
	$smarty->assign('categories', $categories);
	$smarty->assign('courses', $courses);
	$smarty->assign('new_projects', $new_projects);
	$smarty->assign('count', $count);
	$smarty->assign('projects', $projects);
	$smarty->assign('project', $project);
	$smarty->assign('views', $views);
	$smarty->assign('downloads', $downloads);
	$smarty->assign('project_files', $project_files);
	$smarty->assign('count_files', $count_files);
	$content = $smarty->fetch('./templates/view_project.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main.tpl');

?>