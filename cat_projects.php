<?php
	require "./include/config.php";
	
	if (isset($_GET['category'])) {
		$category = $_GET['category'];
	} else {
		header('location:./');
		exit;
	}
	
	$Obj = new classMain();
	$Obj_projects = new Projects();
	$Obj_categories = new Categories();
	$Obj_courses = new Courses();
	
	//Get Projects variables
	$users = $Obj->getUsers();
	$count_users = $Obj->count_users;
	//Get Categories variables
	$categories = $Obj_categories->getCategories();
	$count_categories =$Obj_categories->count_categories;
	//Get Courses variables
	$courses = $Obj_courses->getCourses('');
	$count_courses =$Obj_courses->count_courses;
	//Get Projects variables
	$projects = $Obj_projects->getProjects('');
	$category_projects = $Obj_projects->getCategoryProjects($category);
	$category_name = $Obj_projects->category_name;
		
	$new_projects = $Obj_projects->latest_projects;
	$count = $Obj_projects->count_projects;
	$count_category_projects = $Obj_projects->count_category_projects;
	$count_pending = $Obj_projects->count_pending;
	$approved_projects = $Obj_projects->approved_projects;
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | Projects');
	$smarty->assign('top_logo', 'ProjectPal');
	$smarty->assign('category', $category_name);
	$smarty->assign('categories', $categories);
	$smarty->assign('courses', $courses);
	$smarty->assign('new_projects', $new_projects);
	$smarty->assign('count_projects', $count);
	$smarty->assign('count_pending',$count_pending);
	$smarty->assign('approved_projects', $approved_projects);
	$smarty->assign('count_downloads', '234');
	$smarty->assign('count_users', $count_users);
	$smarty->assign('count_new_users', '1');
	$smarty->assign('count_visits', '6875');
	$smarty->assign('count_courses', $count_courses);
	$smarty->assign('count_categories', $count_categories);
	$smarty->assign('category_projects', $category_projects);
	$smarty->assign('count_category_projects', $count_category_projects);
	$content = $smarty->fetch('./templates/cat_projects.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main.tpl');

?>