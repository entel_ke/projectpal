<?php
	require "./include/config.php";
		
	$Obj = new classMain();
	$Obj_projects = new Projects();
	$Obj_categories = new Categories();
	$Obj_courses = new Courses();
	
	//Get Categories variables
	$categories = $Obj_categories->getCategories();
	
	//Get Courses variables
	$courses = $Obj_courses->getCourses('');
	//Get Projects variables
	$projects = $Obj_projects->getProjects('');
	$new_projects = $Obj_projects->latest_projects;
	$count = $Obj_projects->count_projects;
	
	//print_r('<pre>');
	//print_r($projects);
	//exit;
	
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | Home');
	$smarty->assign('top_logo', 'Projects');
	$smarty->assign('categories', $categories);
	$smarty->assign('courses', $courses);
	$smarty->assign('new_projects', $new_projects);
	$smarty->assign('count', $count);
	$smarty->assign('projects', $projects);
	$content = $smarty->fetch('./templates/projects.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main.tpl');

?>