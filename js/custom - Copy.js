$(function() {
    
	showProgress ();
	
	function showProgress() {
		$('.progress-bar').animate ({width:'100%'},6000);
		
	}
	
	
	
	$('#load-footer .fa-angle-up').on('click', function(){
	    $('.div-footer').animate({height:'110px'},1000);
		$(this).parent().animate({bottom:'65px'},1000);
		$(this).hide();
		$('#load-footer .fa-angle-down').show();
	});
	$('#load-footer .fa-angle-down').on('click', function(){
	    $('.div-footer').animate({height:'0px'},1000);
		$(this).parent().animate({bottom:'-45px'},1000);
		$(this).hide();
		$('#load-footer .fa-angle-up').show();
		
	});
	
	$('.pop-story .close, .backdrop').on('click', function() {
		$('.pop-story').animate({width:'10%', left:'45%'},1000);
		$('.pop-story').animate({height:'0%',top:'50%'},500);
		$(this).parent().hide(1000);
		$('.backdrop').hide(100);
		
		return false;
	});
	
	$('#adlogin').on('click', function() {
		$('.progress-bar').animate ({width:'0%'},10);
		$('.progress-bar').animate ({width:'90%'},3000);
		var Email = $('#ademail').val().trim();
		var Pass = $('#adpass').val().trim();
		//alert(topic+" "+date+" "+where+" "+desc+" "+story);
		if(Email == "") {
			alert ('Please enter email');
			return false;
		} else if(Pass == ""){
			alert ('Please enter password');
			return false;
		}
		$('.backdrop').show();
		$.ajax({
            type    : "POST",
            cache   : false,
            data : { email: Email, pass:Pass},
            url     : "admin/ajax/adminLogin.php",
            success: function(msg) {
                if(msg==1) {
					$('.progress-bar').animate ({width:'100%'},1000);
					$('.hidden_menu_item').css({"display":"inline-block"})
					$('#ademail').val('');
					$('#adpass').val(''); 
					$('#admin-area-cont').show(); 
					$('#admin-area-cont').animate({height:'1600px'}, 2000);
                    $('#adlogin-div').animate({height:'0px'}, 2000);
					$('.backdrop').hide();
                    //getMessages ();
                } else {
                    alert('Authentification failed');
					$('.backdrop').hide();					
                }                   
                
            }
        });
		
		/*function getMessages () {
			$('#span-progress-area').html('<img src="images/301_24.gif" alt="">');
			$.ajax({
            type    : "POST",
            cache   : false,
            data : { msgs: 'msgs'},
            url     : "admin/ajax/getMessages.php",
            success: function(results) {
                results = JSON.parse(results);
				//alert(results);
					if(results.success) {
						lookup_div = $('#tbody-area');
						if(results.data.length) {
							for(var result in results.data) {
								//$(lookup_div).append('<li data-val="'+results.data[result].unit_id+'" data-kal="'+results.data[result].color_band+'"><span class="dv2">' + results.data[result].unit_names + '</span> - <span class="datav">' + results.data[result].unit_acronyms + ' ('+results.data[result].student_size+')</span> (term : ' + results.data[result].semester +') <span class="'+results.data[result].color_band+' right">&nbsp;</span></li>');
								$(lookup_div).append('<tr class="msg-row">'
														+'<td><input type="checkbox" class="check" value="'+results.data[result].id+'" status="'+results.data[result].status+'"></td>'
														+'<td>'
															+'<div class="first sender">'+results.data[result].name+'</div>'
															+'<div class="second message">'+results.data[result].message+'</div>'
															+'<div class="email hidden">'+results.data[result].email+'</div>'
															+'<div class="subject hidden">'+results.data[result].subject+'</div>'
														+'</td>'
														+'<td class="date-sent">'+results.data[result].date_sent+'</td>'
													+'</tr>');
							}
						} else {
							//$('.icon-show-error').html('No More Posts');
						}
						//$('.show-more').hide();
					}
				$('#span-progress-area').html('');               
                
            }
        });
		}*/
		return false;
	});
	
	$('#unread').on('click', function() {
		//alert();
		var search_list = $('#tbody-area');
		var countList = 0;
		var Total_spans = new Array();
		
		Total_spans = search_list.find('.msg-row');
		//alert(Total_spans.length);
			for (var i = 0; i < Total_spans.length; i++) {
				
					stPos = Total_spans[i];
					//alert(); 				 
					var status = $(stPos).find('.check').attr('status');
					//alert(status);
					
					if( status == 1){
					  	$(stPos).hide();
					  //alert(status);
					} else if( status == 0){
						$(stPos).show();
					}
			}
			
		return false;
	});
	
	$('#read').on('click', function() {
		//alert();
		var search_list = $('#tbody-area');
		var countList = 0;
		var Total_spans = new Array();
		
		Total_spans = search_list.find('.msg-row');
		//alert(Total_spans.length);
			for (var i = 0; i < Total_spans.length; i++) {
				
					stPos = Total_spans[i];
					//alert(); 				 
					var status = $(stPos).find('.check').attr('status');
					//alert(status);
					
					if( status == 0){
					  $(stPos).hide();
					  //alert(status);
					} else if( status == 1){
					  $(stPos).show();
					  //alert(status);
					}
			}
			
		return false;
	});
	
	$('#all').on('click', function() {
		$('.div-msgs').show(200);
		$('.div-msgs').animate({width:'100%'},1000);
		$('.full-msg-div').hide(1000);
		$('.full-msg-div').animate({width:'0%'},1000);
		$('.msg-row').show();
		return false;
	});
	
	$('#tbody-area tr').on('click', function(){
		//alert();
		var sender = $(this).find('.sender').children().html();
		var msgId = $(this).find('.check').val();
		var email = $(this).find('.email').html();
		var subject = $(this).find('.subject').html();
		var date = $(this).find('.date-sent').html();
		var msg = $(this).find('.message').children().html();
	    
		$(this).find('b').addClass('not-bold');
		
	    $('.div-msgs').animate({width:'0%'},1000);
		$('.div-msgs').hide(1000);
		$('.full-msg-div').show();
		$('.full-msg-div').animate({width:'100%'},1000);
		$('.full-msg-div .sender').children().html(sender);
		$('.full-msg-div .email').children().html(email);
		$('.full-msg-div .date-sent').children().html(date);
		$('.full-msg-div .subject').children().html(subject);
		$('.full-msg-div .message').children().html(msg);
		
		$.ajax({
            type    : "POST",
            cache   : false,
            data : { id: msgId},
            url     : "admin/ajax/setRead.php",
            success: function(msg) {
                if(msg==1) {
					$(this).find('.check').val(1
					);
					//alert('message read');
                    //getMessages ();
                } else {
                    //alert('Authentification failed');
					
                }                   
                
            }
        });
	});
	
	$('#view-all-inbox').on('click', function() {
		$('.div-msgs').show(200);
		$('.div-msgs').animate({width:'100%'},1000);
		$('.full-msg-div').hide(1000);
		$('.full-msg-div').animate({width:'0%'},1000);
		return false;
	});
	
	
	$('#submit-msg').on('click', function() {
		
		var nam = $('#name').val().trim();
		var Email = $('#email').val().trim();
		var Subject = $('#subject').val().trim();
		var Comments = $('#comments').val().trim();
		//alert(topic+" "+date+" "+where+" "+desc+" "+story);
		if(nam=="" || Email == "" || Subject == "" || Comments == "") {
			alert ('Please fill in all fields');
			return false;
		}
		$('.backdrop').show();
		$.ajax({
            type    : "POST",
            cache   : false,
            data : { name:nam, email: Email, subject:Subject, comments:Comments},
            url     : "./admin/ajax/sendMessage.php",
            success: function(msg) {
				//alert(msg);	
                 if(msg==1) {
					$('#name').val('');
					$('#email').val('');
					$('#subject').val('');
					$('#comments').val('');
					alert('Message sent Successfully');
                    $('.backdrop').hide();
                    
                } else {
                    alert('Sorry, there was a problem');
					$('.backdrop').hide();					
                }                   
                
            }
        });
		
		return false;
	});
	
	$('#btn-subscribe').on('click', function() {
		
		var Email = $('#sub-email').val().trim();
		
		//alert(topic+" "+date+" "+where+" "+desc+" "+story);
		if(Email == "") {
			alert ('Please enter email');
			return false;
		}
		$('.backdrop').show();
		$.ajax({
            type    : "POST",
            cache   : false,
            data : { email: Email},
            url     : "./admin/ajax/subscribe.php",
            success: function(msg) {
                if(msg==1) {
					$('#sub-email').val('');
					alert('Successful!!');
                    $('.backdrop').hide();
                    
                } else {
                    alert('Sorry, there was a problem');
					$('.backdrop').hide();					
                }                   
                
            }
        });
		
		return false;
	});
	
	$('.show-more').on('click', function() {
		
		$('.icon-show-more').html('<img src="images/301_24.gif" alt="">');
		//alert();	
		$.ajax({
            type    : "POST",
            cache   : false,
            data : { dat:'stories'},
            url     : "admin/ajax/load_more_stories.php",
            success: function(results) {
                results = JSON.parse(results);
				//alert(results);
					if(results.success) {
						lookup_div = $('.load-more-area');
						if(results.data.length) {
							for(var result in results.data) {
								//$(lookup_div).append('<li data-val="'+results.data[result].unit_id+'" data-kal="'+results.data[result].color_band+'"><span class="dv2">' + results.data[result].unit_names + '</span> - <span class="datav">' + results.data[result].unit_acronyms + ' ('+results.data[result].student_size+')</span> (term : ' + results.data[result].semester +') <span class="'+results.data[result].color_band+' right">&nbsp;</span></li>');
								$(lookup_div).append('<div class="col-md-4 col-sm-6 max-h">'
														+ '<div class="product-item">'
															+ '<div class="item-thumb">'
																+ '<div class="overlay">'
																	+ '<div class="overlay-inner">'
																		+ '<a href="details.php?id='+results.data[result].id+'" class="view-detail" story-id="'+results.data[result].id+'">'+results.data[result].topic+'</a>'
																	+ '</div>'
																+ '</div>'
																+ '<img src="images/stories/400/'+results.data[result].thumb+'" class="story-img" alt="">'
															+ '</div>'
															+ '<span>'+results.data[result].place+'</span>'
															+ '<h3>'+results.data[result].subtopic+'</h3>'
														+ '</div>'
														+ '</div>');
							}
						} else {
							$('.icon-show-error').html('No More Posts');
						}
						$('.show-more').hide();
					}
				$('.icon-show-more').html('<img src="images/more.png" alt="">');	
            }
        });
		
		return false;
	});
	
	$('.shuffle').on('click', function(){
		
	    $('.shuffle-img').html('<img src="images/301_24.gif" alt="">');
		var shuffle_value = $('#shuffle-box').val();
		lookup_div = $('.new-stories');
		//alert(shuffle_value);
		
		data = {
			's_value':shuffle_value,
		};
		
		$.post(
            $('.shuffle-val').attr('url'),
            data,
            $.proxy(function(results) {
                    results = JSON.parse(results);
                    //alert(results.data);
                    if(results.success==1) { 
						$(lookup_div).animate({opacity:'0'},500);
						$(lookup_div).html('');
                        if(results.data.length) {
                        	for(var result in results.data) {
                                $(lookup_div).append('<div class="col-md-4 col-sm-6 max-h">'
														+'<div class="product-item">'
															+'<div class="item-thumb">'
																+'<div class="overlay">'
																	+'<div class="overlay-inner">'
																		+'<a href="details.php?id='+results.data[result].id+'" class="view-detail">'+results.data[result].topic+'</a>'
																	+'</div>'
																+'</div>'
																+'<img class="story-img" src="images/stories/400/'+results.data[result].thumb+'" alt="">'
															+'</div>'
															+'<span class="where">'+results.data[result].place+'</span>'
															+'<h3 class="pad-left">'+results.data[result].topic+'</h3>'
															+'<h2 class="pad-left margned-top">'+results.data[result].subtopic+'</h2>'
															+'<div class="col-md-12 col-sm-6 margined">'
															+'<span class="v-aligned pad-top pull-left date">'+results.data[result].date+'</span>'
															+'<span class="v-aligned pull-right lnks">'
																+'<a href="details.php?id='+results.data[result].id+'"><img src="images/note.png" alt="">'
																+'<img src="images/s.png" alt="">'
															+'</span>'
															+'</div>'
														+'</div>'
													+'</div>');
                            }
                        }
						if (shuffle_value > 2) {	
							$('#shuffle-box').val(1);
						} else if(shuffle_value == 2){
							$('#shuffle-box').val(3);
						} else if(shuffle_value == 1){
							$('#shuffle-box').val(2);
						}
                        $(lookup_div).animate({opacity:'1'},500);
                    } else {
                       $('.shuffle-img').html('<img src="images/shuffle.png">');                   	  
                    }
					$('.shuffle-img').html('<img src="images/shuffle.png">');
                    //$('.search-button').html('<img src="images/search.png" class="">');
                }, $(this)
            )
        );
	});
	$('.item-thumb .overlay').on('click', function() {
		$('.titled').html('');
		$('.photod').html('');
		$('.titled').html('');
		$('.where-info').html('');
		$('.desc-info').html('');
		$('.date-info').html('');
		var image = $(this).parent().html();
		var title = $(this).parent().parent().find('h3').html();
		var desc = $(this).parent().parent().find('h2').html();
		var where = $(this).parent().parent().find('.where').html();
		var date = $(this).parent().parent().find('.date').html();
		
		$('.backdrop').show(100);
		$('.pop-story').show(100);
		$('.titled').html(title);
		$('.where-info').html(where);
		$('.desc-info').html(desc);
		$('.date-info').html(date);
		$('.photod').append(image);
		$('.photod').children('.overlay').remove();
		$('.pop-story').animate({height:'70%',top:'12%'},500);
		$('.pop-story').animate({width:'64%', left:'18%'},1000);
		
		$('.story-loader').show();
		
		data = {
				'id':$(this).children('.id-holder').val(),
		};
					
		$.post(
			$(this).children('.id-holder').attr('url'),
			data,
			$.proxy(function(results) {
					results = JSON.parse(results);
					//alert(results.data);
					if(results.success==1) { 
						
						if(results.data.length) {
							for(var result in results.data) {
								$(".story-here").html(results.data[result].details);
							}
						}
						
						$('.story-loader').hide();
					} else {
					   $('.story-loader').hide();                	  
					}
					$('.story-loader').hide();
				}, $(this)
			)
		);
				
		return false;
	});
	$('.search-query').keyup(function() {
	
	var $query = $('.search-query').val().trim();
	lookup_div = $('.lookup-results-query ');
	
	if ($query=="") {
		$(lookup_div).hide().html('');
		$('.search-button').html('<img src="images/search.png" class="">');
		return false;
	}
	if ($query.length > 1) {
		
		var search_list = $('.lookup-results-query ');
		var countList = 0;
		var myVal = $query;
		var myValRe = new RegExp(myVal, 'i');
		var Total_spans = new Array();
		Total_spans = search_list.find('.sj_results');
		//var myValRe = new RegExp(myVal, 'i');
		
		//alert(myVal);
		//alert(Total_spans.length);
			for (var i = 0; i < Total_spans.length; i++) {
				
					stPos = Total_spans[i];
					//alert(); 				 
					var varone = $(stPos).children('.datav').html();
					//alert(one);
					var  vartwo = $(stPos).children('.dv2').html();
					//alert(varone+' '+vartwo);
					
					if(varone.match(myValRe) || vartwo.match(myValRe)) {
						 stPos.style.display="block";
					} else {
					  stPos.style.display="none";
					}
			}
			
		return false;
	}
	//alert($query);
        data = {
            //'source' : $(this).attr('data-source'),
            'query' : $query,
        };
        
		$('.search-button').html('<img src="images/301_24.gif" alt="">');
	    
		$.post(
            $('.search-query').attr('url'),
            data,
            $.proxy(function(results) {
                    results = JSON.parse(results);
                    //alert(results.data);
                    if(results.success==1) { 
						$(lookup_div).children().remove();
						$(lookup_div).show().html('<ul></ul>');
						$(lookup_div).animate({opacity:'0.5'},200);
						$(lookup_div).animate({opacity:'1'},100);
						
                        if(results.data.length) {
                        	//search Results
                            $(lookup_div).children('ul').children('li').remove();
                            for(var result in results.data) {
                                $(lookup_div).children('ul').append('<li class="sj_results" data-val="'+results.data[result].id+'" data-ses="4"><a href="details.php?id='+results.data[result].id+'"><span class="dv2">' + results.data[result].topic + '</span> - <span class="datav">' + ' ('+results.data[result].subtopic+')</span> </a></li>');
                            }
                        }   
                        
                    } else {
                        $(lookup_div).html('<ul><li class="query_results_title">No Results Found</li></ul>');                       	  
                    }
                    $('.search-button').html('<img src="images/search.png" class="">');
                }, $(this)
            )
        );
    
        return false;
    });
	
	
	$(document).on('click', function(){
		$('.lookup-results-query ').hide();
		$('.search-query').val('');
	});
	
	$('#submit-story').on('click', function() {
		
		var topic = $('#stopic').val().trim();
		var date = $('#sdate').val().trim();
		var where = $('#swhere').val().trim();
		var desc = $('#sdesc').val().trim();
		var story = $('#sstory').val().trim();
		var stype = $('#stype').val().trim();
		//alert(topic+" "+date+" "+where+" "+desc+" "+story);
		if(topic=="" || date == "" || where == "" || desc == "" || story == "") {
			alert ('Please fill in all fields');
			return false;
		}
		$('.backdrop').show();
		$.ajax({
            type    : "POST",
            cache   : false,
            data : {h:stype, t:topic, d: date, w:where, ds:desc, s:story},
            url     : "./admin/ajax/saveStory.php",
            success: function(msg) {
                if(msg==1) {
					$('#stopic').val('');
					$('#sdate').val('');
					$('#swhere').val('');
					$('#sdesc').val('');
					$('#sstory').val('');
                    alert('Successfully added story');
					$('.backdrop').hide();
					
					var myDropzone = new Dropzone('.dropzone');
					myDropzone.removeAllFiles();
	
                } else {
                    alert('Sorry, there was a problem');
					$('.backdrop').hide();					
                }                   
                
            }
        });
		
		return false;
	});
		
});