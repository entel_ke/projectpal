$(function() {
    //alert('alert');
	$('#registration-form').on('submit', function() {
		//alert();
		var phone = $(this).find('#phone').val();
		
		if (!$.isNumeric(phone)){
			alert('Enter a valid phone number');
			return false;
		}
		$('#register_btn').html('Wait..');	
		$.ajax({
            type    : "POST",
            cache   : false,
            data : $(this).serialize(),
            url     : "../../admin/ajax/register.php",
            success: function(response) {
			    //alert(response);
				if(response==1) {
					window.location.replace('./activate_account.php');
                } else if (response==77){
                    alert('Sorry, the phone number is already taken!!');
					$('#register_btn').html('Register');
					$('#phone').val('').focus();
					
                } else if (response==78){
                    alert('Sorry, email address already taken!!');
					$('#register_btn').html('Register');
					$('#email').val('').focus();
					
                } else {
                    alert('Authentification failed!!');
					$('#register_btn').html('Register');
                }                   
                
            }
        });
		
		return false;
	});
	
	$('.edit-profile').on('click', function() {
		$('.edits').hide(1000);
		$('.editor').show(1000);	
		return false;
	});
	$('.cancel-edit').on('click', function() {
		$('.edits').show(1000);
		$('.editor').hide(1000);	
		return false;
	});
	
	$('#edit-user-form').on('submit', function() {
		//alert();
		var phone = $(this).find('#phone');
		
		if (!$.isNumeric( "-10" )){
			alert('Enter a valid phone number');
			return false;
		}
		$('#update-btn').html('updating..');	
		$.ajax({
            type    : "POST",
            cache   : false,
            data : $(this).serialize(),
            url     : "../../admin/ajax/update.php",
            success: function(response) {
				if(response==1) {
					window.location.replace('./');	
                } else if (response==77){
                    alert('Sorry, phone number already taken!!');
					$('#update-btn').html('Update');
					$('#email').val('').focus();
					
                } else if (response==78){
                    alert('Sorry, email address already taken!!');
					$('#update-btn').html('Update');
					$('#email').val('').focus();
					
                } else if (response==101) {
                    alert('Authentification failed - check password!!');
					$('#update-btn').html('Update');
                } else{
                    alert('Failed to update!!');
					$('#update-btn').html('Update');
                }  
				
                
            }
        });
		
		return false;
	});
	
	$('#admin-login-form').on('submit', function() {
		//alert();
		var email = $(this).find('#admin_email').val().trim();
		var pass = $(this).find('#admin_password').val().trim();
		
		if(email=="" || pass == "") {
			alert ('Please fill in all fields');
			return false;
		}
		
		$('.admin_login_btn').html('Loading...');	
		$.ajax({
            type    : "POST",
            cache   : false,
            data : { e:email, p:pass, level:2},
            url     : "../admin/ajax/userLogin.php",
            success: function(response) {
				//alert(response);
				if(response==1) {
				    window.location.replace('./');
					return false;
                } else {
					//alert();
                    alert('Authentification failed!!');
					$('.admin_login_btn').html('Login');
                }                   
                
            }
        });
		
		return false;
	});
	$('#userLoginForm').on('submit', function() {
		//alert();
		var email = $(this).find('#email').val().trim();
		var pass = $(this).find('#password').val().trim();
		
		if(email=="" || pass == "") {
			alert ('Please fill in all fields');
			return false;
		}
		
		$('#submit').html('Processing...');	
		$.ajax({
            type    : "POST",
            cache   : false,
            data : { e:email, p:pass, level:1},
            url     : "../../admin/ajax/userLogin.php",
            success: function(response) {
				//alert(response);
				if(response==1) {
				    window.location.replace('./');
					return false;
                } else {
					//alert();
                    alert('Authentification failed!!');
					
                }                   
               $('#submit').html('Login'); 
            }
        });
		
		return false;
	});
	
	$('#reset-password').on('submit', function() {
		//alert();
		var phone = $(this).find('#phone').val().trim();
		
		if(phone=="") {
			alert ('Please fill in all fields');
			return false;
		}
		
		$('#submit').html('Wait...');	
		$.ajax({
            type    : "POST",
            cache   : false,
            data : {phone:phone, level:1},
            url     : "../../admin/ajax/reset_password.php",
            success: function(response) {
				//alert(response);
				if(response==1) {
				    window.location.replace('./');
					return false;
                } else {
					//alert();
                    alert('Reset failed!! Please check phone number');
					
                }                   
               $('#submit').html('Reset'); 
            }
        });
		
		return false;
	});
	
    $('#top_login_form').on('submit', function() {
		//alert();
		var email = $(this).find('#top_login_email').val().trim();
		var pass = $(this).find('#top_login_password').val().trim();
		
		if(email=="" || pass == "") {
			alert ('Please fill in all fields');
			return false;
		}
		
		$('.top_login_btn').html('Loading...');	
		$.ajax({
            type    : "POST",
            cache   : false,
            data : { e:email, p:pass, level:1},
            url     : "admin/ajax/userLogin.php",
            success: function(response) {
				if(response==1) {
					window.location.replace('./user_area');
					return false;
                } else {
                    alert('Authentification failed!!');
					$('.top_login_btn').html('Login');
                }                   
                
            }
        });
		
		return false;
	});
	
	$('.check_all_courses').click(function () {
	    //alert();
		$(this).parent('th').parent('tr').parent('thead').siblings('tbody').children('tr').children('td').children(':checkbox').prop('checked', $(this).prop('checked'));
	});
	$('.check_all_categories').click(function () {
	    //alert();
		$(this).parent('th').parent('tr').parent('thead').siblings('tbody').children('tr').children('td').children(':checkbox').prop('checked', $(this).prop('checked'));
	});
	$('.check_all_projects').click(function () {
	    //alert();
		$(this).parent('th').parent('tr').parent('thead').siblings('tbody').children('tr').children('td').children(':checkbox').prop('checked', $(this).prop('checked'));
	});
	
	$("#comb-course-categories").change(function () {  //categories drop down
        //alert();
		$category = $(this).val();
		
	    data = {
            'category' : $category

		};
		
		$(this).parent().parent().parent().parent().find('.course-li').remove();
		$(this).parent().parent().parent().parent().append('<li class="course-li progress-loader centered white">Loading....</li>');
        $.post(
            $(this).attr('url'),
            data,
            $.proxy(function(results) {
                    results = JSON.parse(results);
                    
					$('.progress-loader').remove();
					
					if(results.success == 1) {               
                        if(results.data.length) {
                            for(var result in results.data) {
                                $(this).parent().parent().parent().parent().append('<li class="course-li"><a href="projects.php?id='+results.data[result].id+'"><i class="fa fa-fw fa-certificate"></i>'+results.data[result].course_name+'</a></li>');
                            }
                            
                        } else {
                            $(this).parent().parent().parent().parent().append('<li class="course-li progress-loader centered warn">No Courses Found</li>');
                        } 
                    } else {
						$(this).parent().parent().parent().parent().append('<li class="course-li progress-loader centered warn">No Courses Found</li>');
					}  
                }, $(this)
            )
        );    
    }); 
	
	
	$('#top_search_box_XXX').keyup(function() {
		//alert();
		var $query = $('#top_search_box').val().trim();
		lookup_div = $('#search-area').children('#search-results');
		//alert($query);
		
	if ($query=="") {
		$('#search-area').hide();
		$(lookup_div).html('');
		$('#view-area').show();
		$(lookup_div).animate({height:'0',opacity:'0.5'},100);
		return false;
	}
	if ($query.length > 1) {
		
		var search_list = $('#search-results').find('tr');
		var countList = 0;
		var myVal = $query;
		var myValRe = new RegExp(myVal, 'i');
		var Total_spans = new Array();
		Total_spans = search_list.find('.sj_results');
		//var myValRe = new RegExp(myVal, 'i');
		
		//alert(myVal);
		//alert(Total_spans.length);
			for (var i = 0; i < Total_spans.length; i++) {
				
					stPos = Total_spans[i];
					//alert(); 				 
					var varone = $(stPos).find('.datav').html();
					//alert(one);
					//var  vartwo = $(stPos).children('.dv2').html();
					//alert(varone+' '+vartwo);
					
					//if(varone.match(myValRe) || vartwo.match(myValRe)) {
					if(varone.match(myValRe)) {	
						//alert();
					  stPos.style.display="block";
						//alert(myUnitCode+"here");
					} else {
					  stPos.style.display="none";
					}
			}
			
		return false;
	}
	//alert($query);
        data = {
            //'source' : $(this).attr('data-source'),
            'query' : $query,
        };
        
        
	    $('#view-area').hide();
        $(lookup_div).parent().show();
		$(lookup_div).children('.panel-body').html('<table class="table table-stripped"></table>');
        $(lookup_div).animate({height:'550px',opacity:'0.5'},1000);
		$(lookup_div).animate({opacity:'1'},100);
        $(lookup_div).children('tr').append('<td class="preloader_ls" style="text-align:center; background:#fff; width:100%;" ><img src="css/301_black_24.GIF" /></td>');
            	
        $.post(
            $('#top_search_box').attr('url'),
            data,
            $.proxy(function(results) {
                    results = JSON.parse(results);
                    //alert(results.data);
                    if(results.success) { 
                    		
                        if(results.data.length) {
                        	//Users Results
                            $(lookup_div).children('.panel-body').children('table').children('tr').remove();
                            for(var result in results.data) {
                                $(lookup_div).children('.panel-body').children('table').append('<tr class="sj_results">'
                                + '<td class="td-lgrey" data-val="'+results.data[result].id+'" data-ses="4">'
									+ '<ul class="media-list">'
										+ '<li class="media ">'
											+ '<a class="media-left project-thumb" href="#"> <img src="./images/doc-img.png" alt="..." class="img-thumbnail tile"> </a>'
											+ '<div class="media-body v-align-m">'
												+ '<h4 class="media-heading h-shadowed"><b><a href="view_project.php?id='+results.data[result].id+' class="datav">' + results.data[result].project_title + '</a></b></h4>'
												+ '<p>'
													+ 'Uploaded On: ' + results.data[result].date_uploaded + '<br>'
												+ '</p>'
											+ '</div>'
										+ '</li>'
									+ '</ul>'
								+ '</td>'
								+ '<td class="td-dblue v-align-m center">'
									+ '<a href="billing.php?d_id=' + results.data[result].id + '" class="btn btn-sm btn-success">Download</a><br><br>'
									+ '<span class=""><a href="view_project.php?id=' + results.data[result].id + '">View Details <i class="fa fa-arrow-circle-right"></i></a></span>'
							    + '</td>'
							    + '<tr>');
								
                            }
                            
                         }   
                         
                                           
                    } else {
                        	
                              $(lookup_div).html('<ul><li class="query_results_title">No Results Found</li></ul>');                       	  
                    }
                    $(lookup_div).children('ul').children('.preloader_ls').remove();
                   
                }, $(this)
            )
        );
		
		return false;
	});
	
	$('#category-form').on('submit', function() {
		var protocol = $(this).find('#protocol').val().trim();
		var cat = $(this).find('#category_name').val().trim();
		var cat_id = $(this).find('#category_id').val().trim();
		
		if(cat == "") {
			alert ('Please fill in all fields');
			return false;
		}
		
		if(protocol == 0) {
			$('#save-category').html('Saving..');	
			
			$.ajax({
				type    : "POST",
				cache   : false,
				data : { cat:cat},
				url     : "../admin/ajax/addCategory.php",
				success: function(response) {
					$('#save-category').html('Save');
					if(response==1) {
						alert('Successfull!!');
						$('#category_name').val('');
						 $('.categories-area').load(location.href + ' #tb-categories').fadeIn(4000);
						 //$('#load_tweets').load('record_count.php?q=<?php echo $search_word; ?>').fadeIn("slow");
					} else {
						alert('Failed!!');
					}                   
						
				}
			});
		} else if(protocol == 1) {
			$('#update-category').html('Updating..');	
			
			$.ajax({
				type    : "POST",
				cache   : false,
				data : { id:cat_id, cat:cat},
				url     : "../admin/ajax/updateCategory.php",
				success: function(response) {
					$('#update-category').html('Save');
					if(response==1) {
						alert('Successfull!!');
						$('#category_name').val('');
						 $('.categories-area').load(location.href + ' #tb-categories').fadeIn(4000);
						 
					} else {
						alert('Failed to update!!');
					}                   
						
				}
			});
		
		}
		
		return false;
	});
	
	$('.edit-cat').on('click', function() {
		var cat_id = $(this).attr('id-holder');
		var cat_name = $(this).find('a').html();
		
		$('#protocol').val(1);
		$('#category_name').val(cat_name);
		$('#category_id').val(cat_id);
		$('.side-tbtitle').html('Edit category');
		$('#save-category').hide();
		$('#update-category').show();
		
		
	});
	$('#add-course-form').on('submit', function() {
		//alert();
		var cat = $(this).find('#course-categories').val().trim();
		var c = $(this).find('#course_name').val().trim();
		
		
		if(cat == 0) {
			alert ('Please select category');
			return false;
		} else if(c == "") {
			alert ('Please fill in all fields');
			return false;
		} 
		
		$('#add-course').html('processing..');	
		$.ajax({
            type    : "POST",
            cache   : false,
            data : { cat:cat, c:c},
            url     : "../admin/ajax/addCourse.php",
            success: function(response) {
				if(response==1) {
					alert('Successfull!!');
					window.location.reload();
					$('#course_name').val('');
                } else {
                    alert('Failed!!');
					
                } 
                
                $('#add-course').html('Save');                  
                
            }
        });
		
		return false;
	});
	
	$('#pay-opt').change(function(){
		if ($(this).val() == 2) {	
			$('#project_price').val('');
			$('.price-box').show(500);
		} else if ($(this).val() == 1) {	
			$('#project_price').val(0);
			$('.price-box').hide(500);
		}	
	});	
	
	
	
	$('.course-selectXXX').on('change', function() {
		
		$id = $('#courses').attr('cat-id');
		alert($id);
		$('.category-id-holder').val();
	});
	
	$('#add-project-form').on('submit', function() {
		
		
		
		var t = $(this).find('#pay-opt').val().trim();
		var c = $(this).find('#courses').val().trim();
		var price = $(this).find('#project_price').val().trim();
		var project = $(this).find('#project_title').val().trim();
		var desc = $(this).find('#description').val().trim();
		var keywords = $(this).find('#project_keywords').val().trim();
		var lang = $(this).find('#lang-holder').val().trim();
		
		
		
		if(t == 0) {
			alert ('Please select project pay option');
			return false;
		} else if (t == 2 && price == "") {
			alert ('Please enter the price');
			return false;
		} else if (t == 2 && !$.isNumeric(price)) {
			alert ('Please enter a valid price');
			return false;
		} else if(c == 0) {
			alert ('Please select course');
			return false;
		} else if(project == "" || desc == "") {
			alert ('Please fill in all fields');
			return false;
		} 
		
		
		$('#add_project').html('processing..');	
		
		
		$.ajax({
            type    : "POST",
            cache   : false,
            data : { t:t, price:price, c:c, p:project, desc:desc, lang:lang, key:keywords},
			url     : "../../admin/ajax/addProject.php",
            success: function(response) {
				if(response==1) {
					window.location.replace('./upload_project_2.php');
				} else if(response==177) {
                    alert('Upload project files first!!');
					$('#add_project').html('Upload');
                } else {
                    alert('Sorry, failed to upload!!');
					$('#add_project').html('Upload');
                }                   
                
            }
        });
		return false;
	});
	
	$('#add-projectfiles-form').on('submit', function() {
		
		var project_files = $('#files').children('p').map(function(){
				return $(this).html().trim();
		})
		.get()
		.join();
		
		if (project_files == "") {
			alert("Please select project files");
			return false;
		}
		
		
		$('#add_project_files').html('Processing..');	
		
		
		$.ajax({
            type    : "POST",
            cache   : false,
            data : { files:project_files},
			url     : "../../admin/ajax/addProjectFiles.php",
            success: function(response) {
				if(response==1) {
					window.location.replace('./');
				} else {
                    alert('Sorry, failed to upload files!!');
					$('#add_project_files').html('Finish');
					$('#files').html('');
                }                   
                
            }
        });
		
		//alert();
		return false;
	});
	
	
	$('.btn-refresh').on('click', function(){
	   
		$('.stats-content').load(location.href + '.stats-holder').fadeIn(4000);
	});	
	
	$('.edit-course').on('click', function() {
		//alert();
		//var course_id = $(this).parent().parent().find('.id_holder').val().trim();
		//window.location.href('../edit_course.php?id='+course_id);
	});
	$('.lang-area').on('click', function(){
	   $('.lang-input').focus();
	});	
	
	$('.close').on('click', function(){
	   $(this).parent().remove();
	});	
	
	$('.approve-project').on('click', function(){
	   var $this = $(this);	
	   var proj_id = $(this).parent().parent().parent().children().find('.check_proj').val();
	   alert(proj_id);
	   
	   $this.html('Wait..');
	   $this.prop("disabled", true);
	   
	   $.ajax({
            type    : "POST",
            cache   : false,
            data : {proj_id:proj_id},
			url     : "../admin/ajax/approve_project.php",
            success: function(response) {
				if(response==1) {
					alert('Project successfully approved');
					$this.html('Active');
					$this.removeClass('btn-primary');
					$this.addClass('btn-default');
					$this.removeClass('approve-project');
				} else {
                    alert('Sorry, failed to approve!!');
					$this.html('Approve');
                } 
                
	   			$this.prop('disabled', false);                  
                
            }
        });
       
       return false;
		
	});	
	
	$('.purge-project').on('click', function(){
	   var $this = $(this);	
	   var proj_id = $(this).parent().parent().parent().children().find('.check_proj').val();
	   //alert(proj_id);
	   
	    
	   if(confirm('Delete this project. Are you sure?')) {
	   	  	   
	   	   $this.prop("disabled", true);
		
		   $.ajax({
	            type    : "POST",
	            cache   : false,
	            data : { proj_id:proj_id},
				url     : "../admin/ajax/delete_project.php",
	            success: function(response) {
					if(response==1) {
						alert('Project deleted');
						$this.parent().parent().parent().remove();
					} else {
	                    alert('Sorry, failed to delete!!');
						
	                } 
	                
		   			$this.prop('disabled', false);                  
	                
	            }
	        });
	     }   
       
       return false;
		
	});
	
	$('#stage-select').on('change', function(){
	   //alert('test');
	   var $this = $(this);
	   var stage_id = $(this).val();
	   alert(stage_id);
	   	
	   	$.ajax({
	            type    : "POST",
	            cache   : false,
	            data : { stage_id:stage_id},
				url     : "../../admin/ajax/getStageCourses.php",
	            success: function(results) {
					results = JSON.parse(results);
                    //alert(results.data);
                    if(results.success) {               
                        $course_sel = $('#course-sel');
                        $course_sel.html('');
                        $course_sel.append('<option value="0">--Select Course --</option>');
                        if(results.data.length) {
                            //$("#spinCrs").html('');
                            for(var result in results.data) {
                                $course_sel.append('<option value="'+results.data[result].id+'">' + results.data[result].course_name + '</option>'); 
                             }                               
                        } else {
                            //$("#spinCrs").html('');
                            $course_sel.append('<option value="0">-- No Courses Found --</option>');                            
                        }  
                    }
                }
		});
	   
	   return false;
	 });
	 
	 $('#course-sel').on('change', function(){
	   //alert('test');
	   //var $this = $(this);
	   var stage_id = $('#stage-select').val();
	   alert(stage_id);
	   	
	   	$.ajax({
	            type    : "POST",
	            cache   : false,
	            data : { stage_id:stage_id},
				url     : "../../admin/ajax/getStageLevels.php",
	            success: function(results) {
					results = JSON.parse(results);
                    //alert(results.data);
                    if(results.success) {               
                        $level = $('#level');
                        $level.html('');
                        $level.append('<option value="0">--Select Class --</option>');
                        if(results.data.length) {
                            //$("#spinCrs").html('');
                            for(var result in results.data) {
                                $level.append('<option value="'+results.data[result].id+'">' + results.data[result].level_name + '</option>'); 
                             }                               
                        } else {
                            //$("#spinCrs").html('');
                            $course_sel.append('<option value="0">-- No Levels Found --</option>');                            
                        }  
                    }
                }
		});
	   
	   return false;
	 });	
	 
	 $('#rate-form').on('submit', function() {
		//alert();
		var rate = $(this).find('#rate_holder').val().trim();
		var new_rate = $(this).find('#rate').val().trim();
		
		if(new_rate == "") {
			alert ('Please enter a new Rate');
			return false;
		} 
		
		$('#change-rate').html('Please Wait...');	
		
		if (confirm('Are you sure you want to change the rate from '+rate+ '% to '+new_rate+'%?')) {
			$.ajax({
	            type    : "POST",
	            cache   : false,
	            data : { rate:new_rate},
	            url     : "../admin/ajax/changeRate.php",
	            success: function(response) {
					if(response==1) {
						alert('Successfull!!');
						window.location.reload();
						
	                } else {
	                    alert('Failed!!');
						
	                } 
	                
	               $('#change-rate').html('Change Rate');	              
	                
	            }
	        });
	    }
	    
	    return false;    	
	});
});