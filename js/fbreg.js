// facebook login
window.fbAsyncInit = function() {
	FB.init({
		appId : '1554450808125102',
		cookie : true, // enable cookies to allow the server to access
		// the session
		xfbml : true, // parse social plugins on this page
		version : 'v2.1' // use version 2.1
	});

	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});

};
// Load the SDK asynchronously
( function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id))
			return;
		js = d.createElement(s);
		js.id = id;
		js.src = "//connect.facebook.net/en_US/sdk.js";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));

function statusChangeCallback(response) {
	console.log(response);
	if (response.status === 'not_authorized') {
		// The person is logged into Facebook, but not your app.
		$("#facebook_reg").html("<button class='btn btn-success btn-block' onclick='login()'>Login with Facebook</button>");
	} else {
		// The person is not logged into Facebook, so we're not sure if
		// they are logged into this app or not.
		$("#facebook_reg").html("<button class='btn btn-success' onclick='login()'>Login with Facebook</button>");
	}
}

//call this function to check login to Facebook
function checkLoginState() {
	FB.getLoginStatus(function(response) {
		statusChangeCallback(response);
	});
}

function login() {
	FB.login(function(response) {
		if (response.authResponse) {
			location.href = "http://www.projectpal.co.ke/user_area/pages/fb_reg.php";
		} else {
			$("#facebook_reg").html("<button class='btn btn-success btn-block' onclick='login()'>Login with Facebook</button>");
		}
	}, {
		scope : 'email,user_profile,user_birthday'
	});
	/*
	 console.log('Welcome!  Fetching your information.... ');
	 FB.api('/me', function(response) {
	 console.log('Successful login for: ' + response.name);
	 document.getElementById('status').innerHTML =
	 'Thanks for logging in, ' + response.name + '!';
	 });*/

}

function logout() {
	FB.logout(function(reponse) {
		location.href = "signup.php";
	});
}
