<?php
	require "../../include/config.php";
	
	$Obj = new classMain();
	
	if (isset($_GET['id'])) {
		$user_id = $_GET['id'];
	} else {
		$user_id = $_SESSION['user_id'];
	}
	
	/*if (isset($_GET['id']) && isset($_GET['code'])) {
		if ($Obj->activateUserAccount($_GET['id'], $_GET['code'])) {
			header('location:./');
			exit;
		}
	}*/
	
	
	$error = $Obj->error_verify;
	
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | Activate Account');
	$smarty->assign('user_id', $user_id);
	$smarty->assign('error', $error);
	$menus = $smarty->fetch('./templates/menu_login.tpl');
	$smarty->assign('menu_login', $menus);
	
	
	$smarty->display('./templates/activate_account.tpl');

?>