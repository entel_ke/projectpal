<div class="row">
	<div class="col-lg-12">
		<h3> Upload Past paper file </h3>
		<hr>
	</div>
	<div class="col-lg-6">
		
		<form role="form" id="add-ppfiles-form">
			<div class="panel panel-default">
				
				<div class="panel-body">
					
					<!-- The fileinput-button span is used to style the file input field as button -->
					<span class="btn btn-success btn-block fileinput-button">
						<i class="glyphicon glyphicon-plus"></i>
						<span><b>Click here</b> to select file a pdf file</span>
						<!-- The file input field used as target for the file upload widget -->
						<input id="fileupload" type="file" name="files[]" multiple>
					</span>
					<br>
					<br>
					<!-- The global progress bar -->
					<div id="progress" class="progress">
						<div class="progress-bar progress-bar-primary"></div>
					</div>
					<!-- The container for the uploaded files -->
					<b></b>
					<div id="files" class="files alert alert-info"></div>
				</div>
			</div>
			
			<div class="clearfix"></div>
			
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-block" id="add_pp_files" name="submit">Finish</button>
			</div>
		</form>
									
	</div>
	<div class="col-lg-6">
		<div class="callout callout-info info-div">
			<b><u>FINAL INSTRUCTIONS ON UPLOADING PROJECTS</u></b>
				
			<ul>
				<li>Upload past paper document. - The file should be in PDF file format.</li>
				<li><b>NOTE :</b>Only select one file. The first file selected during upload is the file that will be attached to the pastpaper
				<li>Click <b>Finish</b> to complete saving the pastpaper.</li>
				
			</ul>
			<br>
			
		</div>
	</div>
</div>	
