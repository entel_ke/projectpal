<div class="row">
	<div class="col-lg-12">
		<h3> Upload Project</h3>
		<hr>
	</div>
	<div class="col-lg-6">
		<form role="form" id="add-project-form">
			<div class="form-group">
					<select class="form-control" name="type" id="pay-opt" required="">
						<option value="0">--Select payment option --</option>
						<option value="1">Free project</option>
						<option value="2">Paid project</option>
					</select>
			</div>
			<div class="clearfix"></div>
			<div class="form-group price-box">
				<input class="form-control" placeholder="Enter project price in KES" name="project_price" id="project_price" value="0" required="">
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
					<select class="form-control course-select" name="courses" id="courses">
						<option value="0">--Select Courses --</option>
					{section start=0 step=1 name=row loop=$courses}
						<option cat-id="{$courses[row].category_id}" value="{$courses[row].id}">{$courses[row].course_name}</option>
					{/section}		
					</select>
					
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<input class="form-control" placeholder="Enter project title" name="project_title" id="project_title" required="">
			</div>
			<div class="form-group">
				<textarea class="form-control" rows="5" placeholder="Enter description here" name="description" id="description"  required=""></textarea>
			</div>
			<div class="form-group">
				<input class="form-control" placeholder="Type keywords here" name="project_keywords" id="project_keywords" required="">
			</div>
			<div class="form-group">
				Programming Language(s) - Optional
				<input id="lang-holder" type="text" id="tags-input" value="" placeholder="Enter programming languages used">
			</div>
			{*<div class="form-group">
				Programming Language(s) - Optional
				<div class="lang-area">
					<ul class="lang-ul">
						<li id="lang-input-holder">
							<input type="text" id="autocomplete-ajax" class="lang-input" placeholder="" name="languages" >
						</li>
						<div id="selction-ajax"></div>
					</ul>
				</div>
				
			</div>*}
			
			<div class="clearfix"></div>
			
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-block" id="add_project" name="submit">Next step</button>
			</div>
		</form>
									
	</div>
	<div class="col-lg-6">
		<div class="callout callout-info info-div">
			<b><u>INSTRUCTIONS ON UPLOADING PROJECTS</u></b>
				
			<p>Uploading your projects to ProjectPal puts your work in front of 
				millions of potential customers and fans. Here's a brief overview 
				of the end-to-end process of getting to Upload your projects to us.
			</p>
			<ol>
				<li>Make sure you fill in all the required fields</li>
				<li>By selecting payment option,  you can monetize your project or offer it for free. 
				</li>
				<li>Select the course under which your project falls (Mandatory)</li>
				<li>Project name/title is a <b>MUST</b>. For legibility and minimization of errors 
					on selecting your project title we advice against the usage of special characters.</li>
				<li><b>NOTE : </b>For Software engineering or Computer Science programming projects you make 
					sure you  fill in the programming languages.</li>
				<li>The project's description should be precise and to the point. The description on its own
					is a key factor in encouraging users to download your project</li>
				<li>Type Keywords (each seperated by a comma) associated with your project searching efficiency.</li>	
				<li>Preview your project details to make sure everything looks OK. </li>
				<li>Select next <b>Next step</b> when you're done.</li>
			</div>
	</div>
</div>	