<div class="row">
	<div class="col-lg-3 bg-lgrey right-align color-blue">
		<h2>{$count_user_projects}</h2>
		<h4>Uploaded</h4>
		<hr>
		<h2>{$count_approved_uprojects}</h2>
		<h4>Approved</h4>
		<hr>
		<h2>{$user_projects_downloads}</h2>
		<h4>Downloads</h4>
		<hr>
		<h2>{$user_projects_purchases}</h2>
		<h4>Purchases</h4>
		<hr>
		<h3>KES {if ($user_projects_purchases < 1)} 0 {else} {$user_estimated_revenue} {/if}</h3>
		<h4>Estimated revenue</h4>
	</div>
	<div class="col-lg-9">
		
		<h4>UPLOADED PROJECTS</h4>
		<hr>
		
	    <div class="dataTable_wrapper">
		{if ($count_user_projects != 0)}
	     <table class="table tb-user-projects">
	            <thead>
	                <tr class="bg-lgrey">
	                    <th width="5%" class="centered"><input type="checkbox" class="check_all_categories"></th>
	                    <th width="35%">Mark all</th>
	                    <th width="15%"></th>
						<th width="15%"></th>
						<th width="15%"></th>
						{*<th width="15%"></th>*}
	                </tr>
	            </thead>
	            <tbody>
				
				{section start=0 step=1 name=row loop=$user_projects}
	                <tr class="bdr-bottom">
	                    <td class="l-grayed centered v-align-t"><input type="checkbox" class="check_course" value="{$user_projects[row].id}"></td>
	                    <td class="l-grayed">
							<ul class="media-list">
								<li class="media v-align-m">
									{*<a class="media-left project-thumb" href="#"> <img src="../img/doc-img.png" alt="..." class="img-thumbnail tile"> </a>*}
									<div class="media-body v-align-m">
										<h4 class="media-heading">{$user_projects[row].project_title}</h4>
										<span>Course : {$user_projects[row].course_name}</span>
										<p>
										{*<button class="btn btn-sm btn-success">
											Download
										</button>*}
										</p>
									</div>
								</li>
							</ul>
						</td>
	                    <td class="v-align-m td-dblue ">
							{if ($user_projects[row].approved == 1)}
								<a href="#" class="btn btn-xs btn-default">Approved</a>
							{else}
								<a href="#" class="btn btn-xs btn-danger">In Review</a>
							{/if}
						</td>
						<td class="v-align-m td-lblue ">{if ($user_projects[row].paid == 2)}{$user_projects[row].fee}<br> KES{else}FREE{/if}</td>
						<td class="v-align-m td-dblue ">{$user_projects[row].downloads}<br> downloads</td>
	                    {*<td class="v-align-m td-lblue ">uploaded on <br>{$user_projects[row].date_uploaded}</td>*}
	                </tr>
	            {/section}
				
	            </tbody>
	        </table>
			{else}
				<div class="alert alert-info">No Projects Uploaded <a href="./upload_project.php" class="btn btn-primary pull-right">Upload Project</a>
				</div>
			{/if}
	    </div>
	</div>
</div>	