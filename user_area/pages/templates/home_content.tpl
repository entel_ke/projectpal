<div class="row">
	
	<div class="col-lg-12">
		<div class="callout btn-default centered alert-rate">
			<span class="label label-success label-rate">
				{$vendor_rate}% rate
			</span>
			 <br><br>
			A Project Owner gets {$vendor_rate}% <br>of the total amount of a project per purchase
			
				
		</div>
		<hr>
		
	    <div class="dataTable_wrapper">
			<div class="callout ">
				<h3>Projects</h3>
			</div>
			<div class="callout">
				<div class="row">
					<div class="col-lg-3">
						<h2>{$count_user_projects}</h2>
						<h4>Projects</h4>
						<hr>
						
					</div>
					<div class="col-lg-3">
						<h2>{$user_projects_downloads}</h2>
						<h4>Downloads</h4>
						<hr>
						
					</div>
					
					<div class="col-lg-3">
						<h2>{$user_projects_purchases}</h2>
						<h4>Purchases</h4>
						<hr>
						
					</div>
					<div class="col-lg-3">
						<h2>KES {if ($user_projects_purchases < 1)} 0 {else} {$user_estimated_revenue} {/if}</h2>
						<h4>Estimated revenue</h4>
						<hr>
						
					</div>
				</div>	
			</div>	
	    </div>
	    <div class="dataTable_wrapper">
			<div class="callout ">
				<h3>Past Papers</h3>
			</div>
			<div class="callout">
				<div class="row">
					<div class="col-lg-3">
						<h2>{$count_user_projects}</h2>
						<h4>Pastpapers</h4>
						<hr>
						
					</div>
					<div class="col-lg-3">
						<h2>{$user_projects_downloads}</h2>
						<h4>Downloads</h4>
						<hr>
						
					</div>
					
					<div class="col-lg-3">
						<h2>{$user_projects_purchases}</h2>
						<h4>Purchases</h4>
						<hr>
						
					</div>
					<div class="col-lg-3">
						<h2>KES {if ($user_projects_purchases < 1)} 0 {else} {$user_estimated_revenue} {/if}</h2>
						<h4>Estimated revenue</h4>
						<hr>
						
					</div>
				</div>	
			</div>	
	    </div>
	</div>
</div>	