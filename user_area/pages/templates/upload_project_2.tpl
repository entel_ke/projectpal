<div class="row">
	<div class="col-lg-12">
		<h3> Upload Project files</h3>
		<hr>
	</div>
	<div class="col-lg-6">
		<form role="form" id="add-projectfiles-form">
			<div class="panel panel-default">
				
				<div class="panel-body">
					{*<form class="dropzone" method="post" action="../../admin/ajax/uploadfiles.php">
					</form>*}
					<!-- The fileinput-button span is used to style the file input field as button -->
					<span class="btn btn-success btn-block fileinput-button">
						<i class="glyphicon glyphicon-plus"></i>
						<span><b>Click here</b> to select files (Documentation, zip/war files...)</span>
						<!-- The file input field used as target for the file upload widget -->
						<input id="fileupload" type="file" name="files[]" multiple>
					</span>
					<br>
					<br>
					<!-- The global progress bar -->
					<div id="progress" class="progress">
						<div class="progress-bar progress-bar-primary"></div>
					</div>
					<!-- The container for the uploaded files -->
					<b></b>
					<div id="files" class="files alert alert-info"></div>
				</div>
			</div>
			
			<div class="clearfix"></div>
			
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-block" id="add_project_files" name="submit">Finish</button>
			</div>
		</form>
									
	</div>
	<div class="col-lg-6">
		<div class="callout callout-info info-div">
			<b><u>FINAL INSTRUCTIONS ON UPLOADING PROJECTS</u></b>
				
			<ul>
				<li>Upload project files. - If need be and where appropriate make sure you zip/compress your files. 
				Especially those projects under programming and other related courses.</li>
				<li><b>NOTE :</b>You can select and upload multiple files at the same time.</li>
				<li>Click <b>Finish</b> to complete the process.</li>
				
			</ul>
			<br>
			<p> After your project is approved (take up to several days) and available 
				for purchase/freein the Store, you can use analytic reports on your control panel to 
				track your project's sales and performance, and make changes to your business model over time.
			</p>
		</div>
	</div>
</div>	

<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="../dist/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="../dist/js/jquery.fileupload.js"></script>
    {literal}
	<script>
    $(document).ready(function() {
        $(function () {
        	//alert();
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = '../server/php/';
		    $('#fileupload').fileupload({
		        url: url,
		        dataType: 'json',
		        done: function (e, data) {
		            $.each(data.result.files, function (index, file) {
		                $('<p/>').text(file.name).appendTo('#files');
		            });
		        },
		        progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
		            $('#progress .progress-bar').css(
		                'width',
		                progress + '%'
		            );
		        }
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
		
    });
    </script>
    {/literal}