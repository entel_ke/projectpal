<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>ProjectPal | User Reg</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- font-awesome --> 
    <link href="../../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="../../dist/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="../../../plugins/iCheck/square/blue.css" rel="stylesheet" type="text/css" />
	<link rel="icon" href="../../dist/img/logo_2.png" type="image/x-icon">
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="register-page">
    <div class="register-box" style="margin-top:10px">
      <div class="register-logo">
        <a href="../../index2.html"><b>Project</b>Pal</a>
      </div>

      <div class="register-box-body">
        <p class="login-box-msg">Register a new account</p>
        <form action="" method="post" id="registration-form">
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="First name" name="fname" id="fname" required=''/>
            <span class="glyphicon glyphicon-user form-control-feedback"> </span>
          </div>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Last name" name="lname" id="lname" required=''/>
            <span class="glyphicon glyphicon-user form-control-feedback"> </span>
          </div>
          <div class="form-group has-feedback">
            <input type="phone" class="form-control" placeholder="Phone" name="phone" id="phone" required=''/>
            <span class="glyphicon glyphicon-phone form-control-feedback"> </span>
          </div>

          <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Birthday" name="birthday" id="birthday" required=''/>
            <span class="glyphicon glyphicon-phone form-control-feedback"> </span>
          </div>


          <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email" name="email" id="email" required=''/>
            <span class="glyphicon glyphicon-envelope form-control-feedback"> </span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Password" name="pword" id="pword" required=''/>
            <span class="glyphicon glyphicon-lock form-control-feedback"> </span>
          </div>
          <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Retype password" name="cpword" id="cpword" required='' />
            <span class="glyphicon glyphicon-log-in form-control-feedback"> </span>
          </div>
          <div class="form-group has-feedback">
			<input class="form-control" placeholder="Address" name="address" id="address" required=''/>
			<span class="glyphicon glyphicon-address form-control-feedback"> </span>
		  </div>
          <div class="row">
            <div class="col-xs-8">    
              <div class="checkbox icheck">
                <label style="margin-left:20px;">
                  <input type="checkbox" required=""> I agree to the <a href="terms.php">terms</a>
                </label>
              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat" id="register_btn" name="submit">Register</button>
            </div><!-- /.col -->
          </div>
        </form>        

        <div class="social-auth-links text-center">
          <p>- OR -</p>
            <div class="fb-login">
                <a href="#" class="btn btn-block btn-social btn-facebook btn-flat" onclick="checkLoginState()"><i class="fa fa-facebook"></i> Sign up using Facebook</a>
            </div>
          
        </div>
        <a href="login.php" class="text-center">I already have a membership</a>
      </div><!-- /.form-box -->
    </div><!-- /.register-box -->

    <!-- jQuery 2.1.3 -->
    <script src="../../plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- iCheck -->
    <script src="../../plugins/iCheck/icheck.min.js" type="text/javascript"></script>
    <script src="../../js/custom.js"></script>
    <script src="../fblogin.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&libraries=places"></script>
    <script type="text/javascript" src="../googleScript.js"></script>
  </body>
</html>