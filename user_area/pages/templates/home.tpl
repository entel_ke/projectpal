<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{$title}</title>
    
	<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
	<link href="../dist/css/timeline.css" rel="stylesheet">
	<link href="../dist/css/custom.css" rel="stylesheet">
	<link href="../bower_components/morrisjs/morris.css" rel="stylesheet">
	<link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="icon" href="../../css/logo_2.png" type="image/x-icon">.
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    
    
    {literal}
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-60273234-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
	{/literal}

</head>

<body>

    <div id="wrapper">

       {$menus}

        <div id="page-wrapper">
            <div class="row">
            	
                    <div class="col-lg-12">
	                    <h3 class="page-header">Key Metrics</h3>
	                </div>
                        <hr/>
                
                <!-- /.col-lg-12 --> <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                    
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        	<div class="col-lg-3 bg-lgrey right-align color-blue">
                        		<h2>{$count_user_projects}</h2>
                        		<h4>Uploaded</h4>
                        		<hr>
                        		<h2>{$count_approved_uprojects}</h2>
                        		<h4>Approved</h4>
                        		<hr>
                        		<h2>{$user_projects_downloads}</h2>
                        		<h4>Downloads</h4>
                        		<hr>
                        		<h2>{$user_projects_purchases}</h2>
                        		<h4>Purchases</h4>
                        		<hr>
                        		<h3>KES {if ($user_projects_purchases < 1)} 0 {else} {$user_estimated_revenue} {/if}</h3>
                        		<h4>Estimated revenue</h4>
                        	</div>
                        	<div class="col-lg-9">
                        		<h4>UPLOADED PROJECTS</h4>
                        		<hr>
                        		
	                            <div class="dataTable_wrapper">
								{if ($count_user_projects != 0)}
	                             <table class="table tb-user-projects">
	                                    <thead>
	                                        <tr class="bg-lgrey">
	                                            <th width="5%" class="centered"><input type="checkbox" class="check_all_categories"></th>
	                                            <th width="35%">Check all</th>
	                                            <th width="15%"></th>
												<th width="15%"></th>
												<th width="15%"></th>
												{*<th width="15%"></th>*}
	                                        </tr>
	                                    </thead>
	                                    <tbody>
										
										{section start=0 step=1 name=row loop=$user_projects}
	                                        <tr class="bdr-bottom">
	                                            <td class="l-grayed centered"><input type="checkbox" class="check_course" value="{$user_projects[row].id}"></td>
	                                            <td class="l-grayed">
													<ul class="media-list">
														<li class="media ">
															<a class="media-left project-thumb" href="#"> <img src="../img/doc-img.png" alt="..." class="img-thumbnail tile"> </a>
															<div class="media-body v-align-m">
																<h4 class="media-heading"><b>{$user_projects[row].project_title}</b></h4>
																<p>
																{*<button class="btn btn-sm btn-success">
																	Download
																</button>*}
															</p>
															</div>
														</li>
													</ul>
												</td>
	                                            <td class="v-align-m td-dblue ">
													{if ($user_projects[row].approved == 1)}
														<a href="#" class="btn btn-xs btn-default">Approved</a>
													{else}
														<a href="#" class="btn btn-xs btn-danger">In Review</a>
													{/if}
												</td>
												<td class="v-align-m td-lblue ">{if ($user_projects[row].paid == 2)}{$user_projects[row].fee}<br> KES{else}FREE{/if}</td>
												<td class="v-align-m td-dblue ">{*$user_projects[row].downloads*}<br> downloads</td>
	                                            {*<td class="v-align-m td-lblue ">uploaded on <br>{$user_projects[row].date_uploaded}</td>*}
	                                        </tr>
	                                    {/section}
										
	                                    </tbody>
	                                </table>
									{else}
										<div class="alert alert-info">No Projects Uploaded <a href="./upload_project.php" class="btn btn-primary pull-right">Upload Project</a>
										</div>
									{/if}
	                            </div>
                            </div>
                            <!-- /.table-responsive -->
                            <!--
							<div class="well">
                                <h4>Assets Information</h4>
                                <p>The table above displays all the Items in the systems database. The rows in green represents assets that have been <a target="_blank" href="found.php">recovered</a> by their respective owners. Rows in pink shows items yet not recovered</p>
                                <a class="btn btn-default btn-lg btn-block" target="_blank" href="pending.php">View Pending Assets</a>
                            </div> -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
	<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
	<script src="../bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
	<script src="../dist/js/sb-admin-2.js"></script>
	{literal}
	<script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
                responsive: true
        });
    });
    </script>
    {/literal}
	<script src="../../js/custom.js"></script>
	

</body>

</html>
