

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{$title}</title>
	<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
	<link href="../dist/css/custom.css" rel="stylesheet">
	<link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../../include/dropzone/css/dropzone.css">
	<link rel="icon" href="../../css/logo_2.png" type="image/x-icon">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	{literal}
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-60273234-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
	{/literal}
</head>

<body class="bgd-theme-eee">
	
	{$menu_login}
	
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <div class="reg-panel panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title">Create an Account</h3>
                    </div>
                    <div class="panel-body">
						<div class="col-md-6">
							{*<a href="fb_reg.php" class="btn btn-block btn-primary btn-reg-fb">
								<ul class="ul-inline">
									<li class="">
										<i class="fa fa-facebook"></i>
									</li>
									<li>
										Register with Facebook
									</li>
								</ul>
							</a>*}
							<form role="form" id="registration-form">
								<div class="form-group">
									<input class="form-control" placeholder="Firstname" name="fname" id="fname" required=''>
								</div>
								<div class="form-group">
									<input class="form-control" placeholder="Lastname" name="lname" id="lname" required=''>
								</div>
								<div class="form-group">
									<input class="form-control" type="phone" placeholder="Enter phone no. format 07XXXXXXXX" name="phone" id="phone" required=''>
								</div>
								<div class="form-group">
									<input class="form-control" type="email" placeholder="Email" name="email" id="email" required=''>
								</div>
								<div class="form-group">
									<input class="form-control" type="password" placeholder="Password" name="pword" id="pword" required=''>
								</div>
								<div class="form-group">
									<input class="form-control" type="password" placeholder="Confirm Password" name="cpword" id="cpword" required='' >
								</div>
								<div class="form-group">
									<textarea class="form-control" rows="3" placeholder="Address" name="address" id="address" required=''></textarea>
								</div>
								{*<div class="form-group">
									<input class="form-control " placeholder="City" name="city" id="city" required=''>
								</div>
								<div class="form-group">
									<input class="form-control " placeholder="City" name="city" id="city" required=''>
								</div>*}
								<div class="form-group">
									<input type="checkbox" name="agree" id="agree" required=''>&nbsp;&nbsp;<i>I agree to <a class="terms" href="#"> terms of service</a> </i>
								</div>
								
								<div class="clearfix"></div>
								<div class="form-group">
									<button type="submit" class="btn btn-sm btn-success" id="register_btn" name="submit">Create an Account</button>
								</div>
								<div class="clearfix"></div>
							</form>
						</div>
						
						<div class="col-md-6">
							<label>Upload Profile Picture(<i>optional</i>)</label>:
							<form class="dropzone" method="post" action="../../admin/ajax/uploadPic.php">
							</form>
						</div>
						
						
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	<script src="../../js/custom.js"></script>
	<script src="../../include/dropzone/dropzone.js"></script>

</body>

</html>
