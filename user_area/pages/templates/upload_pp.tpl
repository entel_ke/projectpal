<div class="row">
	<div class="col-lg-12">
		<h3> Upload Past papers</h3>
		<hr>
	</div>
	<div class="col-lg-6">
		<form role="form" id="add-pp-form">
			<div class="form-group">
				<select class="form-control stage-select" name="stage" id="stage-select">
					<option value="0">--Select Stage --</option>
				{section start=0 step=1 name=row loop=$stages}
					<option cat-id="{$stages[row].id}" value="{$stages[row].id}">{$stages[row].stage_name}</option>
				{/section}		
				</select>
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<select class="form-control" name="course" id="course-sel">
					<option value="0">--Select Course --</option>
				</select>
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<select class="form-control" name="level" id="level">
					<option value="0">--Select Class --</option>
				</select>
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<input class="form-control" placeholder="Subject/Unit" name="pp_unit" id="pp_unit" required="">
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<input class="form-control" placeholder="Enter past paper title" name="pp_title" id="pp_title" required="">
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<input class="form-control" placeholder="Type keywords here" name="pp_keywords" id="pp_keywords" required="">
			</div>
			<div class="clearfix"></div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary btn-block" id="add_ppp" name="submit">Next step</button>
			</div>
		</form>
									
	</div>
	<div class="col-lg-6">
		<div class="callout callout-info info-div">
			<b><u>INSTRUCTIONS ON UPLOADING PAST PAPERS</u></b>
				
			<p>Upload high school and higher education pastpapers.
			</p>
			<ol>
				<li>Make sure you fill in all the required fields</li>
				{*<li>By selecting payment option,  you can monetize your project or offer it for free. 
				</li>*}
				<li>Select the stage (high school or higher education) for pastpapers (Mandatory)</li>
				<li>Past papers name/title is a <b>Mandatory</b>. </li>
				<li><b>NOTE : </b> We are NOT liable to any copyright issues on any past papers uploaded
				ALL LIABILITIES whatsoever are herein laid upon the person who uploads the specific past paper on our platform.</li>
				<li>Type Keywords (each seperated by a comma) to ease the searching process.</li>	
				<li>Review the entered details to make sure everything is entered CORRECTLY. </li>
				<li>Select next <b>Next step</b> when you're done to upload the file.</li>
			</div>
	</div>
</div>	