

<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{$title}</title>
	<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
	<link href="../dist/css/custom.css" rel="stylesheet">
	<link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="icon" href="../../css/logo_2.png" type="image/x-icon">.
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	{literal}
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-60273234-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
	{/literal}
</head>

<body>
	
	{$menu_login}
	
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Enter verification code here</h3>
                    </div>
                    <div class="panel-body">
						{if $error}            
							<div class="alert alert-error">
								<button type="button" class="close" data-dismiss="alert">&times;</button>
								{$error}
							</div>
						{/if} 
                        <form role="form" action="activate.php" id="userLoginForm" method="POST" >
							<i class="btn-block ">We have sent you a confirmation SMS. Check your messages for the verification code </i><br>
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Enter Verification code here" name="verify_box" id="verify_box" type="text" autofocus required=''>
                                </div>
								<input class="form-control" placeholder="User Id" name="verify_id" id="verify_id" type="hidden" value="{$user_id}" >
                                								
								<button type="submit" href="register.php" class="btn btn-sm btn-success btn-block" id="btn-verify" name="submit" >Activate Account</button>
								
								<i class="btn-block centered">Havent recieved the confirmation email yet?<br> <b><a href="resend.php">Resend verification code</a></b></i><br>
								
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	<script src="../../js/custom.js"></script>

</body>

</html>
