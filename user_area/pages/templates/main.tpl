<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>{$title}</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="../../bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />  
    <link href="http://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" rel="stylesheet" type="text/css" />    
     <!-- font-awesome --> 
    <link href="../../font-awesome-4.1.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<!-- Theme styles -->
    <link href="../../dist/css/custom.min.css" rel="stylesheet" type="text/css" />
    <link href="../../dist/css/style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../dist/tag-input/css/typeahead.tagging.css">
    <link href="../dist/css/user_area_style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="../dist/css/jquery.fileupload.css"> 
    <link href="../../dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
    <!-- iCheck -->
    <link href="../../plugins/iCheck/flat/blue.css" rel="stylesheet" type="text/css" />
     <!-- bootstrap wysihtml5 - text editor -->
    <link href="../../plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css" rel="stylesheet" type="text/css" />
	<link rel="icon" href="../../dist/img/logo_2.png" type="image/x-icon">
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    {literal}
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-60273234-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
	{/literal}
	
  </head>
  <body class="skin-blue">
  	
		<div id="fb-root"> </div>
		{literal}
			<script>
				(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=1554450808125102&version=v2.0";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));
			</script>
		{/literal}
    <div class="wrapper">
      
      <header class="main-header">
        <!-- Logo -->
        <a href="./" class="logo">
        	<img src="../../dist/img/logo_2.png" class="img-circle circle-small" alt="User Image"/>
          	<b>Project</b>Pal
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- Messages: style can be found in dropdown.less-->
              <li class="dropdown messages-menu">
                <a href="../../">
                  Main page
                </a>
              </li>
               {if isset($smarty.session.user_id)}
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img src="../img/users/128/{$smarty.session.thumbnail}" class="user-image" alt="User Image"/>
                  <span class="hidden-xs">Uweys Makaweys</span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img src="../img/users/128/{$smarty.session.thumbnail}" class="img-circle" alt="User Image" />
                    <p>
                      {$smarty.session.user_name}
                      <small>{$smarty.session.user_email}</small>
                    </p>
                  </li>
                  <!-- Menu Body -->
                  <!--
                  <li class="user-body">
                    <div class="col-xs-4 text-center">
                      <a href="#">Followers</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Sales</a>
                    </div>
                    <div class="col-xs-4 text-center">
                      <a href="#">Friends</a>
                    </div>
                  </li>
                  -->
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <a href="./upload_project.php" class="btn btn-default btn-flat">Upload Project</a>
                    </div>
                    <div class="pull-right">
                      <a href="../../logout.php" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              {else}
              	<li class="">
	                <a href="./login.php">
	                  Sign In / Register
	                </a>
	              </li>
              {/if}
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
         <!-- search form -->
         {* <form action="#" method="get" class="sidebar-form header">
            <div class="input-group">
              <input type="text" name="q" class="form-control" placeholder="Search..."/>
              <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
          </form>*}
          <!-- Sidebar user panel -->
          {if isset($smarty.session.user_id)}
          <div class="user-panel">
            <div class="image image-area">
              <img src="../img/users/128/{$smarty.session.thumbnail}" class="img-user" alt="User Image" />
              <h3 class="color-fff ">{$smarty.session.user_name}</h3>
              <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
            <div class="info">
              
			  <a href="./edit_profile.php" class="btn btn-primary btn-block">
                <span>Edit Profile</span>
              </a>
              
            </div>
          </div>
          {/if}
          
          
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">Dashboard</li>
            <li>
              <a href="./">
                <i class="fa fa-money"></i> <span>Key Metrics</span>
              </a>
            </li>
             <li>
              <a href="./projects.php">
                <i class="fa fa-file"></i> <span>Projects</span>
              </a>
            </li>
            <li class="active treeview">
              <a href="#">
                <i class="fa fa-upload"></i> <span>Upload</span> <i class="fa fa-angle-left pull-right"></i>
              </a>
              <ul class="treeview-menu">
                <li class="active"><a href="upload_project.php"><i class="fa fa-circle-o"></i> Project</a></li>
                <li><a href="upload_pp.php"><i class="fa fa-circle-o"></i> Past Papers</a></li>
              </ul>
            </li>
            <li class="header"> </li>
            
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Main content -->
        <section class="content">
         <!-- Main row -->
          <div class="row">
            <!-- Left col -->
            
            <section class="col-lg-12 connectedSortable">
              <!-- Main area -->
              
              <div class="box box-primary pad">
                	{$content}
              </div><!-- /.Main Area -->
			</section><!-- /.Left col -->
            <!-- right col (We are only adding the ID to make the widgets sortable)-->
           
          </div><!-- /.row (main row) -->

        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> | Developed By <a href="http://entel.co.ke">Entel</a>.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

    <!-- jQuery 2.1.3 -->
    <script src="../../plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <script src="../../js/custom.js"></script>
    
    <!-- Bootstrap 3.3.2 JS -->
    <script src="../../bootstrap/js/bootstrap.min.js" type="text/javascript"></script>  
    <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="../dist/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="../dist/js/jquery.fileupload.js"></script>
    
    
    <script type="text/javascript" src="../dist/tag-input/js/libs/typeahead.bundle.min.js"></script>
    <script type="text/javascript" src="../dist/tag-input/js/typeahead.tagging.js"></script>
    {literal}
    <script type="text/javascript">
      
    	var tagsource = [
    			"Php","Java ", "Javascript", "HTML", "xHTML", "XML", "Python",
    			"Ruby", "Objective-C", "C#", "C++", "Mysql", "PostgreSql", "Pascal", "Fotran",
    			"Cobol", "A+", "ADMB", "Ana", "Analytica", "Yoric", "X10", "SAC"
    			];
			
		$('#lang-holder').tagging(tagsource);	
		
		
		 $(function () {
        	//alert();
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = '../server/php/';
		    //alert(url);
		    $('#fileupload').fileupload({
		        url: url,
		        dataType: 'json',
		        done: function (e, data) {
		            //alert (data);
		            $.each(data.result.files, function (index, file) {
		                $('<p/>').text(file.name).appendTo('#files');
		            });
		        },
		        progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
		            $('#progress .progress-bar').css(
		                'width',
		                progress + '%'
		            );
		        }
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
    </script>
    {/literal}
    
    
    
    
	
    
    
	
  </body>
</html>