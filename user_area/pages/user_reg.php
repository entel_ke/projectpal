<?php
	require "../../include/config.php";
	
	if (isset($_SESSION['user_id'])) {
		session_destroy();
	}
	
	if (isset($_SESSION['thumb'])) unset($_SESSION['thumb']);
	
	$Obj = new classMain();
		
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | Create Account');
	
	$menus = $smarty->fetch('./templates/menu_login.tpl');
	$smarty->assign('menu_login', $menus);
	
	
	$smarty->display('./templates/user_regst.tpl');

?>