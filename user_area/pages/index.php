<?php
	require "../../include/config.php";
	
	if (!isset($_SESSION['user_id'])) {
		header('location:./login.php');
		exit;
	}
	
			
	$Obj = new classMain();
	
	
	if ($_SESSION['active'] == 0) {
		header('location:./activate_account.php');
		exit;
	}
	
	$Obj_projects = new Projects();
	$Obj_categories = new Categories();
	$Obj_courses = new Courses();
	
	//Get Categories variables
	$categories = $Obj_categories->getCategories();
	
	//Get Courses variables
	$courses = $Obj_courses->getCourses('');
	//Get Projects variables
	$projects = $Obj_projects->getProjects('');
	$new_projects = $Obj_projects->latest_projects;
	$count = $Obj_projects->count_projects;
	
	//$project = $Obj_projects->getProject($id);
	$user_projects = $Obj_projects->get_user_projects($_SESSION['user_id']);
	//echo json_encode($user_projects);
	$count_user_projects = $Obj_projects->count_user_projects;
	//approved projects;
	$approved_user_projects = $Obj_projects->approved_user_projects;
	$count_approved_uprojects = sizeof($approved_user_projects);
	//downloads
	$user_projects_downloads = $Obj_projects->user_projects_downloads;
	//purchases
	$user_projects_purchases = $Obj_projects->user_projects_purchases;
	//estimated revenue
	$user_estimated_revenue = $Obj_projects->user_estimated_revenue;
	
	//$project_files = $Obj_projects->getProjectFiles($id);
	$count_files = $Obj_projects->count_files;
	
	$rates = $Obj_projects->get_vendor_rates();
	$vendor_rate = 100 - $rates;
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | User - dashboard');
	$smarty->assign('top_logo', 'ProjectPal');
	$smarty->assign('categories', $categories);
	$smarty->assign('courses', $courses);
	$smarty->assign('new_projects', $new_projects);
	$smarty->assign('count', $count);
	$smarty->assign('projects', $projects);
	
	//$smarty->assign('project', $project);
	//$smarty->assign('project_files', $project_files);
	$smarty->assign('vendor_rate', $vendor_rate);
	$smarty->assign('count_files', $count_files);
	$smarty->assign('user_projects', $user_projects);
	$smarty->assign('count_user_projects', $count_user_projects);
	$smarty->assign('approved_user_projects', $approved_user_projects);
	$smarty->assign('count_approved_uprojects', $count_approved_uprojects);
	$smarty->assign('user_projects_downloads', $user_projects_downloads);
	$smarty->assign('user_projects_purchases', $user_projects_purchases);
	$smarty->assign('user_estimated_revenue', $user_estimated_revenue);
	$content = $smarty->fetch('./templates/home_content.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main.tpl');

?>