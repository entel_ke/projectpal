<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{$title}</title>
	<link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	<link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">
	<link href="../dist/css/timeline.css" rel="stylesheet">
	<link href="../dist/css/custom.css" rel="stylesheet">
	<link href="../bower_components/morrisjs/morris.css" rel="stylesheet">
	<link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" href="../../include/dropzone/css/dropzone.css">
	<link rel="stylesheet" href="../dist/css/style.css">
	<!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
	<link rel="stylesheet" href="../dist/css/jquery.fileupload.css">
	<link rel="icon" href="../../css/logo_2.png" type="image/x-icon">.

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	{literal}
		<script>
		  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		
		  ga('create', 'UA-60273234-1', 'auto');
		  ga('send', 'pageview');
		
		</script>
	{/literal}
</head>

<body>

    <div id="wrapper">

       {$menus}

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h3 class="page-header">Dashboard | Upload project</h3>
                </div>
                <!-- /.col-lg-12 --> <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Upload Project
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
								<div class="col-lg-6">
									<form role="form" id="add-projectfiles-form">
										
										<div class="panel panel-default">
											<div class="panel-heading">
												<h3 class="panel-title centered">Upload project files here</h3>
											</div>
											<div class="panel-body">
												{*<form class="dropzone" method="post" action="../../admin/ajax/uploadfiles.php">
												</form>*}
												<!-- The fileinput-button span is used to style the file input field as button -->
												<span class="btn btn-success fileinput-button">
													<i class="glyphicon glyphicon-plus"></i>
													<span><b>Click here</b> to select files (Documentation, zip/war files...)</span>
													<!-- The file input field used as target for the file upload widget -->
													<input id="fileupload" type="file" name="files[]" multiple>
												</span>
												<br>
												<br>
												<!-- The global progress bar -->
												<div id="progress" class="progress">
													<div class="progress-bar progress-bar-primary"></div>
												</div>
												<!-- The container for the uploaded files -->
												<b></b>
												<div id="files" class="files alert alert-info"></div>
											</div>
										</div>
										
										<div class="clearfix"></div>
										
										<div class="form-group">
											<button type="submit" class="btn btn-primary btn-block" id="add_project_files" name="submit">Finish</button>
										</div>
									</form>
																
								</div>
								<div class="col-lg-6">
									<div class="alert alert-info info-div">
										<b><u>FINAL INSTRUCTIONS ON UPLOADING PROJECTS</u></b>
											
										<ul>
											<li>Upload project files. - If need be and where appropriate make sure you zip/compress your files. 
											Especially those projects under programming and other related courses.</li>
											<li><b>NOTE :</b>You can select and upload multiple files at the same time.</li>
											<li>Click <b>Finish</b> to complete the process.</li>
											
										</ul>
										<br>
										<p> After your project is approved (take up to several days) and available 
											for purchase/freein the Store, you can use analytic reports on your control panel to 
											track your project's sales and performance, and make changes to your business model over time.
										</p>
									</div>
								</div>
                            </div>
                            <!-- /.table-responsive -->
                            <!--
							<div class="well">
                                <h4>Assets Information</h4>
                                <p>The table above displays all the Items in the systems database. The rows in green represents assets that have been <a target="_blank" href="found.php">recovered</a> by their respective owners. Rows in pink shows items yet not recovered</p>
                                <a class="btn btn-default btn-lg btn-block" target="_blank" href="pending.php">View Pending Assets</a>
                            </div> -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
        </div>
        <!-- /#page-wrapper -->

    </div>
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>
	<script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>
	<script src="../bower_components/DataTables/media/js/jquery.dataTables.min.js"></script>
    <script src="../bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js"></script>
    <script src="../dist/js/sb-admin-2.js"></script>
    <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
	<script src="../dist/js/vendor/jquery.ui.widget.js"></script>
	<!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
	<script src="../dist/js/jquery.iframe-transport.js"></script>
    <!-- The basic File Upload plugin -->
    <script src="../dist/js/jquery.fileupload.js"></script>
    {literal}
	<script>
    $(document).ready(function() {
        $(function () {
        	//alert();
		    'use strict';
		    // Change this to the location of your server-side upload handler:
		    var url = '../server/php/';
		    $('#fileupload').fileupload({
		        url: url,
		        dataType: 'json',
		        done: function (e, data) {
		            $.each(data.result.files, function (index, file) {
		                $('<p/>').text(file.name).appendTo('#files');
		            });
		        },
		        progressall: function (e, data) {
		            var progress = parseInt(data.loaded / data.total * 100, 10);
		            $('#progress .progress-bar').css(
		                'width',
		                progress + '%'
		            );
		        }
		    }).prop('disabled', !$.support.fileInput)
		        .parent().addClass($.support.fileInput ? undefined : 'disabled');
		});
		
    });
    </script>
    {/literal}
	<script src="../../js/custom.js"></script>
	<script src="../../include/dropzone/dropzone.js"></script>
</body>

</html>
