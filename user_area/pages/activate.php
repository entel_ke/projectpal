<?php
	require "../../include/config.php";
	
	$Obj = new classMain ();
	
	if (isset($_POST['verify_box']) && isset($_SESSION['user_id'])) {
		
		$id = isset($_SESSION['user_id']) ? $_SESSION['user_id']:$_GET['user_id'];
		$code = isset($_POST['verify_box']) ? $_POST['verify_box']:$_GET['code'];;
		
		$res = $Obj->activateUserAccount($id, $code);
		
		//print_r('<pre>');
		//print_r('Response'.$res);
		//exit;
		//echo $res. json_encode($_SESSION);
		if ($res == 1) {
			header('location:./index.php');
		} else {
			header('location:./activate_account.php?id='.$id);
		}
		
	} else {
		echo '<div style="background-color:#180000; color:#fff; border-radius:10%;padding:10px;width:100%;text-align:center">Fatal Error</div>';
	}