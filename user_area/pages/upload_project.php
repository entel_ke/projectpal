<?php
	require "../../include/config.php";
	
	//echo json_encode($_SESSION);
	
	if (!isset($_SESSION['user_id'])) {
		header('location:./login.php');
		exit;
	}
	
	if (isset($_SESSION['project_folder'])) {
		unset($_SESSION['project_folder']);
	}
	
	if (isset($_SESSION['project_files'])) {
		unset($_SESSION['project_files']);
	}
	
	if ($_SESSION['active'] == 0) {
		header('location:./activate_account.php');
		exit;
	}
		
	$Obj = new classMain();
	$Obj_projects = new Projects();
	$Obj_categories = new Categories();
	$Obj_courses = new Courses();
	
	//Get Categories variables
	$categories = $Obj_categories->getCategories();
	
	//Get Courses variables
	$courses = $Obj_courses->getCourses('');
	//Get Projects variables
	$projects = $Obj_projects->getProjects('');
	$new_projects = $Obj_projects->latest_projects;
	$count = $Obj_projects->count_projects;
	
	//$project = $Obj_projects->getProject($id);
	$user_projects = $Obj_projects->get_user_projects($_SESSION['user_id']);
	$count_user_projects = $Obj_projects->count_user_projects;
	
	//$project_files = $Obj_projects->getProjectFiles($id);
	$count_files = $Obj_projects->count_files;
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | Upload project');
	$smarty->assign('top_logo', 'ProjectPal');
	$smarty->assign('categories', $categories);
	$smarty->assign('courses', $courses);
	$smarty->assign('new_projects', $new_projects);
	$smarty->assign('count', $count);
	$smarty->assign('projects', $projects);
	
	//$smarty->assign('project', $project);
	//$smarty->assign('project_files', $project_files);
	$smarty->assign('count_files', $count_files);
	$smarty->assign('user_projects', $user_projects);
	$smarty->assign('count_user_projects', $count_user_projects);
	
	$content = $smarty->fetch('./templates/upload_project.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main.tpl');

?>