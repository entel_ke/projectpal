-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 29, 2015 at 02:31 PM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projects`
--

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `category_id` int(12) NOT NULL,
  `course_name` text NOT NULL,
  `date_saved` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `category_id`, `course_name`, `date_saved`) VALUES
(1, 1, 'Computer Science', '06/11/2014 22:35:04'),
(2, 2, 'Economics and Finance', '07/11/2014 13:45:39'),
(3, 1, 'Software Engineering', '10/11/2014 22:57:08');

-- --------------------------------------------------------

--
-- Table structure for table `course_categories`
--

CREATE TABLE IF NOT EXISTS `course_categories` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `category` text NOT NULL,
  `date_saved` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `course_categories`
--

INSERT INTO `course_categories` (`id`, `category`, `date_saved`) VALUES
(1, 'Technology and Engineering', '06/11/2014 22:35:04'),
(2, 'asdjddf', '10/01/2015 08:30:23'),
(3, 'Pure and Applied sciences', '10/11/2014 23:07:03'),
(4, 'Fine Arts', '10/01/2015 07:52:14'),
(5, 'Industrial', '10/01/2015 07:53:17'),
(10, 'Last try', '10/01/2015 08:05:30'),
(11, 'likin', '10/01/2015 08:28:47'),
(12, 'try one', '12/01/2015 18:18:07'),
(13, 'ariel', '12/01/2015 18:18:23'),
(14, 'Medicine', '12/01/2015 18:34:15');

-- --------------------------------------------------------

--
-- Table structure for table `files`
--

CREATE TABLE IF NOT EXISTS `files` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `project_id` int(12) NOT NULL,
  `file_name` text NOT NULL,
  `extension` varchar(15) NOT NULL,
  `date_saved` varchar(25) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `files`
--

INSERT INTO `files` (`id`, `project_id`, `file_name`, `extension`, `date_saved`) VALUES
(1, 1, 'proposal.txt', 'txt', '06/11/2014 22:35:04'),
(2, 1, 'project_name_project.zip', 'zip', '06/11/2014 22:35:04'),
(3, 0, 'assets.php', 'php', '26/01/2015 18:08:11'),
(4, 12, 'assets-3.php', 'php', '26/01/2015 19:08:48'),
(5, 12, 'assets.sql', 'sql', '26/01/2015 19:08:48'),
(6, 13, 'assets-2.sql', 'sql', '27/01/2015 12:38:30'),
(7, 14, 'assets-3.sql', 'sql', '28/01/2015 12:03:59'),
(8, 15, 'index-3.html', 'html', '28/01/2015 12:09:42'),
(9, 16, 'index-4.html', 'html', '28/01/2015 17:11:10');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE IF NOT EXISTS `messages` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `email` varchar(90) NOT NULL,
  `message` text NOT NULL,
  `status` int(2) NOT NULL DEFAULT '0',
  `date_sent` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `name`, `email`, `message`, `status`, `date_sent`) VALUES
(1, 'Uweis Salim', 'makaweys@gmail.com', 'This is the first test message . It should appear last in the admin page message/inbox area', 1, '03/04/2014 13:23:12'),
(2, 'James Kirwa', 'jkirwa@gmail.com', 'This is the last test message . It should appear first in the admin page message/inbox area', 1, '03/04/2014 14:23:12');

-- --------------------------------------------------------

--
-- Table structure for table `projects`
--

CREATE TABLE IF NOT EXISTS `projects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(2) NOT NULL,
  `project_title` text NOT NULL,
  `course_id` int(12) NOT NULL,
  `user_id` int(12) NOT NULL,
  `description` text NOT NULL,
  `screen_shot` text NOT NULL,
  `languages` varchar(100) NOT NULL DEFAULT 'N/A',
  `approved` int(2) NOT NULL DEFAULT '0',
  `paid` int(2) NOT NULL DEFAULT '0',
  `fee` varchar(12) NOT NULL DEFAULT '0',
  `date_uploaded` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `projects`
--

INSERT INTO `projects` (`id`, `type`, `project_title`, `course_id`, `user_id`, `description`, `screen_shot`, `languages`, `approved`, `paid`, `fee`, `date_uploaded`) VALUES
(1, 0, 'Project_name', 1, 1, 'Test project data', '', 'Java, php, Mysql', 1, 0, '', '06/11/2014 22:35:04'),
(2, 0, 'vv', 2, 2, 'v', '', 'v', 0, 0, '', '11/11/2014 00:42:35'),
(3, 0, 'vv', 2, 2, 'v', '', 'v', 0, 0, '', '11/11/2014 00:43:12'),
(4, 2, 'test project', 1, 2, 'this is a test project', '', '', 0, 0, '', '12/01/2015 18:43:55'),
(5, 2, 'test 2jbkc;askjbhksdf', 1, 4, 'ihlbbiul', '', 'kasdbas', 0, 0, '', '26/01/2015 18:29:40'),
(6, 1, 'title 2', 2, 4, 'jkl', '', 'jkbvk;o', 0, 0, '', '26/01/2015 18:34:05'),
(7, 1, 'bn', 1, 4, 'ju;gdfas', '', 'kjbcuasbdas', 0, 0, '', '26/01/2015 18:35:40'),
(8, 1, 'bnvmhj', 2, 4, 'kjdbhasfasf', '', 'ilhgciuasof', 0, 0, '', '26/01/2015 18:36:40'),
(9, 1, 'bnvmhj', 2, 4, 'kjdbhasfasf', '', 'ilhgciuasof', 0, 0, '', '26/01/2015 18:36:57'),
(12, 1, 'there en there', 1, 4, 'jbxcouasc', '', 'kjlbas', 0, 0, '', '26/01/2015 19:08:48'),
(13, 1, 'A new Test project', 1, 2, 'Just a test project', '', 'Test Languauges', 0, 0, '', '27/01/2015 12:38:30'),
(14, 2, 'h', 2, 1, 'ju', '', 'kjbv', 0, 0, '', '28/01/2015 12:03:59'),
(15, 2, 'v', 1, 1, 'b', '', 'b', 0, 0, 'v', '28/01/2015 12:09:42'),
(16, 2, 'FUCK', 1, 1, 'NEW', '', 'jkdsfhjdsfs', 0, 0, 'FUCK', '28/01/2015 17:11:10');

-- --------------------------------------------------------

--
-- Table structure for table `p_downloads`
--

CREATE TABLE IF NOT EXISTS `p_downloads` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `project_id` int(12) NOT NULL,
  `user` varchar(30) NOT NULL,
  `ip` varchar(30) NOT NULL,
  `download_date` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `verify_code` varchar(50) NOT NULL,
  `user_level` int(2) NOT NULL DEFAULT '1',
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `email` varchar(90) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` int(20) NOT NULL,
  `address` text NOT NULL,
  `thumbnail` text NOT NULL,
  `active` int(11) NOT NULL DEFAULT '0',
  `date_saved` varchar(20) NOT NULL,
  `last_update` varchar(30) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `verify_code`, `user_level`, `fname`, `lname`, `email`, `password`, `phone`, `address`, `thumbnail`, `active`, `date_saved`, `last_update`) VALUES
(1, '', 1, 'Uweys', 'Makaweys', 'makaweys@yahoo.com', '$2y$10$mPzX4GaVFguspYXfYb3oa.0XgHfh.dCSN07bLQYdl9hWpk1g/TbIK', 701022477, '477, Vanga', 'user.png', 1, '06/11/2014 22:35:04', ''),
(2, '', 2, 'Uweys', 'Makaweys', 'makaweys@gmail.com', '$2y$10$mPzX4GaVFguspYXfYb3oa.0XgHfh.dCSN07bLQYdl9hWpk1g/TbIK', 701022477, '477, Vanga', 'user.png', 1, '06/11/2014 22:35:04', ''),
(4, '4-D-ET*Y~1422360862', 1, 'Moses', 'Nderi', 'monderie@gmail.com', '$2y$10$SNPdeoieZPdRhCGvBWgvXugT3K6vjtjH4h9Sn1fuR.tmEuGZM1WAm', 2147483647, '1234, Juja', 'user.png', 1, '26/01/2015 16:31:27', '26/01/2015 17:30:01'),
(5, '5-x*BrsfT1422365063', 1, 'c', 'c', 'c@set.com', '$2y$10$1SuIqr6Ht0/q7bBNilR1Ve0GN0SKB4plr2m0Vkuy82ARDEXepoD6u', 45, '12345 fd', 'user.png', 0, '27/01/2015 16:24:23', ''),
(6, '6-4thw~QK1422365143', 1, 'c', 'c', 'tina@gmail.com', '$2y$10$AdmGrd6jgmQL.BonW70yputRwofVQelwrnsYtj.lk7ghsQnniqZJq', 45, '12345 fd', 'user.png', 1, '27/01/2015 16:25:43', ''),
(7, '7--Cb^UuA1422365442', 1, 'x', 'x', 'x@x.com', '$2y$10$0O.sSwp1Y4Qlo82Dn4Riju4fO2mpq.Yv.96n/7qi4fXC5Pggy3FvC', 34, 'ghdcgk', 'user.png', 1, '27/01/2015 16:30:42', ''),
(8, '8-B1AxfmR1422454589', 1, 'Moses', 'Nderi', 'monderiee@gmail.com', '$2y$10$9LCeX9WrQDoYS4lfFPHMMu7UPiZE3hUIAyVREN4leg6I/zWauD2Nu', 722421097, '76 wabguru', 'user.png', 1, '28/01/2015 17:16:28', '28/01/2015 17:18:14');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
