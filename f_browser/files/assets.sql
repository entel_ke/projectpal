-- phpMyAdmin SQL Dump
-- version 3.5.2.2
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jan 16, 2015 at 08:35 AM
-- Server version: 5.5.27
-- PHP Version: 5.4.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `assets`
--

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE IF NOT EXISTS `items` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `item` text NOT NULL,
  `serial` varchar(50) NOT NULL,
  `description` text NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `avatar` text NOT NULL,
  `date_saved` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `item`, `serial`, `description`, `status`, `avatar`, `date_saved`) VALUES
(1, 'Identification Card', '28345687', 'Name: Emmanuel\r\nFound In the street.', 0, '', ''),
(2, 'HP Laptop', '354789-012939813', 'Black Dual core HP laptop found in the library On Monday 12th January 2015', 1, '', ''),
(10, 'bgf', '6753423741324bvdasdf', 'khgasdas', 0, '32wsd1gwoikb_pixrpqspygw3_31_tz.jpg', '16/01/2015 10:05:41'),
(11, 'Samsung Galaxy', '67345234573245834223', 'Contact Emmanuel', 0, '03zmxxgggqi2_2_3vhi_nenpc0_kjeg.jpg', '16/01/2015 10:06:48');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(12) NOT NULL AUTO_INCREMENT,
  `user_level` int(2) NOT NULL DEFAULT '1',
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `email` varchar(90) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` int(20) NOT NULL,
  `address` text NOT NULL,
  `thumbnail` text NOT NULL,
  `date_saved` varchar(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `user_level`, `fname`, `lname`, `email`, `password`, `phone`, `address`, `thumbnail`, `date_saved`) VALUES
(1, 2, 'Emmanuel', 'Student', 'emmanuel@yahoo.com', '$2y$10$X0egTtIOjKX33tl.VSQuv.r7fHsNmhxRr3wH2LpFgy.xm1aWPXjf.', 702997978, '477, Juja', 'user.png', '06/11/2014 22:35:04');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
