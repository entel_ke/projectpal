<?php
	require "./dash_area/include/config.php";
	
	
	$Obj = new classMain();
	
	//Get Projects variables
	$items = $Obj->getAssets();
	$item = json_encode($items);
	$count_assets = $Obj->count_assets;
	$count_pend_assets =$Obj->count_pend_assets;
	$pend_assets =$Obj->pend_assets;
	$count_fond_assets =$Obj->count_fond_assets;
	$fond_assets =$Obj->fond_assets;
?>
<!DOCTYPE html>
<html>
<head>
	<!-- Bootstrap Core CSS -->
	<link href="./dash_area/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="./dash_area/bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

    <!-- Timeline CSS -->
    <link href="./dash_area/dist/css/timeline.css" rel="stylesheet">


<title>Dashboard</title>
</head>
<body>
	 <table class="table">
		<thead>
			<tr>
				<th width="5%"><input type="checkbox" class="check_all_categories"></th>
				<th width="40%">Item</th>
				<th width="30%">Code/Serial</th>
				<th width="25%">Date Saved</th>
			</tr>
		</thead>
		<tbody>
			<?php
				for($i=0; $i<sizeof($items); $i++) { ?>
					<tr>
						<td><input type="checkbox" class="check_course" value="<?php echo $items[$i]['id'];?>"></td>
						<td><?php echo $items[$i]['item'];?></td>
						<td><?php echo $items[$i]['serial'];?></td>
						<td><?php echo $items[$i]['date_saved'];?></td>
					</tr>
			<?php
				}
			?>
		</tbody>
	</table>
</body>
</html>
