<?php
	require "./include/config.php";
	
	$query = isset($_POST['top_search_box']) ? $_POST['top_search_box'] :header('location:./');
	
	$Obj = new classMain();
	$Obj_projects = new Projects();
	$Obj_categories = new Categories();
	$Obj_courses = new Courses();
	
	//Get Categories variables
	$categories = $Obj_categories->getCategories();
	
	//Get Courses variables
	$courses = $Obj_courses->getCourses('');
	//Get Projects variables
	$projects = $Obj_projects->getProjects('');
	$new_projects = $Obj_projects->latest_projects;
	$count = $Obj_projects->count_projects;
	
	$result_projects = $Obj_projects->lookupProjects($query);
	$count_results = sizeof($result_projects);
	
	
	$smarty = new Smarty;
	
	$smarty->assign('title', 'ProjectPal | Search');
	$smarty->assign('top_logo', 'ProjectPal');
	$smarty->assign('categories', $categories);
	$smarty->assign('courses', $courses);
	$smarty->assign('new_projects', $new_projects);
	$smarty->assign('count', $count);
	$smarty->assign('projects', $projects);
	$smarty->assign('result_projects', $result_projects);
	$smarty->assign('count_results', $count_results);
	$smarty->assign('query', $query);
	$content = $smarty->fetch('./templates/search_results.tpl');
	$smarty->assign('content', $content);
	
	$smarty->display('./templates/main.tpl');

?>