<?php
class pastPapers extends classMain {
    
	var $course_name ;
	var $all_subjects;
	var $all_stage_courses;
	var $all_p_courses;
	
	var $all_papers;
	var $subject_title;
	
	function __construct() { 
		parent::__construct();
    }
	
	function save_past_papers($title, $subject_id, $course_id, $level_id, $file_name) {
		$now = $now = date ('d/m/Y H:i:s');			
		$sql = "INSERT INTO past_papers (id, title ,subject_unit_id, course_id ,level_id, date_saved) VALUES (null, '".$title."','".$subject_id."', '".$course_id."', '".$level_id."', '".$now."')";
		$res = mysql_query($sql) or die('savepapers=>'.mysql_error());
		
		if ($res) {
			$_SESSION ['saved_paper_id'] = mysql_insert_id();
			return 1;
			//return($this->saveProjectFiles());
		} else {
			return 0;		
		}
	}
	
	function save_paper_files($files, $path) {
		$paper_id = $_SESSION ['saved_paper_id'];
		$args = $files;
		$args = explode(",", $args);
        
		$now = date ('d/m/Y H:i:s');
			
        if(sizeof($args)) {
            foreach($args as $k=>$file) { 
				$ext = substr(strrchr($file,'.'),1);
				$file = mysql_real_escape_string($file);
                $sql = "UPDATE past_papers 
                			SET 
                				file_name = '".$file."'
                			WHERE 1
                			AND id = '".$paper_id."'";
				$res = mysql_query($sql) or die('savepaperfiles=>'.mysql_error());
			}
		
			if ($res) {
				$this->zip_all_the_files($args,$zip_name,$path);
				unset($_SESSION ['saved_paper_id']);
				return 1;
			} else {
				
				return $this->delete_all_files_in_folder($args, $path);	
				//return $this->create_zip($args, $path,$path.$_SESSION['project_folder'].".zip", true);
			}
		}
	}	
	
	function delete_all_files_in_folder($args, $path) {
		
		foreach(glob("{$path}/*") as $file){
	        if(is_dir($file)) { 
	            $this->delete_all_files_in_folder('', $file);
	        } else {
	            unlink($file);
	        }
	    }	
		
		return 107;
			
	}
	
	function get_stages() {
		
		$arr_all_stages = array ();
		
		$sql ="SELECT * FROM `stages` ORDER BY id ASC";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr_all_stages[] = $row;
            }   
        }
		
		return $arr_all_stages;
    }
	
	
	function get_stage_courses($stage_id) {
		
		$arr_all_courses = array();
		
		$WHERE = isset($stage_id) && $stage_id !=""? " WHERE pc.stage_id='".$stage_id."' " :'WHERE 1'; 
		
		$sql ="SELECT * FROM `past_papers_courses` pc ORDER BY course_name ASC";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr_all_courses[] = $row;
            }   
        }
		$this->all_p_courses = $arr_all_courses;
		
		$sql_stage =" SELECT * FROM `past_papers_courses` pc {$WHERE} ORDER BY course_name ASC";
        $res_stage = mysql_query($sql_stage) or die(mysql_error());
       
	    if($res_stage) {
            while($row_stage = mysql_fetch_assoc($res_stage)) {
                $arr_stage_courses[] = $row_stage;
            }   
        }
		
		return $arr_stage_courses;
    }
	
	function get_stage_levels($stage_id) {
		
		$arr_all_levels = array();
		
		$WHERE = isset($stage_id) && $stage_id !=""? " WHERE pc.stage_id='".$stage_id."' " :'WHERE 1'; 
		
		$sql_stage =" SELECT * FROM `levels` pc {$WHERE} ORDER BY id ASC";
        $res_stage = mysql_query($sql_stage) or die(mysql_error());
       
	    if($res_stage) {
            while($row = mysql_fetch_assoc($res_stage)) {
                $arr_all_levels[] = $row;
            }   
        }
		
		return $arr_all_levels;
    }
	
	function get_course($course_id) {
		
		$arr_course = array();
		
		$WHERE = isset($course_id) && $course_id !=""? " WHERE c.id='".$course_id."' " :'WHERE 1'; 
		
		$sql_stage =" SELECT * FROM `past_papers_courses` c {$WHERE} ORDER BY course_name ASC";
        $res_stage = mysql_query($sql_stage) or die(mysql_error());
       
	    if($res_stage) {
            while($row_stage = mysql_fetch_assoc($res_stage)) {
                $arr_course[] = $row_stage;
				$this->course_name = $row_stage ['course_name'];
            }   
        }
		
		return $arr_course;
    }
	
	function get_units_by_course($course_id, $stage_id) {
		$arr_course_units = array();
		
		$WHERE = isset($stage_id) && $stage_id !=""? " WHERE s.stage_id='".$stage_id."' " :'WHERE 1';
		$AND = isset($stage_id) && $stage_id !=""? " AND s.course_id='".$course_id."' " :'';  
		
		$sql_stage =" SELECT * FROM `past_papers_subjects`s {$WHERE} {$AND} ORDER BY subject_name ASC";
        $res_stage = mysql_query($sql_stage) or die(mysql_error());
       
	    if($res_stage) {
            while($row_stage = mysql_fetch_assoc($res_stage)) {
                $arr_course_units[] = $row_stage;
            }   
        }
		
		return $arr_course_units;
	}
	
	function get_subjects($stage_id) {
		$WHERE = isset($stage_id) && $stage_id !=""? " WHERE s.stage_id='".$stage_id."' " :'WHERE 1'; 
		
		$sql ="SELECT * FROM `past_papers_subjects` s ORDER BY id DESC";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr_all_subjects[] = $row;
            }   
        }
		$this->all_subjects = $arr_all_subjects;
		
		$sql_stage =" SELECT * FROM `past_papers_subjects`s {$WHERE} ORDER BY subject_name ASC";
        $res_stage = mysql_query($sql_stage) or die(mysql_error());
       
	    if($res_stage) {
            while($row_stage = mysql_fetch_assoc($res_stage)) {
                $arr_stage_subjects[] = $row_stage;
            }   
        }
		
		return $arr_stage_subjects;
    }
	
	function get_pastpapers($subject_id,$stage_id) {
		
		$arr_all_papers = array();
		$arr_subject_papers = array();
		
		$WHERE = isset($subject_id) && $subject_id !=""? " AND past_papers.subject_unit_id='".$subject_id."' " :''; 
		
		$sql_stage ="SELECT past_papers.title, past_papers.subject_unit_id, past_papers.stage_id, past_papers.level_id, 
								past_papers.course_id, past_papers.file_name, past_papers.date_saved, past_papers_subjects.subject_name, 
								stages.stage_name, past_papers_courses.course_name, levels.level_name 
						FROM past_papers,  past_papers_subjects, past_papers_courses, stages, levels 
						WHERE 1
							AND past_papers.subject_unit_id = past_papers_subjects.id 
							AND past_papers.stage_id = stages.id
							AND past_papers.course_id = past_papers_courses.id
							AND past_papers.level_id = levels.id
							AND past_papers.stage_id = {$stage_id}
							{$WHERE} 
					 	ORDER BY past_papers.title ASC";
        $res_stage = mysql_query($sql_stage) or die(mysql_error());
       
	    if($res_stage) {
            while($row_stage = mysql_fetch_assoc($res_stage)) {
                $arr_subject_papers[] = $row_stage;
            }   
        }
		$this->subject_papers = $arr_subject_papers;
		
		$sql ="SELECT past_papers.title, past_papers.subject_unit_id, past_papers.stage_id, past_papers.level_id, 
								past_papers.course_id, past_papers.file_name, past_papers.date_saved, past_papers_subjects.subject_name, 
								stages.stage_name, past_papers_courses.course_name, levels.level_name 
						FROM past_papers,  past_papers_subjects, past_papers_courses, stages, levels 
						WHERE 1
							AND past_papers.subject_unit_id = past_papers_subjects.id 
							AND past_papers.stage_id = stages.id
							AND past_papers.course_id = past_papers_courses.id
							AND past_papers.level_id = levels.id
							AND past_papers.stage_id = {$stage_id}
						ORDER BY past_papers.id DESC";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr_all_papers[] = $row;
            }   
        }
		
		return $arr_all_papers;
    }
	
	
	function get_unit_subject_by_id($id) {
		$arr_subject = array();
		
		$WHERE = isset($id) && $id !=""? " WHERE s.id='".$id."' " :'WHERE 1'; 
		
		$sql_stage =" SELECT * FROM `past_papers_subjects` s {$WHERE} ORDER BY subject_name ASC";
        $res_stage = mysql_query($sql_stage) or die(mysql_error());
       
	    if($res_stage) {
            while($row_stage = mysql_fetch_assoc($res_stage)) {
                $arr_subject[] = $row_stage;
				$this->subject_title = $row_stage['subject_name'];
            }   
        }
		return $arr_subject;
    }
	
	function delete_subject($id) {
		$sql="DELETE FROM `subjects` 
				WHERE
					id='".$id."'";
		$res = mysql_query($sql) OR DIE(mysql_error());
        
		if($res) {
            return true;
        } else {
        	return false;
        }
		
    }  

}