<?php

/** 
 * @author Ben Cooling <office@bcooling.com.au> 
 * @package cl
 * @subpackage cl_FileUpload
 * @license http://www.gnu.org/copyleft/gpl.html Freely available under GPL
 */

/** 
 * <b>File Upload</b>
 * 
 * Upload files easily. Allow or deny specific files by their extention. Force
 * filesnames to be unique or allow copies or produce random unique names.
 *
 * @package cl
 * @subpackage cl_FileUpload
 * @author Ben Cooling <office@bcooling.com.au> 
 * @version 0.1
 *
 */

class FileUpload
{

    private $file;
    private $file_ext;
    private $target_path = '../../uploads/files/';
    private $naming = 'unique';
    private $max_filesize = false;
    private $allow = true;
    private $deny = false;
    
    public function __set($file, $value){
        if ($file=='file_ext') return false;
        if ($file=='naming' && $this->check_naming($value)==false) return false;
        if ($file=='file'){
            $file_arr = explode('.', $value['file']);
            $this->file_ext = end($file_arr);
        }
        $this->$file = $value;
    }
    
    public function __get($file){
        return $this->$file;
    }
    
    public function __construct($config=null){
        if (is_array($config)) foreach($config as $k => $v){
            if(isset($this->$k)) $this->$k = $v;
        }
    }

    public function upload_file(){
        switch($this->naming) {
            case 'copies':
                if (file_exists($this->target_path.$this->file['file'])){
                    $file_arr = explode('.', $this->file['file']);
                    $this->file['file'] = $file_arr[0].' copy.'.$this->file_ext;
                    if (file_exists($this->target_path.$this->file['file'])) $this->file['file'] = $file_arr[0].' copy 2.'.$this->file_ext;
                    if (file_exists($this->target_path.$this->file['file'])) $this->increment_copy();
                } 
            break;
            case 'random':
                $this->generate_random();
            break;
            default: // unique
                if (file_exists($this->target_path.$this->file['file'])){
                    $this->file['error'] = 'The filename already exists';
                    return false;
                }
        }
        if (!$this->check_ext()) $this->file['error'] = 'The filename is not allowed';
        if ($this->file['error'] !== 0) return false;
        move_uploaded_file($this->file['tmp_name'], $this->target_path.$this->file['file']);
        return true;
        
    }

    private function generate_random($len=32){
        $str = md5(uniqid(rand(), true));
        $this->file['file'] = substr($str, 0, $len).'.'.$this->file_ext;
        if (file_exists($this->target_path.$this->file['file'])) $this->generate_random();
        else return true;
    }

    private function increment_copy(){
        $file_arr = explode('.', $this->file['file']);
        $cur_number = substr($file_arr[0], -1);
        $cur_name = substr($file_arr[0], 0, -1);
        $cur_number = $cur_number +1;
        $this->file['file'] = $cur_name.$cur_number.'.'.$this->file_ext;
        if (file_exists($this->target_path.$this->file['file'])) $this->increment_copy();
    }

    private function check_naming($value){
        $naming_arr = array('copies','random','unique');
        if (in_array($value, $naming_arr)) return true;
        else return false;
    }

    private function check_ext(){
        if (is_array($this->deny)){
                if (in_array($this->file_ext, $this->deny)) return false;
                else return true;
        }
        if (is_array($this->allow)){
                if (in_array($this->file_ext, $this->allow)) return true;
                else return false;
        } 
        return true;           
    }

}