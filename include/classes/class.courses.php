<?php
class Courses extends Categories {
    
	var $courses;
	var $count_courses;    
	
    function __construct() { 
		parent::__construct();
    }
	function saveCourse($category, $course) {
		$now = date ('d/m/Y H:i:s');
		
		$sql = "INSERT INTO courses (id, category_id, course_name, date_saved) VALUES (null, '".$category."', '".$course."', '".$now."')";
		$res = mysql_query($sql) or die('savecorse=>'.mysql_error());
		
		if ($res) {
			return 1;
		} else {
			return 0;		
		}
	}
	function getCourses($category_id) {
		$arr_l =array ();
		$arr_all = array();
		$WHERE = isset($category_id) && $category_id !="" && $category_id !=0 ? " WHERE category_id='".$category_id."'" :""; 
		
		$sql_latest =" SELECT * FROM `courses`";
        $res_latest = mysql_query($sql_latest) or die(mysql_error());
       
	    if($res_latest) {
            while($row = mysql_fetch_assoc($res_latest)) {
                $arr_l[] = $row;
            }   
        }
		$this->count_courses = sizeof($arr_l);
		
		
		$sql_all =" SELECT ct.id AS category_id, ct.category, ct.date_saved AS cat_date_saved, c.id, c.course_name,c.date_saved FROM `courses` c 
						INNER JOIN `course_categories` ct ON(c.category_id = ct.id) 
						{$WHERE} 
						ORDER BY course_name ASC";
        $res_all = mysql_query($sql_all) or die(mysql_error());
       
	    if($res_all) {
            while($row = mysql_fetch_assoc($res_all)) {
                $arr_all[] = $row;
            }   
        }
        
		return $arr_all;
    }
	
	
	function get_course_category($course_id) {
			
		$sql =" SELECT ct.id AS category_id, ct.category, ct.date_saved AS cat_date_saved, c.id, c.course_name,c.date_saved FROM `courses` c 
						INNER JOIN `course_categories` ct ON(c.category_id = ct.id) 
						WHERE c.id='".$course_id."'";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $cat_id = $row ['category_id'];
            }   
        }
        
		return $cat_id;
    }
	
    function getCourse($course_id) {
		$arr_all =array ();
		$arr = array();
		
		$sql =" SELECT ct.id AS category_id, ct.category, ct.date_saved AS cat_date_saved, c.id, c.course_name,c.date_saved FROM `courses` c
					INNER JOIN `course_categories` ct ON (c.category_id = ct.id)
					WHERE c.id='".$course_id."'";
        $res = mysql_query($sql) or die(mysql_error());
       
	   if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr[] = $row;
            }   
       }
       
	   return $arr;
    }
	
	function drop($args) {
		$args = explode(",", $args);
		$args = array_unique($args);
		     
        if(sizeof($args)){
            foreach($args as $k=>$id) { 
                $this->deleteCourse($id);
			}
        } else {
			return 0;		
		}
		
    }
	function deleteCourse($id) {
		$sql ="DELETE FROM `courses` WHERE id='".$id."'";
        $res = mysql_query($sql) or die(mysql_error());
		
		if ($res) {
			return 1;
		} else {
			return 0;		
		}
		
    }
	function updateCourse($id, $category, $course) {
		
		$now = date ('d/m/Y H:i:s');
		
		$sql ="UPDATE `courses` SET course_name='".$course."', category_id='".$category."', date_saved='".$now."' WHERE id='".$id."'";
        $res = mysql_query($sql) or die(mysql_error());
		
		if ($res) {
			return 1;
		} else {
			return 0;		
		}
		
    }
	
   }