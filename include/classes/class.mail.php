<?php
    //set constants - load define class
    class SimpleMail {
        var $user_email;
        function __construct() {
            
        }
        
        // new in version 1.99 support for phpmailer as alternative mail program    
        function send_mail($mail_address, $msg = 29, $subj = 28, $send_admin = false) {
            $this->user_email = $mail_address;
            $subject = is_numeric($subj) ? $this->messages($subj) : $subj;
            $body = is_numeric($msg) ? $this->messages($msg) : $msg;
            if (USE_PHP_MAILER) {
                $mail = new PHPMailer();
                $mail->isHTML(true);
                if (PHP_MAILER_SMTP) {
                    $mail->IsSMTP();
                    $mail->Host = SMTP_SERVER;
                    $mail->SMTPAuth = true;  
                    $mail->Username = SMTP_LOGIN;
                    $mail->Password = SMTP_PASSWD; 
                } else {
                    $mail->IsSendmail(); 
                }
                $mail->From = WEBMASTER_MAIL;
                $mail->FromName = WEBMASTER_NAME;
                $mail->AddAddress($mail_address);
                if ($send_admin) $mail->AddBCC(ADMIN_MAIL);
                $mail->Subject = $subject;
                $mail->Body = $body;
                if($mail->Send()) {
                    return true;
                } else {
                    return false;
                }
            } else {
                $header = "From: \"".WEBMASTER_NAME."\" <".WEBMASTER_MAIL.">\n";
                if ($send_admin) $header .= "Bcc: ".ADMIN_MAIL."\n";
                $header .= "MIME-Version: 1.0\n";
                $header .= "Content-Type: text/html; charset=\"iso-8859-1\"\n";
                $header .= "Content-Transfer-Encoding: 7bit\n";
                
                if (mail($mail_address, $subject, $body, $header)) {
                    return true;
                } else {
                    return false;
                } 
            }             
        }
        
        function send_fb_email() {
            
        }
        
        // message no. 35 is changed because the verification string based in the user name now
        function messages($num) {
            $host = "http://".$_SERVER['HTTP_HOST'];
    
            $msg[10] = "Please enter the right email address or password.";
            $msg[11] = "Login and/or password field is empty!";
            $msg[12] = "Sorry, a user with this E-mail address already exists, choose a new one.";
            $msg[13] = "Please check your e-mail and follow the instructions.";
            $msg[14] = "Sorry, an error occurred please try it again.";
            $msg[15] = "Sorry, an error occurred please try it again later.";
            $msg[16] = "The e-mail address is not valid.";
            $msg[17] = "The field login (min. ".LOGIN_LENGTH." char.) is required.";
            $msg[18] = "Your request is processed. Login to continue.";
            $msg[19] = "Sorry, cannot activate your account.";
            $msg[20] = "There is no account to activate.";
            $msg[21] = "Sorry, this activation key is not valid!";
            $msg[22] = "Sorry, there is no active account which matches this e-mail address.";
            $msg[23] = "Please check your e-mail to get your new password.";
            $msg[24] = "Your user account is activated... ";
            $msg[25] = "Sorry, cannot activate your password.";
            $msg[26] = "Your forgotten password..."; 
            $msg[27] = "Please check your e-mail and activate your modification(s).";
            $msg[28] = "Your request must be processed...";
            $msg[29] = "Hello,\r\n\r\nto activate your request click the following link:\r\nident=".md5($this->user_email)."\r\n\r\nkind regards\r\n";
            $msg[30] = "Your account is modified.";
            $msg[31] = "This e-mail address already exist, please use another one.";
            $msg[32] = "The field password (min. ".LOGIN_LENGTH." char) is required.";
            $msg[33] = "Hello,\r\n\r\nthe new e-mail address must be validated, click the following link:\r\n".$host.LOGIN_PAGE."?id=".md5($this->user_email)."\r\n\r\nKind regards\r\n Onion Team \r\n".WEBMASTER_NAME;
            $msg[34] = "There is no e-mail address for validation.";
            $msg[35] = "Hello,<br/> To set your new password next, please - <a href='".ACTIVE_PASS_PAGE."?activate=".md5($this->user_email)."'> click here </a> - <br/>or copy this link and paste on your browser's address bar: <br/>".ACTIVE_PASS_PAGE."?activate=".md5($this->user_email)."<br/>kind regards<br/>".WEBMASTER_NAME;
            $msg[36] = "Your request is processed and is pending for validation by the admin. \r\nYou will get an e-mail if it's done.";
            $msg[37] = "Hello,\r\n\r\nThe account is active and it's possible to login now.\r\n\r\nClick on this link to access the login page:\r\n".$host.LOGIN_PAGE."\r\n\r\nkind regards\r\n".WEBMASTER_NAME;
            $msg[38] = "The confirmation password does not match the password. Please try again.";
            $msg[39] = "A new user...";
            $msg[40] = "There was a new user registration on ".date("Y-m-d").":\r\n\r\nClick here to enter the admin page:\r\n\r\n".$host."?login_id=";
            $msg[41] = "Validate your e-mail address..."; // subject in e-mail
            $msg[42] = "Your e-mail address is modified.";
            $msg[43] = "Sorry this activation key is expired";          
            $msg[44] = "Mobile phone number should be in the format **** ******";
            $msg[45] = "Order Confirmation";
            $msg[46] = "Your order was successfully received";  //to the students
            $msg[47] = "A student has sign-up for your lesson";  //to the teacher
            $msg[48] = "You have successfully created a lesson";  //to the teacher
            $msg[49] = "Lesson date is approaching";  //to the all
            $msg[50] = "Hi teacher you have successfully created a lesson. Please follow these tips to help you gear up for teaching your students";  //to the all
            $msg[51] = "Hi teacher a student has signed up for this lesson. Please follow these tips to help you gear up for teaching your students";  //to the all
            $msg[52] = "Request from a student.";  //to the all
            $msg[53] = "I think you will like this wonderful course.\r\n\r\n";  //to the all
            $msg[54] = "Welcome to Briteskills";  //to the all
        
            return $msg[$num];
        }    
    }
?>