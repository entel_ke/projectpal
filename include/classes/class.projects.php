<?php
class Projects extends classMain {
    
	var $projects;
	var $count_projects;
	var $count_pending;
	var $pending_projects;
	var $latest_projects;
	var $approved_projects;
	var $all_projects;
	
	var $category_name;
	var $count_category_projects;
	
	var $count_user_projects;
	
	//count  project files
	var $count_files;
	
	var $approved_user_projects;
	var $user_projects_downloads;
	var $user_projects_purchases;
	var $user_estimated_revenue;
	
	
	//Project Info
	var $project_titleInfo;
	var $project_payInfo;
	var $project_feeInfo;
	var $project_descInfo;
	var $project_approveInfo;
	var $project_folderInfo;
	var $project_fileInfo;
	var $project_viewsInfo;
	var $project_userIdInfo;
	var $project_userfolderInfo;
	var $project_projectfolderInfo;


	function __construct() { 
		parent::__construct();
    }
	
	function saveProject($t, $price, $course_id, $project, $description, $lang, $keywords) {
		$user_folder = $_SESSION['user_folder_name'];
		$user = $_SESSION ['user_id'];
		$Ojcc = new Courses();
		$cat_id = $Ojcc->get_course_category($course_id);
		$keywords = mysql_real_escape_string($keywords);
		$description = mysql_real_escape_string($description);
		$lang = mysql_real_escape_string($lang);
		$now = date ('d/m/Y H:i:s');
		$spec_id = "Ppal_Project_".uniqid()."_".time();
		$project_zip = $spec_id.".zip";
		$ip = $_SERVER['REMOTE_ADDR'];
		
		$folder_name = $spec_id;
					
		if($make_dir = mkdir('../../user_area/server/php/f_browser/files/'.$user_folder.'/'.$folder_name)) {
			$_SESSION['project_folder']= $spec_id; 
		}
			
		$sql = "INSERT INTO projects (id, ip,spec_id ,user_id, category_id ,course_id, project_title, zipped_file_name,description, languages, type, paid,fee, keywords,date_uploaded) VALUES (null,'".$ip."', '".$spec_id."','".$user."', '".$cat_id."', '".$course_id."', '".$project."', '".$project_zip."','".$description."', '".$lang."', '".$t."', '".$t."', '".$price."', '".$keywords."', '".$now."')";
		$res = mysql_query($sql) or die('saveproject=>'.mysql_error());
		
		if ($res) {
			$_SESSION ['saved_project_id'] = $spec_id;
			return 1;
			//return($this->saveProjectFiles());
		} else {
			return 0;		
		}
	}
	
	function saveProjectFiles($files) {
		$project_id = $_SESSION ['saved_project_id'];
		$path = '../../user_area/server/php/f_browser/files/'.$_SESSION['user_folder_name'].'/'.$_SESSION['project_folder']."/";
		$zip_name = $_SESSION['project_folder'].'.zip';
		$args = $files;
		$args = explode(",", $args);
        
		$now = date ('d/m/Y H:i:s');
			
        if(sizeof($args)) {
            foreach($args as $k=>$file) { 
				$ext = substr(strrchr($file,'.'),1);
				$file = mysql_real_escape_string($file);
                $sql = "INSERT INTO files (id, project_id, file_name, extension, date_saved) VALUES (null, '".$project_id."', '".$file."', '".$ext."', '".$now."')";
				$res = mysql_query($sql) or die('saveprojectfiles=>'.mysql_error());
			}
		
			if ($res) {
				$this->zip_all_the_files($args,$zip_name,$path);
				unset($_SESSION ['saved_project_id']);
				unset($_SESSION['project_folder']);
				
				return 1;
			} else {
				
				return $this->delete_all_files_in_folder($args,$path);	
				//return $this->create_zip($args, $path,$path.$_SESSION['project_folder'].".zip", true);
			}
		}
	}	
	
	function delete_all_files_in_folder($args, $path) {
		
		foreach(glob("{$path}/*") as $file){
	        if(is_dir($file)) { 
	            $this->delete_all_files_in_folder('', $file);
	        } else {
	            unlink($file);
	        }
	    }	
		
		return 107;
			
	}
	
	
	/* creates a compressed zip file */
	function zip_all_the_files($args,$zip_name,$path) {
		$zip = new ZipArchive();
		$zip->open($path.$zip_name, ZipArchive::CREATE);
		
		foreach($args as $file){
		  $zip->addFile($path.$file, $file);
		  //echo $files['file_name'];
		}
		
		$zip->close();
	}
	function getProjects($course_id) {
		$arr_l =array ();
		$arr_all = array();
		$arr_approved = array();
		$arr_pending = array();
		$arr_comp = array();
		
		$WHERE = isset($course_id) && $course_id !=""? " WHERE p.course_id='".$course_id."' " :'WHERE 1'; 
		
		$sql ="SELECT * FROM `projects` ORDER BY id DESC";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr_comp[] = $row;
            }   
        }
		$this->all_projects = $arr_comp;
		
		$sql_latest =" SELECT * FROM `projects` ORDER BY id DESC LIMIT 0,7";
        $res_latest = mysql_query($sql_latest) or die(mysql_error());
       
	    if($res_latest) {
            while($row = mysql_fetch_assoc($res_latest)) {
                $arr_l[] = $row;
            }   
        }
		$this->latest_projects = $arr_l;
		
		$sql_all =" SELECT * FROM `projects` p  {$WHERE} AND approved = 1";
        $res_all = mysql_query($sql_all) or die(mysql_error());
       
	    if($res_all) {
            while($row = mysql_fetch_assoc($res_all)) {
                $arr_approved[] = $row;
            }   
        }
        $this->approved_projects = sizeof($arr_approved);
		
		$sql_all =" SELECT * FROM `projects` p {$WHERE} AND approved = 0";
        $res_all = mysql_query($sql_all) or die(mysql_error());
       
	    if($res_all) {
            while($row = mysql_fetch_assoc($res_all)) {
                $arr_pending[] = $row;
            }   
        }
        $this->count_pending = sizeof($arr_pending);
		$this->pending_projects = $arr_pending;
		
		$sql_all =" SELECT * FROM `projects` p
						INNER JOIN `course_categories` cc ON (p.category_id=cc.id) 
						INNER JOIN `courses` c ON (p.course_id=c.id)
							{$WHERE} 
							AND approved = 1 
						ORDER BY project_title ASC";
        $res_all = mysql_query($sql_all) or die(mysql_error());
       
	    if($res_all) {
            while($row = mysql_fetch_assoc($res_all)) {
                $arr_all[] = $row;
            }   
        }
        $this->count_projects = sizeof($arr_all);
	    
		return $arr_all;
    }

	function getCategoryProjects($id) {
		$arr_all = array();
		
		$WHERE = isset($id) && $id !=""? " WHERE p.category_id='".$id."' " :'WHERE 1'; 
		
		$sql =" SELECT * FROM `course_categories` WHERE id='".$id."' ";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $category_name = $row ['category'];
            } 
            
        	$this->category_name = $category_name;
		
	    }
		
		$sql_all =" SELECT * FROM `projects` p
						INNER JOIN `course_categories` cc ON (p.category_id=cc.id) 
						INNER JOIN `courses` c ON (p.course_id=c.id)
							{$WHERE} 
							AND approved = 1 
						ORDER BY project_title ASC";
        $res_all = mysql_query($sql_all) or die(mysql_error());
       
	    if($res_all) {
            while($row = mysql_fetch_assoc($res_all)) {
                $arr_all[] = $row;
				$category_name = $row ['category'];
            } 
            
        	$this->count_category_projects = sizeof($arr_all);
	    
			
        }
		
		return $arr_all;  
    }
	
	function get_project_downloadInfo($spec_id) {
		$arr = array();
		
		$WHERE = isset($spec_id) && $spec_id !=""? " WHERE p.spec_id='".$spec_id."'" :'WHERE 1'; 
		
		$sql =" SELECT * FROM `projects` p
						INNER JOIN `users` u ON (p.user_id=u.id)
						INNER JOIN `course_categories` cc ON (p.category_id=cc.id) 
						INNER JOIN `courses` c ON (p.course_id=c.id)
							{$WHERE} 
							AND approved = 1";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr[] = $row;
				$this->project_titleInfo = $row ['project_title'];
				$this->project_payInfo = $row ['paid'];
				$this->project_feeInfo = $row ['fee'];
				$this->project_descInfo = $row ['description'];
				$this->project_approveInfo = $row ['approved'];
				$this->project_userfolderInfo = $row ['user_folder_name'];
				$this->project_projectfolderInfo = $row ['spec_id'];
				$this->project_fileInfo = $row ['zipped_file_name'];
				$this->project_viewsInfo = $row ['views'];
				$this->project_userIdInfo = $row ['user_id'];
            }   
        }
		
        
		return $arr;
    }
	
	function get_user_projects($id) {
		$arr = array();
		$arr_appr = array();
		$arr_user_projects = array();
		
		
		
		
		//Get Approved projects
		$sql =" SELECT projects.id, projects.spec_id, projects.type, projects.zipped_file_name, projects.project_title, projects.category_id, projects.user_id, projects.description, projects.keywords, projects.screen_shot, projects.languages,
					   projects.approved, projects.paid, projects.fee, projects.views, projects.date_uploaded, projects.course_id, courses.course_name,
					   courses.category_id, course_categories.category
				 FROM 
				 	projects projects
				 LEFT JOIN courses courses
				 	ON(projects.course_id = courses.id)
				 LEFT JOIN course_categories course_categories
				 	ON(courses.category_id = course_categories.id)		 
				 WHERE 1
				 AND	  
				 	projects.user_id='".$id."' 
				 AND 
				 	approved = 1";
        $res = mysql_query($sql) or die(mysql_error());
		
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr_appr[] = $row;
            }   
		}		
		$this->approved_user_projects = $arr_appr;
		
		//Get Purchases
		$sql =" SELECT COUNT(id) AS purchases FROM `p_downloads` WHERE vendor='".$id."' AND type=2";
        $res = mysql_query($sql) or die(mysql_error());
		
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $purchases = $row['purchases'];
            }   
		}		
		$this->user_projects_purchases = $purchases;
		
		$sql =" SELECT COUNT(id) AS user_downloads FROM `p_downloads` WHERE vendor='".$id."'";
        $res = mysql_query($sql) or die(mysql_error());
		
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $downloads = $row['user_downloads'];
            }   
		}		
		$this->user_projects_downloads = $downloads;
		
		//Get Revenue
		$sql =" SELECT SUM(revenue) AS revenue FROM `p_downloads` WHERE vendor='".$id."' AND type=2";
        $res = mysql_query($sql) or die(mysql_error());
		
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $revenue = $row['revenue'];
            }   
		}		
		$this->user_estimated_revenue = $revenue;
				 
		
		$sql =" SELECT projects.id, projects.spec_id, projects.type, projects.zipped_file_name, projects.project_title, projects.category_id, projects.user_id, projects.description, projects.keywords, projects.screen_shot, projects.languages,
					   projects.approved, projects.paid, projects.fee, projects.views, projects.date_uploaded, projects.course_id, courses.course_name,
					   courses.category_id, course_categories.category
				 FROM 
				 	projects projects
				 LEFT JOIN courses courses
				 	ON(projects.course_id = courses.id)
				 LEFT JOIN course_categories course_categories
				 	ON(courses.category_id = course_categories.id)
				 WHERE 1
				 AND	  
				 	projects.user_id='".$id."'";
        
		$res = mysql_query($sql) or die(mysql_error());
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
            	$row ['downloads'] = $this->count_project_downloads($row['id']);
                $arr[] = $row;
            }   
		}
		
		$this->count_user_projects = sizeof($arr);
       
	   return $arr;		
    }
    
    function count_project_downloads($project_id) {
    	$count = 0;
    	$sql =" SELECT COUNT(id) AS downloads
    				FROM
    					p_downloads
					WHERE
						project_id='".$project_id."'";
        
		$res = mysql_query($sql) or die(mysql_error());
       
	   if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $count = $row['downloads'];
            }   
       }
	   return $count;	
    	
    }
    
    function getProject($id) {
		$arr = array();
		
		$sql =" SELECT * FROM `projects` p 
					INNER JOIN (`courses` c 
							        LEFT JOIN `course_categories` cc ON (c.category_id = cc.id)
								) ON(p.course_id = c.id)
					INNER JOIN `users` u ON(p.user_id = u.id)
					WHERE 1 
					AND p.spec_id='".$id."'";
        
		$res = mysql_query($sql) or die(mysql_error());
       
	   if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr[] = $row;
            }   
       }
	   $this->project_views = $row['views'];
       
	   return $arr;		
    }
	
	function getProjectFiles($id) {
		$arr = array();
		
		$sql =" SELECT * FROM `files` WHERE project_id='".$id."'";
        $res = mysql_query($sql) or die(mysql_error());
       
	   if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr[] = $row;
            }   
       }
       $this->count_files = sizeof($arr);
	   return $arr;		
    }
   
    function lookupProjects($search) {
		$arr = array();
        $sql="SELECT * FROM `projects` p
				INNER JOIN `course_categories` cc ON (p.category_id=cc.id) 
				INNER JOIN `courses` c ON (p.course_id=c.id)
				WHERE 1
				AND approved = 1";
		$sql .= " AND ";
        $sql .=" ( project_title LIKE '%{$search}%')";					
        
		$res = mysql_query($sql) OR DIE(mysql_error());
        
		if($res) {
            while($row= mysql_fetch_assoc($res)) {
                $arr[] = $row; 
                }   
			
        } 
		
		return $arr;
		
    }  
	
	function get_vendor_rates() {
		$sql="SELECT * FROM vendor_rates ORDER BY id DESC LIMIT 0,1";
		$res = mysql_query($sql) OR DIE(mysql_error());
        if($res) {
            while($row= mysql_fetch_assoc($res)) {
                $rate = $row ['rate']; 
                }   
		} 
		
		return $rate;
		
    }
	
	function save_downloads($spec_id) {
		
		$ipdownloader= $_SERVER['REMOTE_ADDR'];
		$ipdownloader = mysql_real_escape_string($ipdownloader);
		
		$row = $this->get_project_downloadInfo($spec_id);
		$vendor_rate =  $this->get_vendor_rates();
		
		//$arr = array();
		$now = date ('d/m/Y H:i:s');
		$fee = $this->project_feeInfo;
		$revenue = $fee * $vendor_rate;
		
		$sql = "INSERT INTO p_downloads (id,project_id ,type, ip, payer, vendor, revenue, ip,download_date) VALUES (null, '".$spec_id."','".$this->project_payInfo."''".$_SESSION ['user_id']."', '".$this->project_userIdInfo."', '".$revenue."', '".$ipdownloader."','".$now."')";
		$res = mysql_query($sql) or die('savedownload=>'.mysql_error());
							
        $res = mysql_query($sql) OR DIE(mysql_error());
        
		if($res) {
            return 1;
        } else {
			return 0;
		}
		
    }
	function get_downloads($project_id) {
		//$arr = array();
		$now = date ('d/m/Y H:i:s');
		$sql="SELECT COUNT(id) as project_downloads FROM `p_downloads`
				WHERE 1
				AND project_id = '".$project_id."'";
		$res = mysql_query($sql) OR DIE(mysql_error());
        if($res) {
            while($row= mysql_fetch_assoc($res)) {
                $downloads = $row ['project_downloads']; 
                }   
		} 
		
		return $downloads;
        
		
    }
	
	function get_all_downloads() {
		//$arr = array();
		$now = date ('d/m/Y H:i:s');
		$sql="SELECT COUNT(id) as downloads FROM `p_downloads`";
		$res = mysql_query($sql) OR DIE(mysql_error());
        if($res) {
            while($row= mysql_fetch_assoc($res)) {
                $downloads = $row ['downloads']; 
                }   
		} 
		
		return $downloads;
        
		
    }
	function save_views($id) {
		//$arr = array();
		$sql="SELECT * FROM `projects`
				WHERE 1
				AND spec_id = '".$id."'";
		$res = mysql_query($sql) OR DIE(mysql_error());
		
        if($res) {
            while($row= mysql_fetch_assoc($res)) {
                $views = $row ['views']; 
                }   
		} 
		
		$new_views = $views + 1;
        
		$sql="UPDATE `projects` 
				SET
				views = '".$new_views."'
				WHERE 1
				AND spec_id = '".$id."'";					
        $res = mysql_query($sql) OR DIE(mysql_error());
        
		if($res) {
            return 1;
        } else {
			return 0;
		}
		
    } 
	
	function get_views($id) {
		$sql="SELECT * FROM `projects`
				WHERE 1
				AND spec_id = '".$id."'";
		$res = mysql_query($sql) OR DIE(mysql_error());
        if($res) {
            while($row= mysql_fetch_assoc($res)) {
                $v = $row ['views']; 
                }   
		} 
		
		return $v;
     	
	}
	
	function approve_project($id) {
		$sql="UPDATE `projects` 
				SET
					approved = 1
				WHERE
					spec_id='".$id."'";
		$res = mysql_query($sql) OR DIE(mysql_error());
        
		if($res) {
            return true;
        } else {
        	return false;
        }
		
    } 
	
	function delete_project($id) {
		$sql="DELETE FROM `projects` 
				WHERE
					spec_id='".$id."'";
		$res = mysql_query($sql) OR DIE(mysql_error());
        
		if($res) {
            return true;
        } else {
        	return false;
        }
		
    }  

}