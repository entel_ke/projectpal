<?php
class Categories extends classMain {
    
	var $categories;
	var $count_categories;    
		
    function __construct() { 
		parent::__construct();
    }
	function saveCategory($category) {
		$now = date ('d/m/Y H:i:s');
		
		$sql = "INSERT INTO course_categories (id, category, date_saved) VALUES (null, '".$category."', '".$now."')";
		$res = mysql_query($sql) or die('savecategory=>'.mysql_error());
		
		if ($res) {
			return 1;
		} else {
			return 0;		
		}
	}
	function getCategories() {
		$arr_all = array();
				
		$sql =" SELECT * FROM `course_categories` ORDER BY category ASC";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr_all[] = $row;
            }   
        }
		
        $this->count_categories = sizeof($arr_all);
	    
		return $arr_all;
    }
	
	
	
    function getCategory($id) {
		$arr = array();
		
		$sql =" SELECT * FROM `course_categories` WHERE id='".$id."'";
        $res = mysql_query($sql) or die(mysql_error());
       
	   if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr[] = $row;
            }   
       }
       
	   return $arr;
    }
	function drop($args) {
		$args = explode(",", $args);
		$args = array_unique($args);
		     
        if(sizeof($args)){
            foreach($args as $k=>$id) { 
                $this->deleteCategory($id);
			}
        } else {
			return 0;		
		}
		
    }
	function deleteCategory($id) {
		$sql ="DELETE FROM `course_categories` WHERE id='".$id."'";
        $res = mysql_query($sql) or die(mysql_error());
		
		if ($res) {
			return 1;
		} else {
			return 0;		
		}
		
    }
	function updateCategory($id, $category) {
		
		$now = date ('d/m/Y H:i:s');
		
		$sql ="UPDATE `course_categories` SET category='".$category."', date_saved='".$now."' WHERE id='".$id."'";
        $res = mysql_query($sql) or die(mysql_error());
		
		if ($res) {
			return 1;
		} else {
			return 0;		
		}
		
    }
    
   }