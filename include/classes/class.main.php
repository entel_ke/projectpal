<?php

class classMain {
    
	var $host;               // mySQL host to connect to
    var $user;               // mySQL user name
    var $pw;                 // mySQL password
    var $db;                 // mySQL database to select
	
	var $last_error;            // current/last database link identifier
    
    var $db_link;            // current/last database link identifier
    var $auto_slashes;       // the class will add/strip slashes when it can
	var $currentYear;
	
	var $error_verify;
	
	var $users;
	var $count_users;
	var $unread_count;
	var $unread_messages;
	var $count_all_messages;
	
    private $username = "patikana";
    private $apiKey = "25dee3389d84101b3b7da93f0d6683d3af7e1fcbe46263c4ba09f97f0e869fe2";
 
	
    var $cypher="sha256";    //password
    
    function __construct() { 
		
		$this->connect();
    }

   function connect($host=HOST, $user=USER, $pw=PASS, $db=DBASE, $persistant=true) {
		date_default_timezone_set('Africa/Nairobi');
		$this->currentYear = date('Y', time()); 
		if (!empty($host)) $this->host = $host; 
		if (!empty($user)) $this->user = $user; 
		if (!empty($pw)) $this->pw = $pw; 

		// Establish the connection.
		if ($persistant) 
		 $this->db_link = mysql_pconnect($this->host, $this->user, $this->pw);
		else 
		 $this->db_link = mysql_connect($this->host, $this->user, $this->pw);

		// Check for an error establishing a connection
		if (!$this->db_link) {
		 $this->last_error = mysql_error();
		 return false;
		} 

		// Select the database
		if (!$this->select_db($db)) //return false;

		return $this->db_link;  // success
   }

   function select_db($db='') {
      // Selects the database for use.  If the function's $db parameter is 
      // passed to the function then the class variable will be updated.
 
      if (!empty($db)) $this->db = $db; 
     
      if (!mysql_select_db($this->db)) {
         $this->last_error = mysql_error();
         return false;
      }
 
      return true;
    }

	function get_geolocation ($ip_address) {
		$user_ip = $ip_address;
		$geo = unserialize(file_get_contents("http://www.geoplugin.net/php.gp?ip=$user_ip"));
		
		return $geo;
    }

	function get_rates () {
			
		$arr = array ();	
		$sql = "SELECT * FROM vendor_rates ORDER BY id DESC LIMIT 0,1" ;
		$res = mysql_query($sql) or die(mysql_error()); 
	
		if($res) {
			
            while($row = mysql_fetch_assoc($res)) {
                $arr[] = $row;
            }   
        
        return $arr;
		} else {
			return false;
		}
		
	}	
	
	function update_vendor_rates ($rate) {
		$now = date ('d/m/Y H:i:s');
		$sql = "INSERT INTO vendor_rates
					SET 
						rate='".$rate."',
						last_update='".$now."'" ;
		$res = mysql_query($sql) or die(mysql_error()); 
	
		if($res) {
			return true;
		} else {
			return false;
		}
		
	}	
	
	function generate_verify_code ($l, $c) {
		
		for ($s = '', $cl = strlen($c)-1, $i = 0; $i < $l; $s .= $c[mt_rand(0, $cl)], ++$i);
		return $s;

	}
	
	function registerUser($fname, $lname, $phone, $email, $pword, $address) {
		
			if ($this->check_user_phone($phone, 1)) {
				return 77;
				exit;
			} else if ($this->check_user($email, 1)) {
				return 78;
				exit;
			} 
			
			$thumb = isset($_SESSION['thumb'])? $_SESSION['thumb']:'user.png';
			$now = date ('d/m/Y H:i:s');
			$secure_password = $this->generatePassword($pword);
			$verify_code = $this->generate_verify_code ('5', '0123456789');
			//echo $verify_code;
			//exit;
			
			$sql = "INSERT INTO users
					SET 
						verify_code='".$verify_code."',
						fname='".$fname."',
						lname='".$lname."',
						phone='".$phone."',
						email='".$email."',
						password='".$secure_password."',
						address='".$address."',
						thumbnail='".$thumb."',
						date_saved='".$now."'" ;
	
			$res = mysql_query($sql) or die(mysql_error()); 
	
			if($res) {
					$user_id = mysql_insert_id();
					$folder_name = $fname.'-'.$lname.'-ppal-'.uniqid();
					
					if($make_dir = mkdir('../../user_area/server/php/f_browser/files/'.$folder_name)) {
						$sql_save_folder_name = "UPDATE users
													SET
													user_folder_name='".$folder_name."'
													WHERE
													id='".$user_id."'";
						$res_sfn = mysql_query($sql_save_folder_name) or die(mysql_error()); 
					}
				
					$_SESSION['user_id'] = $user_id;
					$_SESSION['user_name'] = $fname." ".$lname;
					$_SESSION['firstname'] = $fname;
					$_SESSION['lastname'] = $lname;
					$_SESSION['user_email'] = $email;
					$_SESSION['user_phone'] = $phone;
					$_SESSION['user_code'] = $verify_code;
					$_SESSION['user_address'] = $address;
					$_SESSION['thumbnail'] = $thumb;
					
					if ($res_sfn){
						$_SESSION['user_folder_name']  = $folder_name ;
					}	
					
					$_SESSION['active'] = 0;
					
					if (isset($_SESSION['thumb'])) unset($_SESSION['thumb']);
					
					$this->sendText($phone, $verify_code, '');
					
					return true;
										
			} else {
				return false;
			}
	}
	
	function reset_password ($phone, $level) {
		$phone = intval($phone);
		
		$code = $this->generate_verify_code ('5', '0123456789');
			
		if ($this->save_reset_password ($phone, $code)) {
			$this->sendText($phone, $code, true);
			return true;
		} else {
			return false;
		}
		
	}	
	
	function save_reset_password ($phone, $code) {
		$secure_password = $this->generatePassword($code);
		$sql = "UPDATE users
					SET 
						verify_code='".$code."',
						password='".$secure_password."'
					WHERE 
						phone='".$phone."'" ;
		$res = mysql_query($sql) or die(mysql_error()); 
	
		if($res) {
			return true;
		} else {
			return false;
		}
		
	}	
		
	function sendText($recipients, $code, $msg) {
        $message = 'ProjectPal verification code:'.$code;
		
		if ($msg){
			$message = 'ProjectPal new password:'.$code;
		}
		
		$rcp="+254".intval($recipients);	  
		$gateway = new AfricasTalkingGateway($this->username, $this->apiKey);
		$results = $gateway -> sendMessage($rcp, $message);
		
		return true;
	}
	
	function activateUserAccount($id, $activation_code) {
		$user_data = $this->getUserDataByID($id);
			
		if ($user_data['verify_code'] == $activation_code) {
			
			
			$sql = "UPDATE `users` 
						SET
							active= 1 
						WHERE id='".$id."'";
			
			$res = mysql_query($sql) or die(mysql_error());
			if ($res) {
				//unset($_SESSION);
				$_SESSION['user_id'] = $id;
				$_SESSION['user_name'] = $user_data['fname']." ".$user_data['lname'];
				$_SESSION['firstname'] = $user_data['fname'];
				$_SESSION['lastname'] = $user_data['lname'];
				$_SESSION['user_email'] = $user_data['email'];
				$_SESSION['user_phone'] = $user_data['phone'];
				$_SESSION['user_address'] = $user_data['address'];
				$_SESSION['user_code'] = $user_data['verify_code'];
				$_SESSION['thumbnail'] = $user_data['thumbnail'];
				$_SESSION['active'] = 1;
				
				return true;
			} else {
				
				$this->error_verify = 'Authentification failed, check verification code';
				return false;
			}
			
		} else {
			
			return false;
		}
		
	}
	
	function updateUser($fname, $lname, $phone, $email, $cpword, $pword, $address) {
		
		if($this->check_user($_SESSION['user_email'], 1)) {
			
			
			if ($this->check_user_phone($phone, 1)) {
				return 77;
				exit;
			} 
			
			$user_data = $this->fetchPass($_SESSION['user_email']);
			
			$db_hash = $user_data['password'];
			$db_id = $user_data['id'];
			
			if ($this->password_verify_with_rehash($cpword, $db_hash, $db_id)) {
									
				$now = date ('d/m/Y H:i:s');
				$secure_password = $this->generatePassword($pword);
				
				if ($this->check_user($email, 1) && $email!=$_SESSION['user_email']) {
					return 77;
					exit;
				} 
				
				$sql = "UPDATE `users` 
							SET
								fname='".$fname."',
								lname='".$lname."',
								phone='".$phone."',
								email='".$email."',
								password='".$secure_password."',
								address='".$address."',
								last_update='".$now."' 
							WHERE id='".$db_id."'";
			
				$res = mysql_query($sql) or die(mysql_error()); 
		
				if($res) {
						$_SESSION['user_id'] = $db_id;
						$_SESSION['user_name'] = $fname." ".$lname;
						$_SESSION['firstname'] = $fname;
						$_SESSION['lastname'] = $lname;
						$_SESSION['user_email'] = $email;
						$_SESSION['user_phone'] = $phone;
						$_SESSION['user_address'] = $address;
						$_SESSION['user_code'] = $user_data['verify_code'];
						$_SESSION['active'] = $user_data['active'];
					return true;
				} else {
					return false;
				}
				 
			} else {
				return 101;
			}
		} else {
			return false;
		}
	}
	
	function check_user($email, $user_level) {
		switch ($user_level) {
            case 1: 
            $sql = "SELECT COUNT(*) AS num_rows FROM users WHERE email = '".$email."'";
            break;
			case 2: 
            $sql = "SELECT COUNT(*) AS num_rows FROM users WHERE email = '".$email."' AND user_level='2'";
            break;
			default:
            $sql = "SELECT COUNT(*) AS num_rows FROM users WHERE email = '".$email."'";
		}	
        $result = mysql_query($sql) or die('Check error=>'.mysql_error());
        
        if (mysql_result($result, 0, "num_rows") == 1) {
            return true;
        } else {
            return false;
        }
    }  
	
	function check_user_phone($phone, $user_level) {
		switch ($user_level) {
            case 1: 
            $sql = "SELECT COUNT(*) AS num_rows FROM users WHERE phone = '".$phone."'";
            break;
			default:
            $sql = "SELECT COUNT(*) AS num_rows FROM users WHERE phone = '".$phone."'";
			
		}	
        $result = mysql_query($sql) or die('Check error=>'.mysql_error());
        
        if (mysql_result($result, 0, "num_rows") == 1) {
            return true;
        } else {
            return false;
        }
    }             
    
	public function fetchPass($email) {
        
		$sql = null;
        $sql = "SELECT *
					FROM `users` 
					WHERE `email` = '".$email."'";
        if (!$result = mysql_query($sql)) {
           return false;
        } else {
            $row = mysql_fetch_array($result);
            return $row; 
        }   
        
    }
	
	
	public function getUserDataByID($id){
        
		$sql = null;
        $sql = "SELECT *
					FROM `users` 
					WHERE `id` = '".$id."'";
        if (!$result = mysql_query($sql)) {
           return false;
        } else {
            $row = mysql_fetch_array($result);
            return $row; 
        }   
        
    }
	
	public function generatePassword($password) {
		$hash = password_hash($password, PASSWORD_DEFAULT);
        return $hash;
    }
	
	public function comparePassword($pass, $db_hash) {
   		return (password_verify($pass, $db_hash)) ? true : false; 
    }
    
    public function password_verify_with_rehash($pass, $db_hash, $db_id) {
		    if (!password_verify($pass, $db_hash)) {
		        return false;
				exit;
		    }
		
		    if (password_needs_rehash($db_hash, PASSWORD_DEFAULT)) {
		        $hash = password_hash($pass, PASSWORD_DEFAULT);
		        
		        $sql = "UPDATE `users` SET password ='".$pass."' WHERE id='".$db_id."'";
				$res = mysql_query($sql) or die(mysql_error());
			}
		
		    return true;
	}
	
	function userLogin ($email, $pass, $user_level) {
		$arr = array ();
		
		if($this->check_user($email, $user_level)) {
			
			$user_data = $this->fetchPass($email);
			
			$db_hash = $user_data['password'];
			$db_id = $user_data['id'];
			
			if ($this->password_verify_with_rehash($pass, $db_hash, $db_id)) {
				
				//print_r('<pre>');
				//print_r($user_data);
				if ($user_level == 2) {
					$_SESSION['admin'] = $user_data['email'];
					$_SESSION['admin_id'] = $user_data['id'];
					$_SESSION['admin_name'] = $user_data['fname']." ".$user_data['lname'];
				}	
				$_SESSION['user_id'] = $user_data['id'];
				$_SESSION['user_name'] = $user_data['fname']." ".$user_data['lname'];
				$_SESSION['firstname'] = $user_data['fname'];
				$_SESSION['lastname'] = $user_data['lname'];
				$_SESSION['user_code'] = $user_data['verify_code'];
				$_SESSION['user_email'] = $user_data['email'];
				$_SESSION['user_phone'] = $user_data['phone'];
				$_SESSION['user_address'] = $user_data['address'];
				$_SESSION['thumbnail'] = $user_data['thumbnail'];
				$_SESSION['user_folder_name'] = $user_data['user_folder_name'];
				$_SESSION['active'] = $user_data['active'];
				
				return true;
				 
			} else {
				return false;
			}
		} else {
			return false;
		}		
    }
	function userLogin_ ($email, $pass) {
		$arr = array ();
		
		$sql =" SELECT * FROM `users` WHERE email='".$email."' AND password='".$pass."' AND user_level='1'";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $password = $row ['password'];
				$id = $row ['id'];
				$name = $row ['fname'].' '.$row['lname'];
				$email = $row ['email'];
				$phone = $row ['phone'];
				$address = $row ['address'];
			}   
			
			if ($pass==$password) {
				$_SESSION['user_id'] = $id;
				$_SESSION['user_name'] = $name;
				$_SESSION['user_email'] = $email;
				$_SESSION['user_phone'] = $phone;
				$_SESSION['user_address'] = $address;
				
				return 1;
			} else {
				return 0;
			}
			
        } else {
			return 0;
	    }
       
	}
	
	function getUserDetails ($id) {
		$arr_all =array ();
		$arr = array();
		
		$sql =" SELECT * FROM `users` WHERE id='".$id."'";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr [] = $row;
            }   
			
			return $arr;    
        } else {
			return 0;
	    }
       
	   
	}
	function getUsers() {
		$arr_all =array ();
		$arr = array();
		$sql =" SELECT * FROM `users` ORDER BY id DESC";
        $res = mysql_query($sql) or die(mysql_error());
		
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr[] = $row;
            }   
        }
		
		$this->count_users = sizeof($arr);
		
        return $arr;
		  
    }
	
	function reg_user($fname, $lname, $gender, $phone, $email, $pass) {
		$now = date ('d/m/Y H:i:s');
		$ip = 0;
		//echo $now;
		$sql = "INSERT INTO `users`
						SET
							`fname` = '".$fname."',
							`lname` = '".$lname."',
							`gender` = '".$gender."',
							`phone` = '".$phone."',
							`email` = '".$email."',
							`password` = '".$pass."',
							`date_registered` = '".$now."'";
							
		$res = mysql_query($sql) or die('subscribe=>'.mysql_error());
		
		if ($res) {
			return 1;
			$this->subscribe($email);
		} else {
			return 0;		
		}
    }
	
	function subscribe($email) {
		$now = date ('d/m/Y H:i:s');
		$ip = 0;
		//echo $now;
		$sql = "SELECT * FROM subscription WHERE email='".$email."'";
		$res = mysql_query($sql) or die('subscribe=>'.mysql_error());
		
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $db_email = $row ['email'];
			}   
			
			if ($db_email!=$email) {
				$this->complete_subscription($email);
			} else {
				return 11;
			}
			
        } else {
			return 0;
	    }
       
    }
	function saveMsg($name, $email, $msg) {
		$now = date ('d/m/Y H:i:s');
		
		$sql = "INSERT INTO messages (id, name, message, email, date_sent) VALUES (null, '".$name."', '".$msg."', '".$email."', '".$now."')";
		$res = mysql_query($sql) or die('sendMsg=>'.mysql_error());
		if ($res) {
			return 1;
		} else {
			return 0;		
		}
	}
	
	function getMessages() {
		$arr_all =array ();
		$arr = array();
		$arr_unread =array ();
		
		$sql =" SELECT * FROM `messages`";
        $res = mysql_query($sql) or die(mysql_error());
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr_all[] = $row;
            }   
        }
        $this->count_all_messages = sizeof($arr_all);
		
		$sql =" SELECT * FROM `messages` WHERE status = '0'";
        $res = mysql_query($sql) or die(mysql_error());
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr_unread[] = $row;
            }   
        }
        $this->unread_count = sizeof($arr_unread);
		$this->unread_messages = $arr_unread;
		
		$sql =" SELECT * FROM `messages` ORDER BY id DESC LIMIT 0,100";
        $res = mysql_query($sql) or die(mysql_error());
       
	    if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr[] = $row;
            }   
        }
       
	   return $arr_all;
    }
	
    function getMessageById($id) {
		$arr_all =array ();
		$arr = array();
		$sql =" SELECT * FROM `messages` WHERE id='".$id."'";
        $res = mysql_query($sql) or die(mysql_error());
		
		if($res) {
            while($row = mysql_fetch_assoc($res)) {
                $arr[] = $row;
            }   
        }
        return $arr;
		  
    }
    function setRead($id) {
		
		$sql ="UPDATE `messages` 
					SET 
						status = 1 
					WHERE id = '".$id."'";  
        $res = mysql_query($sql) or die('update error' . mysql_error()); 
       
        if($res) {
            return 1; 
        } else {
			return 0;
	    }
          
    }
	
	function send_mail($mail_address, $msg = 29, $subj = 28, $send_admin = false) {
	    $mail = new SimpleMail();
        if($mail->send_mail($mail_address, $msg, $subj, $send_admin)) {
            return true;
        }
    }
	
	function complete_subscription($email){
		$now = date ('d/m/Y H:i:s');
		$sql = "INSERT INTO `subscription`
						SET
							`email` = '".$email."',
							`ip` = '".$ip."',
							`date_saved` = '".$now."'";
							
		$res = mysql_query($sql) or die('subscribe=>'.mysql_error());
		
		if ($res) {
			return 1;
		} else {
			return 0;		
		}
	}
    
    function logout() {
		session_destroy();
		header('location:./');
    }
    
   }