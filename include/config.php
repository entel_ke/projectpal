<?php
	session_start();
	
    define('HOST', 'localhost');    
    define('USER', 'root');    
    define('PASS', '');
    define('DBASE', 'projectpal');
    define('URL', 'http://www.projectpal.co.ke/');
    require ("define.php");
	
	require ("password_compat/lib/password.php");
    require ("classes/class.main.php");
	require ("classes/class.password.php");
	require ("classes/class.projects.php");
	require ("classes/class.category.php");
	require ("classes/class.courses.php");
	require ("classes/class.pastpapers.php");
	require ("classes/class.image.php");
	require ("classes/class.upload.php");
	require ("classes/class.mail.php");
	require ("classes/AfricasTalkingGateway.php");
	require ("classes/Sms.php");
    
	include("smarty/Smarty.class.php");
	
    $rootDir=$_SERVER['DOCUMENT_ROOT'].'/';
    
	$smarty = new Smarty;
   // $smarty->assign('base', URL);
    
    