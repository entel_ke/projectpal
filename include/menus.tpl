 <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                
                <a class="navbar-brand" href="index.php"><span class="logo-holder"><img src="../img/main-logo.png" width="40" height="40"></span> Project Pal | Dashboard</a>
            </div>
            <!-- /.navbar-header -->

            <ul class="nav navbar-top-links navbar-right">
                <li class="dropdown">
                    <a  href="../../">
                        <i class="fa fa-home fa-fw"></i>Go Home 
                    </a>
                    
                </li>
                <!-- /.dropdown -->
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
						<li><a href="./upload_project.php"><i class="fa fa-upload fa-fw"></i> Upload Project</a>
                        </li>
						<li><a href="./"><i class="fa fa-book fa-fw"></i> My Projects</a>
                        </li>
                         <li class="divider"></li>
                        <li><a href="login.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
						<form method="post" action="results.php">
                            <div class="input-group custom-search-form">
								<input type="text" class="form-control" name="search" placeholder="Search..." required="">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="submit">
                                    <i class="fa fa-search"></i>
                                </button>
								</span>
							</div>
						</form>		
                            <!-- /input-group -->
                        </li>
                        <li>
                            <div class="row">
								<div class="col-md-12">
									<div class="thumbnail">
										<img src="../img/users/128/{$smarty.session.thumbnail}" alt="...">
										<div class="caption">
											<div class="edits">
												<h3 class="">{$smarty.session.user_name}</h3>
												<p>  
													<label>Email:</label><br>
													<span class="btn-block">{$smarty.session.user_email}</span>
													<label>Phone:</label><br>
													<span class="btn-block">{$smarty.session.user_phone}</span>
													<label>Address:</label><br>
													<span class="btn-block">{$smarty.session.user_address}</span>
												</p>
												<button class="btn btn-sm btn-primary btn-block edit-profile" role="button">Edit Profile</button>
											</div>	
											<div class="editor">
												<form role="form" id="edit-user-form">
													<div class="form-group">
														<button class="btn btn-sm btn-default btn-block cancel-edit" id="cancel-edit" >Cancel Editing</button>
													</div>
													<div class="form-group">
														<input class="form-control" placeholder="Firstname" name="fname" id="fname" value="{$smarty.session.firstname}" required=''>
													</div>
													<div class="form-group">
														<input class="form-control" placeholder="Lastname" name="lname" id="lname" value="{$smarty.session.lastname}" required=''>
													</div>
													<div class="form-group">
														<input class="form-control" type="phone" placeholder="Phone" name="phone" id="phone" value="{$smarty.session.user_phone}" required=''>
													</div>
													<div class="form-group">
														<input class="form-control" type="email" placeholder="Email" name="email" id="email" value="{$smarty.session.user_email}" required=''>
													</div>
													<div class="form-group">
														<input class="form-control" type="password" placeholder="Enter current password" name="cpword" id="cpword" value="" required=''>
													</div>
													<div class="form-group">
														<input class="form-control" type="password" placeholder="Enter new password" name="pword" id="pword" value="" required=''>
													</div>
													<div class="form-group">
														<input class="form-control" type="password" placeholder="Confirm Password" name="cpword" id="cpword" value="" required='' >
													</div>
													<div class="form-group">
														<textarea class="form-control" rows="3" placeholder="Address" name="address" id="address" value="{$smarty.session.user_address}" required=''></textarea>
													</div>
													<div class="clearfix"></div>
													<div class="form-group">
														<button type="submit" class="btn btn-sm btn-success btn-block" id="update-btn" name="submit">Update</button>
													</div>
													<div class="form-group">
														<button class="btn btn-sm btn-default btn-block cancel-edit" id="cancel-edit" >Cancel</button>
													</div>
													<div class="clearfix"></div>
												</form>
											</div>	
										</div>
									</div>
								</div>
							</div> 
						</li>
                        <li>
                            <a href="#"><i class="fa fa-table fa-fw"></i> Categories<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
								{section start=0 step=1 name=row loop=$categories}
									<li>
										<a href="courses.php?cat_id={$categories[row].id}">{$categories[row].category}</a>
									</li>
                                {/section}	
								
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="./login.php"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>