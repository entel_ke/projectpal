<?php
$su_rootClassClient = 'classes/class.main.php';
$rootDir=$_SERVER['DOCUMENT_ROOT'].'/';

    define("FILES_DIR", $rootDir . "f_browser/files/");
	// change this constants to the right mail settings
    define("WEBMASTER_MAIL", "info@projectpal.co.ke"); 
    define("WEBMASTER_NAME", "PROJECTPAL"); 
    define("ADMIN_MAIL", "admin@projectpal.co.ke"); 
    define("ADMIN_NAME", "AdminProjectPal"); 
	
	 // change these settings to use phpmailer in place of the PHP mail() function
    define("USE_PHP_MAILER", true); // true = use phpmailer
    define("PHP_MAILER_SMTP", false); // true send by SMTP, false = sendmail
    // if you enable these features you need to install the phpmailer class!
    if (USE_PHP_MAILER) include_once("classes/phpMailer/class.phpmailer.php");
    if (PHP_MAILER_SMTP) {
        define("SMTP_SERVER", "mail.projectpal.co.ke");
        define("SMTP_LOGIN", "login");
        define("SMTP_PASSWD", "password");
    }
    
    

?>